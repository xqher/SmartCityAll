package com.xqh.smartcityall.charity;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.page.ServiceAdapter;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class CharityAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private androidx.appcompat.widget.SearchView mSv;
    private androidx.recyclerview.widget.RecyclerView mIconRv;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private CharityAdapter charityAdapter;

    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_charity;
    }

    @Override
    public void initData() {

        get("/prod-api/api/public-welfare/ad-banner/list", res -> {
            runOnUiThread(() -> {
                DefBanner bean = new Gson().fromJson(res, DefBanner.class);
                if (bean.getCode() == 200) {
                    initBanner(mBanner, bean.getData(), id -> {
                    });
                }
            });
        });

        initSv(mSv, res -> jumpId(CharitySearchAc.class, res));

        charityAdapter = new CharityAdapter(activity, null);
        initRV(mListRv, charityAdapter);

        IconAdapter iconAdapter = new IconAdapter(activity, null);
        iconAdapter.onClick = ()->{
            jumpId(CharityListAc.class, iconAdapter.posBean.getIconId());
        };
        initGRV(mIconRv, iconAdapter, 4);

        get("/prod-api/api/public-welfare/public-welfare-type/list", res -> {
            runOnUiThread(() -> {
                DefType bean = new Gson().fromJson(res, DefType.class);
                if (bean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    for (DefType.DataBean data : bean.getData()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId()+""));
                        show.add(icon);
                    }
                    iconAdapter.setData(show);
                }
            });
        });
    }

    @Override
    public void resumeData() {

        get("/prod-api/api/public-welfare/public-welfare-activity/recommend-list", res -> {
            runOnUiThread(() -> {
                CharityBean bean = new Gson().fromJson(res, CharityBean.class);
                charityAdapter.setData(bean.getRows());
            });
        });
    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mSv = findViewById(R.id.sv);
        mIconRv = findViewById(R.id.iconRv);
        mListRv = findViewById(R.id.listRv);
    }
}