package com.xqh.smartcityall.charity;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class CharityListAc extends BaseAc {


    private androidx.appcompat.widget.SearchView mSv;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private CharityAdapter charityAdapter;
    private List<CharityBean.RowsBean> list = new ArrayList<>();
    private int typeId = 13;
    @Override
    public String initTitle() {
        return "公益列表";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_charity_list;
    }

    @Override
    public void initData() {

        typeId = Integer.parseInt(getJumpId());

        charityAdapter = new CharityAdapter(activity, null);
        initRV(mListRv, charityAdapter);


        initSv(mSv, this::searchList);

        mTodoBt.setOnClickListener(v -> {
            typeId = typeId<10?10:typeId>16?10:++typeId;
            List<CharityBean.RowsBean> temp = new ArrayList<>();
            for(CharityBean.RowsBean row:list) {
                if(row.getTypeId()==typeId) {
                    temp.add(row);
                }
            }

            charityAdapter.setData(temp);
        });



    }
    public void searchList(String str) {
        List<CharityBean.RowsBean> temp = new ArrayList<>();
        for(CharityBean.RowsBean row:list) {
            if(row.getName().contains(str)) {
                temp.add(row);
            }
        }
        charityAdapter.setData(temp);
        if(temp.size()==0) {
            toast("未查询到相应结果");
        }
    }
    @Override
    public void resumeData() {
        get("/prod-api/api/public-welfare/public-welfare-activity/list", res -> {
            runOnUiThread(() -> {
                CharityBean bean = new Gson().fromJson(res, CharityBean.class);
                list = bean.getRows();

                List<CharityBean.RowsBean> temp = new ArrayList<>();

                for(CharityBean.RowsBean row:list) {
                    if(row.getTypeId()==typeId) {
                        temp.add(row);
                    }
                }
                charityAdapter.setData(temp);
            });
        });
    }

    @Override
    public void initView() {

        mSv = findViewById(R.id.sv);
        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}