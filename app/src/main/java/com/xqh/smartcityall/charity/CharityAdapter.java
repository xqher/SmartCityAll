package com.xqh.smartcityall.charity;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class CharityAdapter extends RecyclerView.Adapter<CharityAdapter.VH> {


    private List<CharityBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public CharityBean.RowsBean posBean;

    public CharityAdapter(Context ctx, List<CharityBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<CharityBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_button, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_button;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mStatus;
        private Button mTodoBt;


        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(CharityBean.RowsBean bean) {

            loadImg(bean.getImgUrl(), mCover);
            mTitle.setText(getStr(bean.getName()));
            mContent.setText(getStrHtml("发布人:%s<br/>公益类别:%s<br/>项目时间:%s<br/>",
                    bean.getAuthor() ,
                    bean.getType().getName(),
                    bean.getActivityAt() ));
            mStatus.setText(String.format("已有%s人共捐款%s元", bean.getDonateCount() + "", bean.getMoneyNow()+""));

            mTodoBt.setOnClickListener(v -> {

                EditText net = new EditText(activity);
                net.setInputType(InputType.TYPE_CLASS_NUMBER);
                net.setHint("单位(元)");
                new AlertDialog.Builder(activity)
                        .setTitle("输入捐款金额，单位(元)")
                        .setView(net)
                        .setPositiveButton("捐", (dialog, which) -> {
                            String inp = net.getText().toString();
                            if (inp.isEmpty()) {
                                activity.toast("未输入内容");
                                return;
                            }
                            if (inp.equals("0")) {
                                activity.toast("请捐助至少1元");
                                return;
                            }
                            HashMap<String, String> map = new HashMap<>();
                            map.put("activityId", bean.getId()+"");
                            map.put("donateMoney", inp);

                            activity.post("/prod-api/api/public-welfare/donate-record", map, json -> {
                                activity.runOnUiThread(() -> {
                                    DefRes res = new Gson().fromJson(json, DefRes.class);
                                    activity.toast(res.getMsg());
                                    if(res.getCode()==200) {
                                        dialog.dismiss();
                                        activity.resumeData();
                                    }
                                });
                            });
                        })
                        .setNegativeButton("取消", null)
                        .create()
                        .show();
            });
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(CharityDetailAc.class, bean.getId());
            });
        }

        private void initView() {

            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mStatus = findViewById(R.id.status);
            mTodoBt = findViewById(R.id.todoBt);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
