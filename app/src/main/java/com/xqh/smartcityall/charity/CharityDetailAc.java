package com.xqh.smartcityall.charity;

import android.app.AlertDialog;
import android.text.InputType;
import android.widget.EditText;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.ManifestAdapter;
import com.xqh.smartcityall.pojo.ActiveDetail;
import com.xqh.smartcityall.pojo.CharityDetail;
import com.xqh.smartcityall.pojo.CharityRecord;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.ItemManifest;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getSp;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initWebView;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class CharityDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageDesc;
    private android.widget.TextView mCharityDynamic;
    private android.widget.TextView mCharityProgressTv;
    private android.widget.ProgressBar mCharityProgressBar;
    private android.widget.Button mTodoBt;
    private android.webkit.WebView mContent;
    private androidx.recyclerview.widget.RecyclerView mManifestRv;

    @Override
    public String initTitle() {
        return "公益详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_charity_detail;
    }

    @Override
    public void initData() {

        mTodoBt.setOnClickListener(v -> {
            EditText net = new EditText(activity);
            net.setInputType(InputType.TYPE_CLASS_NUMBER);
            net.setHint("单位(元)");
            new AlertDialog.Builder(activity)
                    .setTitle("输入捐款金额，单位(元)")
                    .setView(net)
                    .setPositiveButton("捐", (dialog, which) -> {
                        String inp = net.getText().toString();
                        if (inp.isEmpty()) {
                            toast("未输入内容");
                            return;
                        }
                        if (inp.equals("0")) {
                            toast("请捐助至少1元");
                            return;
                        }
                        HashMap<String, String> map = new HashMap<>();
                        map.put("activityId", getJumpId());
                        map.put("donateMoney", inp);

                        post("/prod-api/api/public-welfare/donate-record", map, json -> {
                            runOnUiThread(() -> {
                                DefRes res = new Gson().fromJson(json, DefRes.class);
                                toast(res.getMsg());
                                if(res.getCode()==200) {
                                    dialog.dismiss();
                                    resumeData();
                                }
                            });
                        });
                    })
                    .setNegativeButton("取消", null)
                    .create()
                    .show();
        });
    }

    @Override
    public void resumeData() {
        get("/prod-api/api/public-welfare/public-welfare-activity/"+getJumpId(), res -> {
            runOnUiThread(() -> {
                CharityDetail bean = new Gson().fromJson(res, CharityDetail.class);
                if (bean.getCode() == 200) {
                    CharityDetail.DataBean data = bean.getData();
                    mTitle.setText(getStr(data.getName()));
                    loadImg(data.getImgUrl(), mCover);
                    mPageDesc.setText(getStr("发布人:%s\n项目时间:%s\n项目备案号:%s",
                            getStr(data.getAuthor()),
                            getStr(data.getAuthor()),
                            getStr(data.getMoneyTotal()+data.getMoneyNow()+"")
                    ));

                    mCharityProgressTv.setText(getStr("共需%s元，已有%s人共捐助%s元",
                            data.getMoneyTotal()+"",
                            data.getDonateCount()+"",
                            data.getMoneyNow()+""));

                    mCharityProgressBar.setMax(data.getMoneyTotal());
                    mCharityProgressBar.setProgress(data.getMoneyNow());
                    ManifestAdapter manifestAdapter = new ManifestAdapter(activity, null);
                    initRV(mManifestRv, manifestAdapter);
                    List<ItemManifest> manifestList = new ArrayList<>();
                    for(CharityDetail.DataBean.DetailsListBean detail:data.getDetailsList()) {
                        manifestList.add(new ItemManifest(detail.getItemName(), detail.getItemMoney()+"元"));
                    }
                    manifestAdapter.setData(manifestList);

                    initWebView(mContent, data.getDescription());
                }
            });
        });

        get("/prod-api/api/public-welfare/donate-record/list?pageSize=20&pageNum=1&activityId="+getJumpId(), res -> {
            runOnUiThread(() -> {
                CharityRecord bean = new Gson().fromJson(res, CharityRecord.class);
                if (bean.getCode() == 200) {
                    StringBuilder sb = new StringBuilder(bean.getTotal());
                    for(CharityRecord.RowsBean row: bean.getRows()) {
                        sb.append("🔈")
                                .append(row.getUserName()).append("于")
                                .append(row.getCreateTime()).append("捐助了")
                                .append(row.getDonateMoney()).append("元!\t\t");
                    }
                    mCharityDynamic.setText(sb.toString());
                    mCharityDynamic.setSelected(true);
                }
            });
        });
    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageDesc = findViewById(R.id.pageDesc);
        mCharityDynamic = findViewById(R.id.charityDynamic);
        mCharityProgressTv = findViewById(R.id.charityProgressTv);
        mCharityProgressBar = findViewById(R.id.charityProgressBar);
        mTodoBt = findViewById(R.id.todoBt);
        mContent = findViewById(R.id.content);
        mManifestRv = findViewById(R.id.manifestRv);
    }
}