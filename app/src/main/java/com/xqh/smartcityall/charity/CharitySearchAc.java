package com.xqh.smartcityall.charity;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.page.NewsAdapter;
import com.xqh.smartcityall.page.NewsDetailAc;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.initRV;

public class CharitySearchAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private CharityAdapter adapter;
    @Override
    public String initTitle() {
        return "搜索结果";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {

        adapter = new CharityAdapter(activity, null);
        initRV(mListRv, adapter);


    }

    @Override
    public void resumeData() {
        get("/prod-api/api/public-welfare/public-welfare-activity/list", res -> {
            runOnUiThread(()->{
                CharityBean bean = new Gson().fromJson(res, CharityBean.class);
                List<CharityBean.RowsBean> temp = new ArrayList<>();
                if(bean.getCode()==200) {
                    for(CharityBean.RowsBean row:bean.getRows()) {
                        if(row.getName().contains(getJumpId())) {
                            temp.add(row);
                        }
                    }
                }
                adapter.setData(temp);
                if(temp.size()==0) {
                    toast("未查询到相应结果");
                }
            });
        });
    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
    }
}