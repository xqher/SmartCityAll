package com.xqh.smartcityall.charity;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.ItemVerticalAdapter;
import com.xqh.smartcityall.bus.BusDetailAc;
import com.xqh.smartcityall.pojo.BusLine;
import com.xqh.smartcityall.pojo.BusStation;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.get;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initHRV;
import static com.xqh.smartcityall.utils.Const.jumpTo;

public class BusAdapter extends RecyclerView.Adapter<BusAdapter.VH> {


    private List<BusLine.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public BusLine.RowsBean posBean;

    public BusAdapter(Context ctx, List<BusLine.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<BusLine.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_bus, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_bus;
        private BaseAc activity = (BaseAc) ctx;
        private Button mName;
        private TextView mStart;
        private TextView mEnd;
        private TextView mStatus;
        private TextView mTime;
        private ImageView mFolder;
        private RecyclerView mStationRv;
        private ItemVerticalAdapter itemAdapter;
        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            itemAdapter = new ItemVerticalAdapter(activity, null);
            initHRV(mStationRv, itemAdapter);
        }

        public void onItem(BusLine.RowsBean bean) {
            mName.setText(getStr(bean.getName()));
            mStart.setText(getStr(bean.getFirst()));
            mEnd.setText(getStr(bean.getEnd()));
            mTime.setText(getStr("运行时间:%s-%s",bean.getStartTime(), bean.getEndTime()));
            mStatus.setText(getStr("票价:%s 元\t\t里程:%s km",bean.getPrice(), bean.getMileage()));
            mName.setOnClickListener(v -> activity.jumpId(BusDetailAc.class, new Gson().toJson(bean)));
            get("/prod-api/api/bus/stop/list?linesId="+bean.getId(), res -> {
                activity.runOnUiThread(() -> {
                    BusStation station = new Gson().fromJson(res, BusStation.class);
                    if (station.getCode() == 200) {
                        List<ItemList> items = new ArrayList<>();
                        for(BusStation.RowsBean row:station.getRows()) {
                            ItemList item = new ItemList();
                            item.setTitle(row.getName());
                            items.add(item);
                        }
                        itemAdapter.setData(items);
                    }
                });
            });
            mFolder.setOnClickListener(v -> {
                if(mStationRv.getVisibility()==View.GONE) {
                    mStationRv.setVisibility(View.VISIBLE);
                    mFolder.setImageResource(R.drawable.ic_fold);
                }else {
                    mStationRv.setVisibility(View.GONE);
                    mFolder.setImageResource(R.drawable.ic_unfold);
                }
            });
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }

            });
        }

        private void initView() {
            mName = findViewById(R.id.name);
            mStart = findViewById(R.id.start);
            mEnd = findViewById(R.id.end);
            mStatus = findViewById(R.id.status);
            mTime = findViewById(R.id.time);
            mFolder = findViewById(R.id.folder);
            mStationRv = findViewById(R.id.stationRv);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
