package com.xqh.smartcityall.post;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.PostArea;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class PostAreaAdapter extends RecyclerView.Adapter<PostAreaAdapter.VH> {


    private List<PostArea.DataBean> list;
    private Context ctx;
    public Runnable onClick;
    public PostArea.DataBean posBean;

    public PostAreaAdapter(Context ctx, List<PostArea.DataBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<PostArea.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_area, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_area;
        private BaseAc activity = (BaseAc)ctx;
        private ImageView mCover;
        private TextView mTitle;

        public VH(@NonNull View itemView) {
            super(itemView);

            initView();
        }

        public void onItem(PostArea.DataBean bean) {

            loadImg(bean.getImgUrl(), mCover);
            mTitle.setText(getStr(bean.getName()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(PostAreaDetailAc.class, bean.getId());
            });
        }

        private void initView() {

            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
        }
        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
