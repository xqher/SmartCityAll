package com.xqh.smartcityall.post;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.PostAreaDetail;
import com.xqh.smartcityall.pojo.PostFile;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.Collections;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class PostAreaDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageDetail;
    private androidx.recyclerview.widget.RecyclerView mFileRv;

    @Override
    public String initTitle() {
        return "人才政策区域";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_area;
    }

    @Override
    public void initData() {
        get("/prod-api/api/youth-inn/talent-policy-area/" + getJumpId(), res -> {
            runOnUiThread(() -> {
                PostAreaDetail bean = new Gson().fromJson(res, PostAreaDetail.class);
                if (bean.getCode() == 200) {
                    PostAreaDetail.DataBean data = bean.getData();
                    loadImg(getStr(data.getImgUrl()), mCover);
                    mTitle.setText(getStr(data.getName()));
                    mPageDetail.setText(getStr(data.getIntroduce()));
                }
            });
        });
        PostFileAdapter postAdapter = new PostFileAdapter(activity, null);
        initRV(mFileRv, postAdapter);
        get("/prod-api/api/youth-inn/talent-policy/list?areaId="+getJumpId(), res -> {
            runOnUiThread(()->{
                PostFile bean = new Gson().fromJson(res, PostFile.class);
                if(bean.getCode()==200) {
                    Collections.reverse(bean.getData());
                    postAdapter.setData(bean.getData());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageDetail = findViewById(R.id.pageDetail);
        mFileRv = findViewById(R.id.fileRv);
    }
}