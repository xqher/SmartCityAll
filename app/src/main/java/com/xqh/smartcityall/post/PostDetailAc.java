package com.xqh.smartcityall.post;

import android.text.Html;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AdvBanner;
import com.xqh.smartcityall.pojo.PostDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initTargetBanner;

public class PostDetailAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private android.widget.TextView mPostConfigTv;

    @Override
    public String initTitle() {
        return "驿站详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_post_detail;
    }

    @Override
    public void initData() {
        get("/prod-api/api/youth-inn/youth-inn/" + getJumpId(), res -> {
            runOnUiThread(() -> {
                PostDetail bean = new Gson().fromJson(res, PostDetail.class);
                if (bean.getCode() == 200) {
                    PostDetail.DataBean data = bean.getData();
                    List<AdvBanner.RowsBean> banners = new ArrayList<>();
                    for(String str:data.getImageUrlList()) {
                        banners.add(new AdvBanner.RowsBean(str, 0));
                    }
                    initTargetBanner(mBanner, banners, targetId -> {

                    });

                    mTitle.setText(getStr(data.getName()));
                    mPostConfigTv.setText(Html.fromHtml(String.format(
                            "<br/><h4>地址:</h4>&emsp;%s" +
                                    "<h4>联系电话📞:</h4>&emsp;%s" +
                                    "<h4>办理入住时间:</h4>&emsp;%s" +
                                    "<h4>剩余床位:</h4>&emsp; 男(%s)&emsp;女(%s)" +
                                    "<h4>驿站介绍:</h4>&emsp;%s" +
                                    "<h4>房间配置:</h4>&emsp;%s" +
                                    "<h4>周边配套:</h4>&emsp;%s" +
                                    "<h4>特色服务:</h4>&emsp;%s<br/>",
                            getStr(data.getAddress()),
                            getStr(data.getPhone()),
                            getStr(data.getWorkTime()),
                            data.getBedsCountBoy(),
                            data.getBedsCountGirl(),
                            data.getIntroduce(),
                            data.getInternalFacilities(),
                            data.getSurroundingFacilities(),
                            data.getSpecialService()
                                    ),Html.FROM_HTML_MODE_COMPACT));

                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mPostConfigTv = findViewById(R.id.postConfigTv);
    }
}