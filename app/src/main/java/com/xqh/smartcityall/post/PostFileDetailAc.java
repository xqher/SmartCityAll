package com.xqh.smartcityall.post;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.PostAreaDetail;
import com.xqh.smartcityall.pojo.PostFileDetail;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initWebView;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class PostFileDetailAc extends BaseAc {


    private android.widget.TextView mPageTitle;
    private android.widget.TextView mPageDetail;
    private android.webkit.WebView mContent;

    @Override
    public String initTitle() {
        return "人才政策文件详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_file_detail;
    }

    @Override
    public void initData() {
        get("/prod-api/api/youth-inn/talent-policy/" + getJumpId(), res -> {
            runOnUiThread(() -> {
                PostFileDetail bean = new Gson().fromJson(res, PostFileDetail.class);
                if (bean.getCode() == 200) {
                    PostFileDetail.DataBean data = bean.getData();
                    mPageTitle.setText(getStr(data.getTitle()));
                    mPageDetail.setText(String.format("发布人:%s\n发布时间:%s",
                            getStr(data.getAuthor()),
                            getStr(data.getCreateTime())));
                    initWebView(mContent, data.getContent());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mPageTitle = findViewById(R.id.pageTitle);
        mPageDetail = findViewById(R.id.pageDetail);
        mContent = findViewById(R.id.content);
    }
}