package com.xqh.smartcityall.post;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.LibraryBean;
import com.xqh.smartcityall.pojo.PostArea;
import com.xqh.smartcityall.pojo.PostBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.Collections;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initHRV;
import static com.xqh.smartcityall.utils.Const.initRV;

public class PostAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageDetail;
    private androidx.recyclerview.widget.RecyclerView mAreaRv;
    private androidx.recyclerview.widget.RecyclerView mPostRv;

    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_post;
    }

    @Override
    public void initData() {
        PostAreaAdapter postAreaAdapter = new PostAreaAdapter(activity, null);
        initGRV(mAreaRv, postAreaAdapter, 3);
        get("/prod-api/api/youth-inn/talent-policy-area/list", res -> {
            runOnUiThread(()->{
                PostArea bean = new Gson().fromJson(res, PostArea.class);
                if(bean.getCode()==200) {
                    postAreaAdapter.setData(bean.getData());
                }
            });
        });
        PostAdapter postAdapter = new PostAdapter(activity, null);
        initRV(mPostRv, postAdapter);
        get("/prod-api/api/youth-inn/youth-inn/list", res -> {
            runOnUiThread(()->{
                PostBean bean = new Gson().fromJson(res, PostBean.class);
                if(bean.getCode()==200) {
                    postAdapter.setData(bean.getRows());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageDetail = findViewById(R.id.pageDetail);
        mAreaRv = findViewById(R.id.areaRv);
        mPostRv = findViewById(R.id.postRv);
    }
}