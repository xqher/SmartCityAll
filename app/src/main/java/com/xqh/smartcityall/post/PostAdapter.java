package com.xqh.smartcityall.post;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.PostBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.VH> {


    private List<PostBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public PostBean.RowsBean posBean;

    public PostAdapter(Context ctx, List<PostBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<PostBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_post, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_post;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mPostIntroduce;

        public VH(@NonNull View itemView) {
            super(itemView);

            initView();
        }

        public void onItem(PostBean.RowsBean bean) {

            loadImg(bean.getCoverImgUrl(), mCover);
            mTitle.setText(getStr(bean.getName()));
            mContent.setText(String.format("剩余床位: 男(%s)\t女(%s)\n地址:%s", bean.getBedsCountBoy(), bean.getBedsCountGirl(), bean.getAddress()));
            mPostIntroduce.setOnClickListener(v -> {
                if(mPostIntroduce.getText().toString().equals("站点介绍")){
                    mPostIntroduce.setText(getStr(bean.getIntroduce()));
                }else{
                    mPostIntroduce.setText("站点介绍");
                }
            });
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(PostDetailAc.class, bean.getId());
            });
        }

        private void initView() {
            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mPostIntroduce = findViewById(R.id.postIntroduce);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
