package com.xqh.smartcityall.page;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.pojo.NewsDetail;
import com.xqh.smartcityall.utils.BaseAc;
import static com.xqh.smartcityall.utils.Const.*;
public class NewsDetailAc extends BaseAc {


    private android.widget.TextView mPageTitle;
    private android.widget.TextView mPageStatus;
    private android.widget.ImageView mCover;
    private android.webkit.WebView mContent;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "新闻详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_news_detail;
    }

    @Override
    public void initData() {
        get("/prod-api/press/press/"+getJumpId(),res -> {
            runOnUiThread(()->{
                NewsDetail bean = new Gson().fromJson(res, NewsDetail.class);
                if(bean.getCode()==200) {
                    NewsDetail.DataBean data = bean.getData();
                    loadImg(data.getCover(), mCover);
                    mPageTitle.setText(getStr(data.getTitle()));
                    mPageStatus.setText(getStr(data.getPublishDate()));
                    mContent.loadDataWithBaseURL(URL, data.getContent(), "text/html", "utf-8", null);
                }
            });
        });
        NewsAdapter newsAdapter = new NewsAdapter(activity, null);
        newsAdapter.onClick=()->{
            jumpId(NewsDetailAc.class, newsAdapter.posBean.getId());
        };
        initRV(mListRv, newsAdapter);

        get("/prod-api/press/press/list?top=Y", res -> {
            runOnUiThread(()->{
                NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                if(bean.getCode()==200) {
                    newsAdapter.setData(bean.getRows());
                }
            });
        });
        mTodoBt.setOnClickListener(v -> jumpId(NewsCommentAc.class, getJumpId()));
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mPageTitle = findViewById(R.id.pageTitle);
        mPageStatus = findViewById(R.id.pageStatus);
        mCover = findViewById(R.id.cover);
        mContent = findViewById(R.id.content);
        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}