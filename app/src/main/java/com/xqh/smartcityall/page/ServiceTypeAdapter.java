package com.xqh.smartcityall.page;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.park.ParkAc;
import com.xqh.smartcityall.pojo.TypeBean;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.EmptyAc;

import java.lang.reflect.Type;
import java.util.List;

public class ServiceTypeAdapter extends RecyclerView.Adapter<ServiceTypeAdapter.VH> {


    private List<TypeBean> list;
    private Context ctx;
    public Runnable onClick;
    public TypeBean posBean;
    private int position = 0;

    public ServiceTypeAdapter(Context ctx, List<TypeBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<TypeBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_tv, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_tv;
        private TextView mNameTv;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(TypeBean bean, int pos) {
            mNameTv.setText(bean.getName());
            itemView.setBackgroundColor(bean.isChecked()? Color.WHITE:0xcccccc);
            itemView.setOnClickListener(v -> {
                posBean = bean;
                list.get(position).setChecked(false);
                notifyItemChanged(position);
                position = pos;
                list.get(position).setChecked(true);
                notifyItemChanged(position);
                if (onClick != null) {
                    onClick.run();
                }

            });
        }

        private void initView() {
            mNameTv = itemView.findViewById(R.id.name_tv);
        }
    }
}
