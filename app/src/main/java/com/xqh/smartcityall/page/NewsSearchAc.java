package com.xqh.smartcityall.page;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.initRV;

public class NewsSearchAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;

    @Override
    public String initTitle() {
        return "搜索结果";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {

        NewsAdapter newsAdapter = new NewsAdapter(activity, null);
        newsAdapter.onClick=()->{
            jumpId(NewsDetailAc.class, newsAdapter.posBean.getId());
        };
        initRV(mListRv, newsAdapter);

        get("/prod-api/press/press/list", res -> {
            runOnUiThread(()->{
                NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                List<NewsBean.RowsBean> news = new ArrayList<>();
                if(bean.getCode()==200) {
                    for(NewsBean.RowsBean row:bean.getRows()) {
                        if(row.getTitle().contains(getJumpId())) {
                            news.add(row);
                        }
                    }
                }
                newsAdapter.setData(news);
                if(news.size()==0) {
                    toast("未查询到相应结果");
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
    }
}