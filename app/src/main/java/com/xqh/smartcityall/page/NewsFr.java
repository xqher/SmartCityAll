package com.xqh.smartcityall.page;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AdvBanner;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.utils.BaseFr;
import com.youth.banner.Banner;

import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initTargetBanner;

public class NewsFr extends BaseFr {


    private Banner mBanner;
    private TabLayout mTl;
    private RecyclerView mListRv;

    @Override
    public int initLayout() {
        return R.layout.fragment_news;
    }

    @Override
    public void initData() {


        get("/prod-api/api/rotation/list?type=2", res -> {
            runOnUi(()->{
                AdvBanner bean = new Gson().fromJson(res, AdvBanner.class);
                if(bean.getCode()==200) {
                    initTargetBanner(mBanner, bean.getRows(), targetId->{
                        jumpId(NewsDetailAc.class, targetId);
                    });
                }
            });
        });

        get("/prod-api/press/category/list", res -> {
            runOnUi(()->{
                DefType bean = new Gson().fromJson(res, DefType.class);
                if(bean.getCode()==200) {
                    for(DefType.DataBean type:bean.getData()) {
                        mTl.addTab(mTl.newTab().setText(type.getName()).setTag(type.getId()));
                    }
                }
            });
        });

        NewsAdapter newsAdapter = new NewsAdapter(activity, null);
        newsAdapter.onClick=()->{
            jumpId(NewsDetailAc.class, newsAdapter.posBean.getId());
        };
        initRV(mListRv, newsAdapter);

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                get("/prod-api/press/press/list?type="+tab.getTag(), res -> {
                    runOnUi(()->{
                        NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                        if(bean.getCode()==200) {
                            newsAdapter.setData(bean.getRows());
                        }
                    });
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
    }
}
