package com.xqh.smartcityall.page;

import android.app.AlertDialog;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.pojo.TypeBean;
import com.xqh.smartcityall.utils.BaseFr;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.*;

public class ServeFr extends BaseFr {


    private SearchView mSv;
    private RecyclerView mServeRv;
    private RecyclerView mListRv;
    private List<ServeBean.RowsBean> serves = new ArrayList<>();
    private HashMap<String,List<ServeBean.RowsBean>> map = new HashMap<>();
    private ServiceAdapter iconAdapter;
    private ServiceTypeAdapter typeAdapter;
    @Override
    public int initLayout() {
        return R.layout.fragment_serve;
    }

    @Override
    public void initData() {


        typeAdapter = new ServiceTypeAdapter(activity, null);
        typeAdapter.onClick = ()->{
          iconAdapter.setData(map.get(typeAdapter.posBean.getName()));
        };
        initRV(mServeRv, typeAdapter);

        iconAdapter = new ServiceAdapter(activity, null);
        initGRV(mListRv, iconAdapter, 3);

        initSv(mSv, res -> {
            List<ServeBean.RowsBean> tmp = new ArrayList<>();
            for(ServeBean.RowsBean serve:serves) {
               if(serve.getServiceName().contains(res)) {
                   tmp.add(serve);
               }

            }
            toast("查找到"+tmp.size()+"条服务");

            if(tmp.size()>0) {

                RecyclerView rv = new RecyclerView(activity);
                ServiceAdapter adapter = new ServiceAdapter(activity, null);
                initGRV(rv, adapter, 3);
                adapter.setData(tmp);

                AlertDialog dialog = new AlertDialog.Builder(activity)
                        .setTitle("搜索结果")
                        .setView(rv)
                        .setPositiveButton("取消",null)
                        .create();
                dialog.show();
            }


        });
        get("/prod-api/api/service/list", res -> {
            runOnUi(()->{
                ServeBean bean = new Gson().fromJson(res, ServeBean.class);
                if(bean.getCode()==200) {
                    serves = bean.getRows();
                    // 处理124 服务器获取服务不全
                    if(!URL.startsWith("218")) {
                        serves.add(new ServeBean.RowsBean("垃圾分类","生活服务"));
                        serves.add(new ServeBean.RowsBean("数字图书馆","生活服务"));
                        serves.add(new ServeBean.RowsBean("政府服务热线","生活服务"));
                        serves.add(new ServeBean.RowsBean("法律咨询","生活服务"));
                        serves.add(new ServeBean.RowsBean("预约检车","车主服务"));
                        serves.add(new ServeBean.RowsBean("志愿服务","生活服务"));
                    }
                    List<TypeBean> types = new ArrayList<>();
                    for(ServeBean.RowsBean serve:serves) {
                        if(map.get(serve.getServiceType())==null) {
                            TypeBean type = new TypeBean(serve.getServiceType());
                            if(serve.getServiceType().equals("车主服务")) type.setChecked(true);
                            types.add(type);
                            map.put(serve.getServiceType(), new ArrayList<>());
                        }
                        map.get(serve.getServiceType()).add(serve);
                    }
                    typeAdapter.setData(types);
                    iconAdapter.setData(map.get("车主服务"));
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mSv = activity.findViewById(R.id.sv);
        mServeRv = activity.findViewById(R.id.serveRv);
        mListRv = activity.findViewById(R.id.listRv);
    }
}
