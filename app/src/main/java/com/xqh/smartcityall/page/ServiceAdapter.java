package com.xqh.smartcityall.page;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.active.ActiveAc;
import com.xqh.smartcityall.appeal.AppealAc;
import com.xqh.smartcityall.bus.BusAc;
import com.xqh.smartcityall.charity.CharityAc;
import com.xqh.smartcityall.chart.ChartAc;
import com.xqh.smartcityall.hospital.HospitalAc;
import com.xqh.smartcityall.house.HouseAc;
import com.xqh.smartcityall.lawyer.LawyerAc;
import com.xqh.smartcityall.library.LibraryAc;
import com.xqh.smartcityall.park.ParkAc;
import com.xqh.smartcityall.park.ParkRecordAc;
import com.xqh.smartcityall.per.LoginAc;
import com.xqh.smartcityall.pojo.ParkRecord;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.pojo.ServeBean.RowsBean;
import com.xqh.smartcityall.post.PostAc;
import com.xqh.smartcityall.trash.TrashAc;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.EmptyAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.isEmpty;
import static com.xqh.smartcityall.utils.Const.isLogin;
import static com.xqh.smartcityall.utils.Const.loadImg;
import static com.xqh.smartcityall.utils.Const.setSp;

public class ServiceAdapter extends RecyclerView.Adapter<ServiceAdapter.VH> {


    private List<ServeBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public ServeBean.RowsBean posBean;

    public ServiceAdapter(Context ctx, List<ServeBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }
    public void setData(List<ServeBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_icon, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_icon;
        private ImageView mIcon;
        private TextView mName;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(ServeBean.RowsBean bean) {
            if(!isEmpty(bean.getImgUrl())) {
                loadImg(bean.getImgUrl(), mIcon);
            }else{
                mIcon.setImageResource(bean.getResId()==0?R.drawable.more_serve:bean.getResId());
            }
            mName.setText(getStr(bean.getServiceName()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                String serveName = bean.getServiceName();
                MainActivity ac = ((MainActivity)ctx);
                if (onClick != null) {
                    onClick.run();
                }
                if(serveName.equals("更多服务")) {
                    ac.showMore();
                    return;
                }

                boolean[] ans = {false};
                isLogin(ans, ()->{
                    if(ans[0]) {

                        if(jumpFuc(serveName, "停哪儿", ParkAc.class)) return;
                        if(jumpFuc(serveName, "数字图书馆", LibraryAc.class)) return;
                        if(jumpFuc(serveName, "青年驿站", PostAc.class)) return;
                        if(jumpFuc(serveName, "活动管理", ActiveAc.class)) return;
                        if(jumpFuc(serveName, "爱心捐赠", CharityAc.class)) return;
                        if(jumpFuc(serveName, "政府服务热线", AppealAc.class)) return;
                        if(jumpFuc(serveName, "数据分析", ChartAc.class)) return;
                        if(jumpFuc(serveName, "找房子", HouseAc.class)) return;
                        if(jumpFuc(serveName, "门诊预约", HospitalAc.class)) return;
                        if(jumpFuc(serveName, "垃圾分类", TrashAc.class)) return;
                        if(jumpFuc(serveName, "法律咨询", LawyerAc.class)) return;
                        if(jumpFuc(serveName, "智慧巴士", BusAc.class)) return;
                        if(jumpFuc(serveName, "预约检车", BusAc.class)) return;
                        if(jumpFuc(serveName, "看电影", BusAc.class)) return;
                        if(jumpFuc(serveName, "志愿服务", BusAc.class)) return;
                        if(jumpFuc(serveName, "宠物医院", BusAc.class)) return;
                        if(jumpFuc(serveName, "城市地铁", BusAc.class)) return;
                        if(jumpFuc(serveName, "物流查询", BusAc.class)) return;
                        if(jumpFuc(serveName, "智慧交管", BusAc.class)) return;
                        if(jumpFuc(serveName, "生活缴费", BusAc.class)) return;
                        if(jumpFuc(serveName, "找工作", BusAc.class)) return;

                        ((BaseAc)ctx).jumpId(EmptyAc.class, serveName);
                    } else{
                        ((BaseAc)ctx).toast("未登录或网络设置错误");
                        setSp("token","");
                        ((BaseAc)ctx).jump(LoginAc.class);
                    }
                });

            });
        }
        private boolean jumpFuc(String judge, String name, Class<?> clz) {
            boolean flag = judge.equals(name);
            if(flag) {
                ((MainActivity)ctx).jumpId(clz, judge);
            }
            return flag;
        }
        private void initView() {
            mIcon = itemView.findViewById(R.id.icon);
            mName = itemView.findViewById(R.id.name);
        }
    }
}
