package com.xqh.smartcityall.page;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.per.LoginAc;
import com.xqh.smartcityall.pojo.AdvBanner;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.utils.BaseFr;
import com.xqh.smartcityall.utils.Const;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.*;

public class HomeFr extends BaseFr {
    private Banner mBanner;
    private SearchView mSv;
    private RecyclerView mIconRv;
    private RecyclerView mTopicRv;
    private TabLayout mTl;
    private RecyclerView mListRv;


    @Override
    public int initLayout() {
        return R.layout.fragment_home;
    }

    @Override
    public void initData() {

        get("/prod-api/api/rotation/list?type=2", res -> {
            runOnUi(()->{
                AdvBanner bean = new Gson().fromJson(res, AdvBanner.class);
                if(bean.getCode()==200) {
                    initTargetBanner(mBanner, bean.getRows(), targetId->{
                        jumpId(NewsDetailAc.class, targetId);
                    });
                }
            });
        });

        initSv(mSv, res -> {
            jumpId(NewsSearchAc.class, res);
        });


        ServiceAdapter iconAdapter = new ServiceAdapter(activity, null);
        initGRV(mIconRv, iconAdapter, 5);

        get("/prod-api/api/service/list?pageNum=1&pageSize=9", res -> {
            runOnUi(()->{
                ServeBean bean = new Gson().fromJson(res, ServeBean.class);
                if(bean.getCode()==200) {
                   List<ServeBean.RowsBean> serves = bean.getRows();
                   List<ServeBean.RowsBean> show = new ArrayList<>();
                   for(int i=serves.size()-1;i>serves.size()-10;i--) {
                       show.add(serves.get(i));
                   }
                   ServeBean.RowsBean sb = new ServeBean.RowsBean();
                   sb.setServiceName("更多服务");
                   show.add(sb);
                   iconAdapter.setData(show);
                }
            });
        });

        TopicAdapter topicAdapter = new TopicAdapter(activity, null);
        topicAdapter.onClick=()->{
            jumpId(NewsDetailAc.class, topicAdapter.posBean.getId());
        };
        initGRV(mTopicRv, topicAdapter, 2);
        get("/prod-api/press/press/list?hot=Y", res -> {
            runOnUi(()->{
                NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                if(bean.getCode()==200) {
                    topicAdapter.setData(bean.getRows());
                }
            });
        });

        get("/prod-api/press/category/list", res -> {
            runOnUi(()->{
                DefType bean = new Gson().fromJson(res, DefType.class);
                if(bean.getCode()==200) {
                    for(DefType.DataBean type:bean.getData()) {
                        mTl.addTab(mTl.newTab().setText(type.getName()).setTag(type.getId()));
                    }
                }
            });
        });

        NewsAdapter newsAdapter = new NewsAdapter(activity, null);
        newsAdapter.onClick=()->{
          jumpId(NewsDetailAc.class, newsAdapter.posBean.getId());
        };
        initRV(mListRv, newsAdapter);

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                get("/prod-api/press/press/list?type="+tab.getTag(), res -> {
                    runOnUi(()->{
                        NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                        if(bean.getCode()==200) {
                            newsAdapter.setData(bean.getRows());
                        }
                    });
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



    }

    @Override
    public void resumeData() {
        boolean[] login = {false};
        Const.URL = getSp("url");
        Const.isLogin(login, ()->{
            if(!login[0]){
                toast("未登录或网络设置错误！");
                setSp("token","");
                jump(LoginAc.class);
                activity.finish();
            }
        });
    }

    @Override
    public void initView() {
        mBanner = activity.findViewById(R.id.banner);
        mSv = activity.findViewById(R.id.sv);
        mIconRv = activity.findViewById(R.id.iconRv);
        mTopicRv = activity.findViewById(R.id.topicRv);
        mTl = activity.findViewById(R.id.tl);
        mListRv = activity.findViewById(R.id.listRv);
    }
}
