package com.xqh.smartcityall.page;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.pojo.NewsBean;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class TopicAdapter extends RecyclerView.Adapter<TopicAdapter.VH> {


    private List<NewsBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public NewsBean.RowsBean posBean;

    public TopicAdapter(Context ctx, List<NewsBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<NewsBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_topic, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_topic;
        private ImageView mCover;
        private TextView mTitle;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(NewsBean.RowsBean bean) {

            loadImg(bean.getCover(), mCover);
            mTitle.setText(getStr(bean.getTitle()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }

            });
        }

        private void initView() {
            mCover = itemView.findViewById(R.id.cover);
            mTitle = itemView.findViewById(R.id.title);
        }
    }
}
