package com.xqh.smartcityall.hospital;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.HospitalCard;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class HospitalCardAdapter extends RecyclerView.Adapter<HospitalCardAdapter.VH> {


    private List<HospitalCard.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public HospitalCard.RowsBean posBean;

    public HospitalCardAdapter(Context ctx, List<HospitalCard.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<HospitalCard.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_comment;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mAvatar;
        private TextView mName;
        private TextView mContent;
        private ImageView mToLike;
        private TextView mLikeNum;
        private TextView mTime;
        private TextView mStatus;


        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mAvatar.setVisibility(View.GONE);
            mLikeNum.setVisibility(View.GONE);
            mTime.setVisibility(View.GONE);
            mStatus.setVisibility(View.GONE);
            mContent.setText("点击列表修改就诊卡片信息");
        }

        public void onItem(HospitalCard.RowsBean bean) {

            boolean isFinish = bean.getAddress()!=null&&bean.getBirthday()!=null;

            mToLike.setImageResource(isFinish?R.drawable.ic_double_arrow:R.drawable.ic_edit);
            mName.setText(isFinish?bean.getName():"未完善资料，点击完善资料");
            mToLike.setOnClickListener(v -> {
                Const.USER_NAME = bean.getName();
                if (isFinish){
                   activity.jump(HospitalDepartmentAc.class);
                }else{
                    activity.jumpId(HospitalCardCreateAc.class, new Gson().toJson(bean));
                }
            });
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(HospitalCardCreateAc.class, new Gson().toJson(bean));
            });
        }

        private void initView() {

            mAvatar = findViewById(R.id.avatar);
            mName = findViewById(R.id.name);
            mContent = findViewById(R.id.content);
            mToLike = findViewById(R.id.toLike);
            mLikeNum = findViewById(R.id.likeNum);
            mTime = findViewById(R.id.time);
            mStatus = findViewById(R.id.status);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
