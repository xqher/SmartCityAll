package com.xqh.smartcityall.hospital;

import android.app.DatePickerDialog;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HospitalCard;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.Calendar;
import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.isEmpty;

public class HospitalCardCreateAc extends BaseAc {


    private com.google.android.material.textfield.TextInputEditText mUserNameEt;
    private com.google.android.material.textfield.TextInputEditText mAddressEdit;
    private com.google.android.material.textfield.TextInputEditText mIdEdit;
    private com.google.android.material.textfield.TextInputEditText mPhoneEt;
    private android.widget.TextView mBirthdayTv;
    private android.widget.RadioGroup mSexRg;
    private android.widget.RadioButton mMale;
    private android.widget.RadioButton mFemale;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "修改就诊人卡片";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_hospital_card_create;
    }

    @Override
    public void initData() {

        HospitalCard.RowsBean bean = new Gson().fromJson(getJumpId(), HospitalCard.RowsBean.class);
        mAddressEdit.setText(bean.getAddress()!=null?bean.getAddress():"");
        mBirthdayTv.setText(bean.getBirthday()!=null?bean.getBirthday():"点击设置生日日期");
        mIdEdit.setText(bean.getCardId()!=null?bean.getCardId():"");
        mUserNameEt.setText(bean.getName()!=null?bean.getName():"");
        mPhoneEt.setText(bean.getTel()!=null?bean.getTel():"");
        if(bean.getSex().equals("0")){
            mMale.setChecked(true);
        }else{
            mFemale.setChecked(true);
        }

        mBirthdayTv.setOnClickListener(v -> {
            Calendar calendar = Calendar.getInstance();
            DatePickerDialog dialog = new DatePickerDialog(this,
                    (view, year, month, dayOfMonth) ->
                            mBirthdayTv.setText(String.format("%s-%s-%s",year,month+1,dayOfMonth)),
                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH) + 1, calendar.get(Calendar.DAY_OF_MONTH));
            dialog.setTitle("设置生日日期");
            dialog.create();
            dialog.show();
        });

        mTodoBt.setOnClickListener(v->{
            String userName = mUserNameEt.getText().toString();
            String cardId = mIdEdit.getText().toString();
            String address = mAddressEdit.getText().toString();
            String birthday = mBirthdayTv.getText().toString();
            String phone = mPhoneEt.getText().toString();
            String sex = mMale.isChecked() ? "0" : "1";

            if(birthday.equals("点击设置生日日期")||isEmpty(userName)||isEmpty(cardId)||isEmpty(address)||isEmpty(phone)){
                toast("未输入完成");
            }else{
                HashMap<String, String> map = new HashMap<>();
                map.put("name", userName);
                map.put("birthday", birthday);
                map.put("cardId", cardId);
                map.put("address", address);
                map.put("tel", phone);
                map.put("sex", sex);
                if(bean.getId()!=0){
                    map.put("id", bean.getId()+"");
                    put("/prod-api/api/hospital/patient", map, res -> {
                        runOnUiThread(()->{
                            DefRes resp = new Gson().fromJson(res, DefRes.class);
                            toast(resp.getMsg());
                            if(resp.getCode()==200) {
                                finish();
                            }
                        });
                    });
                }else{
                    post("/prod-api/api/hospital/patient", map, res -> {
                        runOnUiThread(()->{
                            DefRes resp = new Gson().fromJson(res, DefRes.class);
                            toast(resp.getMsg());
                            if(resp.getCode()==200) {
                                finish();
                            }
                        });
                    });
                }
            }



        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mUserNameEt = findViewById(R.id.userNameEt);
        mAddressEdit = findViewById(R.id.addressEdit);
        mIdEdit = findViewById(R.id.idEdit);
        mPhoneEt = findViewById(R.id.phoneEt);
        mBirthdayTv = findViewById(R.id.birthdayTv);
        mSexRg = findViewById(R.id.sexRg);
        mMale = findViewById(R.id.male);
        mFemale = findViewById(R.id.female);
        mTodoBt = findViewById(R.id.todoBt);
    }
}