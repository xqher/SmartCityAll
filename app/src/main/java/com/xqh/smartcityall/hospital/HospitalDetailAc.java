package com.xqh.smartcityall.hospital;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HospitalDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initWebView;

public class HospitalDetailAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private android.webkit.WebView mContent;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "医院详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_hospital_detail;
    }

    @Override
    public void initData() {

        get("/prod-api/api/hospital/banner/list", res -> {
            runOnUiThread(() -> {
                DefBanner bean = new Gson().fromJson(res, DefBanner.class);
                if (bean.getCode() == 200) {
                    List<DefBanner.DataBean> tmp = new ArrayList<>();
                    for(DefBanner.DataBean data:bean.getData()) {
                        if(data.getHospitalId()==data.getHospitalId()) {
                            tmp.add(data);
                        }
                    }
                    initBanner(mBanner, tmp, id->{});
                }
            });
        });
        get("/prod-api/api/hospital/hospital/"+getJumpId(), res -> {
            runOnUiThread(() -> {
                HospitalDetail bean = new Gson().fromJson(res, HospitalDetail.class);
                if (bean.getCode() == 200) {
                    HospitalDetail.DataBean data = bean.getData();
                    initWebView(mContent, data.getBrief());
                }
            });
        });
        mTodoBt.setOnClickListener(v -> jumpId(HospitalCardListAc.class, getJumpId()));

    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mContent = findViewById(R.id.content);
        mTodoBt = findViewById(R.id.todoBt);
    }
}