package com.xqh.smartcityall.hospital;

import android.os.Handler;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HospitalCard;
import com.xqh.smartcityall.pojo.UserInfo;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initRV;

public class HospitalCardListAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;

    private List<HospitalCard.RowsBean> cards = new ArrayList<>();
    private HospitalCardAdapter adapter;
    @Override
    public String initTitle() {
        return "就诊人卡片";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_hospital_card_list;
    }

    @Override
    public void initData() {

        adapter = new HospitalCardAdapter(activity, null);
        initRV(mListRv, adapter);

        mTodoBt.setOnClickListener(v -> {
            get("/prod-api/api/common/user/getInfo", res -> {
                runOnUiThread(() -> {
                    UserInfo bean = new Gson().fromJson(res, UserInfo.class);
                    if(bean.getCode()==200) {
                        UserInfo.UserBean user = bean.getUser();
                        HospitalCard.RowsBean wr = new HospitalCard.RowsBean();
                        wr.setName(user.getUserName());
                        wr.setCardId(user.getIdCard());
                        wr.setSex(user.getSex());
                        wr.setTel(user.getPhonenumber());
                        jumpId(HospitalCardCreateAc.class, new Gson().toJson(wr));
                    }
                });
            });
        });

    }

    @Override
    public void resumeData() {

        get("/prod-api/api/hospital/patient/list", res -> {
            runOnUiThread(() -> {
                HospitalCard bean = new Gson().fromJson(res, HospitalCard.class);
                if (bean.getCode() == 200) {
                    cards = bean.getRows();
                    adapter.setData(bean.getRows());
                }
            });
        });
        new Handler().postDelayed(()->{
            if(cards.size()==0){
                get("/prod-api/api/common/user/getInfo", res -> {
                    runOnUiThread(() -> {
                        UserInfo bean = new Gson().fromJson(res, UserInfo.class);
                        if(bean.getCode()==200) {
                            UserInfo.UserBean user = bean.getUser();
                            HospitalCard.RowsBean wr = new HospitalCard.RowsBean();
                            wr.setName(user.getUserName());
                            wr.setCardId(user.getIdCard());
                            wr.setSex(user.getSex());
                            wr.setTel(user.getPhonenumber());
                            cards.add(wr);
                            adapter.setData(cards);
                            jumpId(HospitalCardCreateAc.class, new Gson().toJson(wr));
                        }
                    });
                });
            }
        },500);
    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}