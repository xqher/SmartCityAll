package com.xqh.smartcityall.hospital;

import com.google.gson.Gson;
import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.Hospital;
import com.xqh.smartcityall.pojo.HospitalRegister;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;

public class HospitalRegisterResAc extends BaseAc {


    private android.widget.TextView mPageDesc;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "挂号信息";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_hospital_register_res;
    }

    @Override
    public void initData() {
        get("/prod-api/api/hospital/reservation/list", res -> {
            runOnUiThread(() -> {
                HospitalRegister bean = new Gson().fromJson(res, HospitalRegister.class);
                if (bean.getCode() == 200) {
                    if(bean.getRows().size()>0) {
                        HospitalRegister.RowsBean data = bean.getRows().get(bean.getRows().size()-1);
                        mPageDesc.setText(getStrHtml(
                                "<h4>预约科室</h4>&emsp;%s<h4>门诊类型</h4>&emsp;%s<h4>预约时间</h4>&emsp;%s",
                                getStr(data.getCategoryName()),
                                getStr(data.getType()).equals("1")?"普通号":"专家号",
                                getStr(data.getReserveTime())
                        ));
                    }
                }
            });
        });
        mTodoBt.setOnClickListener(v -> {
            jump(MainActivity.class);
            finish();
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mPageDesc = findViewById(R.id.pageDesc);
        mTodoBt = findViewById(R.id.todoBt);
    }
}