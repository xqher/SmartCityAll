package com.xqh.smartcityall.hospital;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HospitalDeparment;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initRV;

public class HospitalDepartmentAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;

    private HospitalDepartmentAdapter listAdapter;
    @Override
    public String initTitle() {
        return "科室列表";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {
        listAdapter = new HospitalDepartmentAdapter(activity, null);
        initRV(mListRv, listAdapter);

        get("/prod-api/api/hospital/category/list?type=2", res -> {
            runOnUiThread(() -> {
                HospitalDeparment bean = new Gson().fromJson(res, HospitalDeparment.class);
                if (bean.getCode() == 200) {
                    listAdapter.setData(bean.getRows());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}