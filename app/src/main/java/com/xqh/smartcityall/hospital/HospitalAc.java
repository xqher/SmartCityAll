package com.xqh.smartcityall.hospital;

import android.app.AlertDialog;

import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.Hospital;
import com.xqh.smartcityall.pojo.HouseBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class HospitalAc extends BaseAc {


    private androidx.appcompat.widget.SearchView mSv;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private HospitalAdapter listAdapter;
    private List<Hospital.RowsBean> hospitals = new ArrayList<>();
    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_hospital;
    }

    @Override
    public void initData() {


        initSv(mSv, res -> {
            if(hospitals.size()>0) {
                List<Hospital.RowsBean> tmp = new ArrayList<>();
                for(Hospital.RowsBean house:hospitals) {
                    if(getStr(house.getHospitalName()).contains(res)) {
                        tmp.add(house);
                    }
                }
                if(tmp.size()==0) {
                    toast("未查找到相关数据");
                    return;
                }
                RecyclerView searchRv = new RecyclerView(activity);
                HospitalAdapter searchAdapter = new HospitalAdapter(activity, tmp);
                initRV(searchRv, searchAdapter);

                new AlertDialog.Builder(activity)
                        .setTitle("搜索到" + tmp.size() + "条")
                        .setView(searchRv)
                        .setPositiveButton("关闭", null)
                        .create().show();
            }else {
                toast("未加载完数据");
            }
        });

        listAdapter = new HospitalAdapter(activity, null);
        initRV(mListRv, listAdapter);

        get("/prod-api/api/hospital/hospital/list", res -> {
            runOnUiThread(() -> {
                Hospital bean = new Gson().fromJson(res, Hospital.class);
                hospitals = bean.getRows();
                listAdapter.setData(hospitals);
            });
        });
    }
    @Override
    public void resumeData() {


    }

    @Override
    public void initView() {

        mSv = findViewById(R.id.sv);
        mListRv = findViewById(R.id.listRv);
    }
}