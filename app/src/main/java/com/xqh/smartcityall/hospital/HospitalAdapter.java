package com.xqh.smartcityall.hospital;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.Hospital;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class HospitalAdapter extends RecyclerView.Adapter<HospitalAdapter.VH> {


    private List<Hospital.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public Hospital.RowsBean posBean;

    public HospitalAdapter(Context ctx, List<Hospital.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<Hospital.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_hospital, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_hospital;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private AppCompatRatingBar mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(Hospital.RowsBean bean) {

            loadImg(bean.getImgUrl(), mCover);
            mTitle.setText(getStr(bean.getHospitalName()));
            mContent.setText(getStrHtml("%s", bean.getBrief() + ""));
            mStatus.setRating(bean.getLevel());
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(HospitalDetailAc.class, bean.getId());
            });
        }

        private void initView() {
            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mStatus = findViewById(R.id.status);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
