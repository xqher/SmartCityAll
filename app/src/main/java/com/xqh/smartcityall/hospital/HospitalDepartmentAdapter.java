package com.xqh.smartcityall.hospital;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.HospitalDeparment;
import com.xqh.smartcityall.pojo.HospitalRegister;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class HospitalDepartmentAdapter extends RecyclerView.Adapter<HospitalDepartmentAdapter.VH> {


    private List<HospitalDeparment.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public HospitalDeparment.RowsBean posBean;

    public HospitalDepartmentAdapter(Context ctx, List<HospitalDeparment.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<HospitalDeparment.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_tv, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_tv;
        private BaseAc activity = (BaseAc) ctx;
        private TextView mNameTv;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(HospitalDeparment.RowsBean bean) {

            mNameTv.setText(getStr(bean.getCategoryName()));
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(HospitalRegisterAc.class, new Gson().toJson(bean));
            });
        }

        private void initView() {
            mNameTv = findViewById(R.id.name_tv);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
