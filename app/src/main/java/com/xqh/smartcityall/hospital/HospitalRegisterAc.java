package com.xqh.smartcityall.hospital;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HospitalDeparment;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

import java.util.ArrayList;
import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.getStr;

public class HospitalRegisterAc extends BaseAc {


    private com.google.android.material.tabs.TabLayout mTl;
    private androidx.cardview.widget.CardView mRegisterCard;
    private android.widget.TextView mRegisterType;
    private android.widget.TextView mDate;
    private android.widget.TextView mTime;
    private android.widget.Button mTodoBt;
    private String date, time;
    @Override
    public String initTitle() {
        return "挂号页面";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_hospital_register;
    }

    @Override
    public void initData() {
        HospitalDeparment.RowsBean rowsBean = new Gson().fromJson(getJumpId(), HospitalDeparment.RowsBean.class);


        mRegisterType.setText(rowsBean.getCategoryName());
        mDate.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(activity);
            dateDialog.setOnDateSetListener((view, year, month, dayOfMonth) -> {
                date = year+"-"+(month+1)+"-"+dayOfMonth;
                mDate.setText(date);
            });
            dateDialog.show();
        });

        mTime.setOnClickListener(v -> {
            TimePickerDialog timeDialog = new TimePickerDialog(activity, (TimePickerDialog.OnTimeSetListener) (view, hourOfDay, minute) -> {
                time = hourOfDay+":"+minute;
                mTime.setText(time);
            },12,0,false);
            timeDialog.show();
        });
        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String selected = tab.getText().toString();
                if(selected.equals("专家号")){
                    toast("暂无数据");
                    mRegisterCard.setVisibility(View.GONE);
                }else{
                    mRegisterCard.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        mTodoBt.setOnClickListener(v -> {
            if(mDate.getText().equals("设置日期")||mTime.getText().equals("设置时间")) {
                toast("未设置预约时间");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("name", Const.USER_NAME);
            map.put("categoryId", rowsBean.getId()+"");
            map.put("money", rowsBean.getMoney()+"");
            map.put("patientName", Const.USER_NAME);
            map.put("reserveTime", date+" "+time);
            map.put("type", rowsBean.getType()+"");
            post("/prod-api/api/hospital", map, res -> {
                runOnUiThread(()->{
                    DefRes resp = new Gson().fromJson(res, DefRes.class);
                    toast(resp.getMsg());
                    if(resp.getCode()==200) {
                        jump(HospitalRegisterResAc.class);
                        finish();
                    }
                });
            });

        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mTl = findViewById(R.id.tl);
        mRegisterCard = findViewById(R.id.registerCard);
        mRegisterType = findViewById(R.id.registerType);
        mDate = findViewById(R.id.date);
        mTime = findViewById(R.id.time);
        mTodoBt = findViewById(R.id.todoBt);
    }
}