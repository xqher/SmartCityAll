package com.xqh.smartcityall.park;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ParkLot;
import com.xqh.smartcityall.utils.BaseAc;

public class ParkDetailAc extends BaseAc {


    private android.widget.TextView mParkTitle;
    private android.widget.TextView mLotStatus;
    private android.widget.TextView mParkFee;
    private android.widget.TextView mPosFree;
    private android.widget.TextView mFeeRef;

    @Override
    public String initTitle() {
        return "停车场详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_park_detail;
    }

    @Override
    public void initData() {
        ParkLot.RowsBean bean = new Gson().fromJson(getJumpId(), ParkLot.RowsBean.class);
        mParkTitle.setText(bean.getParkName());
        mLotStatus.setText(String.format("%s\t\t\t\t%skm\n%s",
                bean.getOpen().equals("Y")?"对外开放":"不对外开放",bean.getDistance(),bean.getAddress()));
        mFeeRef.setText("收费参考\n每小时"+bean.getRates()+"元，中最高"+bean.getPriceCaps()+"元一天" );
        mParkFee.setText(bean.getRates()+"元/小时");
        mPosFree.setText(bean.getPriceCaps()+"个/"+bean.getAllPark()+"个");
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mParkTitle = findViewById(R.id.parkTitle);
        mLotStatus = findViewById(R.id.lotStatus);
        mParkFee = findViewById(R.id.parkFee);
        mPosFree = findViewById(R.id.posFree);
        mFeeRef = findViewById(R.id.feeRef);
    }
}