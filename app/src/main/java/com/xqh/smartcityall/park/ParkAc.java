package com.xqh.smartcityall.park;

import android.view.View;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.pojo.ParkLot;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

public class ParkAc extends BaseAc {

    private int cur = 1;
    private int size = 0;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private ParkLotAdapter adapter;
    @Override
    public String initTitle() {
        return "停哪儿";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_park;
    }

    @Override
    public void initData() {
        setOptionClick(v -> jump(ParkRecordAc.class));
        adapter = new ParkLotAdapter(activity, null);
        Const.initRV(mListRv, adapter);
        loadData(cur);
        mTodoBt.setOnClickListener(v -> {
            cur++;
            loadData(cur);
        });

    }
    public void loadData(int page) {
        get("/prod-api/api/park/lot/list?&pageSize="+(page*5)+"&pageNum=1", res -> {
            runOnUiThread(()->{
                ParkLot bean = new Gson().fromJson(res, ParkLot.class);
                if(bean.getCode()==200) {
                    size = bean.getTotal();
                    if(page*5>=size) {
                        toast("已加载全部内容");
                        return;
                    }
                    adapter.setData(bean.getRows());
                }
            });
        });
    }
    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}