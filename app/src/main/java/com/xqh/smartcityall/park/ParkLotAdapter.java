package com.xqh.smartcityall.park;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ParkLot;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ParkLotAdapter extends RecyclerView.Adapter<ParkLotAdapter.VH> {


    private List<ParkLot.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public ParkLot.RowsBean posBean;

    public ParkLotAdapter(Context ctx, List<ParkLot.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<ParkLot.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(ParkLot.RowsBean bean) {

            mCover.setVisibility(View.GONE);

            mTitle.setTextSize(20);
            mTitle.setText(getStr(bean.getParkName()));
            mContent.setText("地址:"+getStr(bean.getAddress()));
            mDate.setText(String.format("空位:%s个 | 停车费:%s元/小时", bean.getVacancy(), bean.getRates()));
            mStatus.setText(bean.getDistance()+"km");

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                ((BaseAc)ctx).jumpId(ParkDetailAc.class, new Gson().toJson(bean));
            });
        }

        private void initView() {
            mCover = itemView.findViewById(R.id.cover);
            mTitle = itemView.findViewById(R.id.title);
            mContent = itemView.findViewById(R.id.content);
            mDate = itemView.findViewById(R.id.date);
            mStatus = itemView.findViewById(R.id.status);
        }
    }
}
