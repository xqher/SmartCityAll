package com.xqh.smartcityall.park;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.widget.DatePicker;
import android.widget.TimePicker;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ParkLot;
import com.xqh.smartcityall.pojo.ParkRecord;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

public class ParkRecordAc extends BaseAc {


    private int cur = 1;
    private int size = 0;
    private android.widget.TextView mEnterDate;
    private android.widget.TextView mEnterTime;
    private android.widget.TextView mOutDate;
    private android.widget.TextView mOutTime;
    private android.widget.Button mSearchBt;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private String entryDate = "2000-01-01";
    private String entryTime = "12:00";
    private String outDate = "2022-01-01";
    private String outTime = "12:00";
    private ParkRecordAdapter adapter;

    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_park_record;
    }

    @Override
    public void initData() {
        adapter = new ParkRecordAdapter(activity, null);
        Const.initRV(mListRv, adapter);

        mEnterDate.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(activity);
            dateDialog.setOnDateSetListener((view, year, month, dayOfMonth) -> {
                entryDate = year+"-"+(month+1)+"-"+dayOfMonth;
                mEnterDate.setText(entryDate);
            });
            dateDialog.show();
        });

        mOutDate.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(activity);
            dateDialog.setOnDateSetListener((view, year, month, dayOfMonth) -> {
                outDate = year+"-"+(month+1)+"-"+dayOfMonth;
                mOutDate.setText(outDate);
            });
            dateDialog.show();
        });

        mEnterTime.setOnClickListener(v -> {
            TimePickerDialog timeDialog = new TimePickerDialog(activity, (TimePickerDialog.OnTimeSetListener) (view, hourOfDay, minute) -> {
                entryTime = hourOfDay+":"+minute;
                mEnterTime.setText(entryTime);
            },12,0,false);
            timeDialog.show();
        });
        mOutTime.setOnClickListener(v -> {
            TimePickerDialog timeDialog = new TimePickerDialog(activity, (TimePickerDialog.OnTimeSetListener) (view, hourOfDay, minute) -> {
                outTime = hourOfDay+":"+minute;
                mOutTime.setText(outTime);
            },12,0,false);
            timeDialog.show();
        });

        mSearchBt.setOnClickListener(v -> {
            cur = 1;
            loadData(cur);
        });
        loadData(cur);
        mTodoBt.setOnClickListener(v -> {
            cur++;
            loadData(cur);
        });

    }
    public void loadData(int page) {
        get("/prod-api/api/park/lot/record/list?entryTime="+(entryDate+" "+entryTime)+"&outTime="+(outDate+" "+outTime)+"&pageSize="+(page*5)+"&pageNum=1", res -> {
            runOnUiThread(()->{
                ParkRecord bean = new Gson().fromJson(res, ParkRecord.class);
                if(bean.getCode()==200) {
                    size = bean.getTotal();
                    adapter.setData(bean.getRows());
                    if(page*5>=size) {
                        toast("已加载全部内容");
                    }
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mEnterDate = findViewById(R.id.enterDate);
        mEnterTime = findViewById(R.id.enterTime);
        mOutDate = findViewById(R.id.outDate);
        mOutTime = findViewById(R.id.outTime);
        mSearchBt = findViewById(R.id.searchBt);
        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}