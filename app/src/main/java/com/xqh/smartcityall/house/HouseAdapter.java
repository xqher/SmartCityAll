package com.xqh.smartcityall.house;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.active.ActiveDetailAc;
import com.xqh.smartcityall.pojo.ActiveBean;
import com.xqh.smartcityall.pojo.HouseBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class HouseAdapter extends RecyclerView.Adapter<HouseAdapter.VH> {


    private List<HouseBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public HouseBean.RowsBean posBean;

    public HouseAdapter(Context ctx, List<HouseBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<HouseBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(HouseBean.RowsBean bean) {

            loadImg(getStr(bean.getPic()), mCover);
            mTitle.setText(getStr(bean.getSourceName()));
            mContent.setText(getStrHtml("%s",bean.getDescription()+""));
            mStatus.setText(String.format("房源面积:%s ㎡", bean.getAreaSize()+""));
            mDate.setText(String.format("房源价格:%s", bean.getPrice()+""));
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(HouseDetailAc.class, bean.getId());
            });
        }

        private void initView() {
            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mDate = findViewById(R.id.date);
            mStatus = findViewById(R.id.status);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
