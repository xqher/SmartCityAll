package com.xqh.smartcityall.house;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.charity.CharityListAc;
import com.xqh.smartcityall.charity.CharitySearchAc;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.HouseBean;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class HouseAc extends BaseAc {


    private androidx.appcompat.widget.SearchView mSv;
    private androidx.recyclerview.widget.RecyclerView mIconRv;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private HouseAdapter listAdapter;
    private List<HouseBean.RowsBean> houses = new ArrayList<>();
    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_house;
    }

    @Override
    public void initData() {


        initSv(mSv, res -> {
            if(houses.size()>0) {
                List<HouseBean.RowsBean> tmp = new ArrayList<>();
                for(HouseBean.RowsBean house:houses) {
                    if(getStr(house.getSourceName()).contains(res)) {
                        tmp.add(house);
                    }
                }
                if(tmp.size()==0) {
                    toast("未查找到相关数据");
                }
                listAdapter.setData(tmp);
            }else {
                toast("未加载完房源数据");
            }
        });

        listAdapter = new HouseAdapter(activity, null);
        initRV(mListRv, listAdapter);

        IconAdapter iconAdapter = new IconAdapter(activity, null);
        iconAdapter.onClick = ()->{
            showPage(iconAdapter.posBean.getIconName());
        };
        initGRV(mIconRv, iconAdapter, 4);

        List<IconBean> show = Arrays.asList(
                new IconBean("二手", R.drawable.home),
                new IconBean("租房", R.drawable.home),
                new IconBean("楼盘", R.drawable.home),
                new IconBean("中介", R.drawable.home)
        );
        iconAdapter.setData(show);

        showPage("二手");
        get("/prod-api/api/house/housing/list", res -> {
            runOnUiThread(() -> {
                HouseBean bean = new Gson().fromJson(res, HouseBean.class);
                houses = bean.getRows();
            });
        });
    }
    public void showPage(String type) {
        get("/prod-api/api/house/housing/list?pageSize=20&pageNum=1&houseType="+type, res -> {
            runOnUiThread(() -> {
                HouseBean bean = new Gson().fromJson(res, HouseBean.class);
                listAdapter.setData(bean.getRows());
            });
        });
    }
    @Override
    public void resumeData() {


    }

    @Override
    public void initView() {

        mSv = findViewById(R.id.sv);
        mIconRv = findViewById(R.id.iconRv);
        mListRv = findViewById(R.id.listRv);
    }
}