package com.xqh.smartcityall.house;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.active.ActiveAdapter;
import com.xqh.smartcityall.active.ActiveCommentAc;
import com.xqh.smartcityall.pojo.ActiveBean;
import com.xqh.smartcityall.pojo.ActiveDetail;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HouseDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initWebView;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class HouseDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageTitle;
    private android.widget.TextView mPageDesc;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "房源详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_house_detail;
    }

    @Override
    public void initData() {
        mTodoBt.setOnClickListener(v -> finish());

        get("/prod-api/api/house/housing/"+getJumpId(), res -> {
            runOnUiThread(() -> {
                HouseDetail bean = new Gson().fromJson(res, HouseDetail.class);
                if (bean.getCode() == 200) {
                    HouseDetail.DataBean data = bean.getData();
                    loadImg(data.getPic(), mCover);
                    mPageTitle.setText(getStrHtml("%s", data.getSourceName()));

                    mPageDesc.setText(getStrHtml(
                            "<h4>建筑面积</h4>&emsp;%s<h4>房源单价</h4>&emsp;%s<h4>房源类型</h4>&emsp;%s<h4>房源介绍</h4>&emsp;%s",
                            getStr(data.getAreaSize()+"㎡"),
                            getStr(data.getPrice()),
                            getStr(data.getHouseType()),
                            getStr(data.getDescription())
                    ));
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageTitle = findViewById(R.id.pageTitle);
        mPageDesc = findViewById(R.id.pageDesc);
        mPageDesc = findViewById(R.id.pageDesc);
        mTodoBt = findViewById(R.id.todoBt);
    }
}