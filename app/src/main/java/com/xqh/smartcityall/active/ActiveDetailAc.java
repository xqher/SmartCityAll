package com.xqh.smartcityall.active;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ActiveBean;
import com.xqh.smartcityall.pojo.ActiveDetail;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.DefRowType;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initWebView;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ActiveDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageTitle;
    private android.webkit.WebView mContent;
    private android.widget.Button mTodoBt;
    private androidx.recyclerview.widget.RecyclerView mRecommendRv;
    private android.widget.Button mCommentBt;

    @Override
    public String initTitle() {
        return "活动详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_active_detail;
    }

    @Override
    public void initData() {

        ActiveAdapter adapter = new ActiveAdapter(activity, null);
        initRV(mRecommendRv, adapter);

        get("/prod-api/api/activity/activity/"+getJumpId(), res -> {
            runOnUiThread(() -> {
                ActiveDetail bean = new Gson().fromJson(res, ActiveDetail.class);
                if (bean.getCode() == 200) {
                    ActiveDetail.DataBean data = bean.getData();
                    loadImg(data.getImgUrl(), mCover);
                    mPageTitle.setText(getStrHtml("%s", data.getName()));
                    initWebView(mContent, data.getContent());

                    get("/prod-api/api/activity/activity/list?recommend=Y&categoryId="+data.getCategoryId(), json -> {
                        runOnUiThread(()->{
                            ActiveBean active = new Gson().fromJson(json, ActiveBean.class);
                            if(active.getCode()==200) {
                                adapter.setData(active.getRows());
                            }
                        });
                    });

                    mTodoBt.setOnClickListener(v -> {
                        HashMap<String, String> map = new HashMap<>();
                        map.put("activityId", getJumpId());

                        post("/prod-api/api/activity/signup", map, json -> {
                            runOnUiThread(() -> {
                                DefRes resp = new Gson().fromJson(json, DefRes.class);
                                toast(resp.getMsg());
                            });
                        });
                    });

                    mCommentBt.setOnClickListener(v -> {
                        jumpId(ActiveCommentAc.class, getJumpId());
                    });
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageTitle = findViewById(R.id.pageTitle);
        mContent = findViewById(R.id.content);
        mTodoBt = findViewById(R.id.todoBt);
        mRecommendRv = findViewById(R.id.recommendRv);
        mCommentBt = findViewById(R.id.commentBt);
    }
}