package com.xqh.smartcityall.active;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ActiveCommentStatus;
import com.xqh.smartcityall.pojo.AvatarComment;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import static com.xqh.smartcityall.utils.Const.*;

public class ActiveCommentAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private com.google.android.material.textfield.TextInputEditText mCommentEt;
    private android.widget.Button mCommentBt;
    private int page = 1;
    private List<AvatarComment.RowsBean> comments = new ArrayList<>();
    private ActiveCommentAdapter adapter;
    private int stId = -1;
    private int commentCnt = 0;
    @Override
    public String initTitle() {
        return "评论";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_comment_page;
    }

    @Override
    public void initData() {
        adapter = new ActiveCommentAdapter(activity, null);
        initRV(mListRv, adapter);

        loadComment();

        mTodoBt.setOnClickListener(v -> {
            page++;
            loadComment();
        });

        get("/prod-api/api/activity/comment/number?activityId="+getJumpId(), json -> {
            runOnUiThread(() -> {
                ActiveCommentStatus res = new Gson().fromJson(json, ActiveCommentStatus.class);
                toast(res.getMsg());
                if(res.getCode()==200) {
                    commentCnt = res.getCommentNum();
                    mTitle.setText(getStrHtml("共有评论%s条", commentCnt+""));
                }
            });
        });
        mCommentBt.setOnClickListener(v -> {
            String content = mCommentEt.getText().toString();
            if(isEmpty(content)) {
                toast("未输入完成");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("activityId", getJumpId());
            map.put("content", content);

            post("/prod-api/api/activity/comment", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    page = 1;
                    comments.clear();
                    loadComment();
                    if(res.getCode()==200) {
                        mCommentEt.setText("");
                        commentCnt++;
                        mTitle.setText(getStrHtml("共有评论%s条", commentCnt+""));
                    }
                });
            });
        });
    }
    public void loadComment() {
        get(String.format("/prod-api/api/activity/comment/list?activityId=%s&pageSize=20&pageNum=%s", getJumpId(), page), res -> {
            runOnUiThread(()->{
                AvatarComment bean = new Gson().fromJson(res, AvatarComment.class);
                if(bean.getCode()==200) {
                    if(bean.getRows() == null || bean.getRows().size()==0) {
                        toast("没有更多数据了");
                        return;
                    }
                    if(stId==bean.getRows().get(0).getId()) {
                        toast("没有更多数据了");
                        return;
                    }
                    stId = bean.getRows().get(0).getId();
                    comments.addAll(bean.getRows());
                    adapter.setData(comments);
                }
            });
        });
    }
    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
        mCommentEt = findViewById(R.id.commentEt);
        mCommentBt = findViewById(R.id.commentBt);
    }
}