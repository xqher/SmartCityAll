package com.xqh.smartcityall.active;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ActiveBean;
import com.xqh.smartcityall.pojo.AdvBanner;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.initTargetBanner;
import static com.xqh.smartcityall.utils.Const.initRV;

public class ActiveAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private TabLayout mTl;
    private androidx.recyclerview.widget.RecyclerView mListRv;

    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_active;
    }

    @Override
    public void initData() {


        get("/prod-api/api/activity/rotation/list", res -> {
            runOnUiThread(()->{
                AdvBanner bean = new Gson().fromJson(res, AdvBanner.class);
                if(bean.getCode()==200) {
                    List<AdvBanner.RowsBean> banners = bean.getRows();
                    initTargetBanner(mBanner, banners, targetId -> {
                        jumpId(ActiveDetailAc.class, targetId);
                    });
                }
            });
        });

        get("/prod-api/api/activity/category/list", res -> {
            runOnUiThread(()->{
                DefType bean = new Gson().fromJson(res, DefType.class);
                if(bean.getCode()==200) {
                    for(DefType.DataBean type:bean.getData()) {
                        mTl.addTab(mTl.newTab().setText(type.getName()).setTag(type.getId()));
                    }
                }
            });
        });

        ActiveAdapter adapter = new ActiveAdapter(activity, null);
        initRV(mListRv, adapter);

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                get("/prod-api/api/activity/activity/list?categoryId="+tab.getTag(), res -> {
                    runOnUiThread(()->{
                        ActiveBean bean = new Gson().fromJson(res, ActiveBean.class);
                        if(bean.getCode()==200) {
                            adapter.setData(bean.getRows());
                        }
                    });
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
    }
}