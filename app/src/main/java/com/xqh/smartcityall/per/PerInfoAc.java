package com.xqh.smartcityall.per;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.UserInfo;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class PerInfoAc extends BaseAc {


    private android.widget.ImageView mAvatar;
    private android.widget.EditText mUsername;
    private android.widget.EditText mPhone;
    private android.widget.TextView mIdCard;
    private android.widget.RadioButton mMale;
    private android.widget.RadioButton mFemale;
    private android.widget.Button mTodoBt;
    private String originalPhone = "";
    @Override
    public String initTitle() {
        return "个人信息";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_per_info;
    }

    @Override
    public void initData() {
        get("/prod-api/api/common/user/getInfo", res -> {
            runOnUiThread(()->{
                UserInfo.UserBean user = new Gson().fromJson(res, UserInfo.class).getUser();
                if (user != null) {
                    loadImg(user.getAvatar(), mAvatar);
                    mUsername.setText(getStr(user.getNickName()));

                    originalPhone = getStr(user.getPhonenumber());
                    String phone = originalPhone;
                    if(originalPhone.length()>4) {
                        phone = phone.substring(0, phone.length()-4) + "****";
                    }
                    mPhone.setText(phone);

                    String id = getStr(user.getIdCard());
                    if(id.length()==18) {
                        id = id.substring(0,2) +"************" + id.substring(14);
                    }
                    mIdCard.setText(id);

                    if(user.getSex().equals("0")) {
                        mMale.setChecked(true);
                    } else {
                        mFemale.setChecked(true);
                    }


                    mUsername.setText(getStr(user.getNickName()));
                }
            });
        });
        mTodoBt.setOnClickListener(v -> {
            String phone = mPhone.getText().toString();
            String username = mUsername.getText().toString();
            String sex = mMale.isChecked()?"0":"1";
            if (username.isEmpty()
                    || phone.isEmpty()
            ) {
                toast("未输入完成");
                return;
            }

            if(phone.contains("*")) phone = originalPhone;

            if(phone.length()<11) {
                toast("手机号码格式错误");
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("nickName", username);
            map.put("phonenumber", phone);
            map.put("sex", sex);

            put("/prod-api/api/common/user", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                });
            });


        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mAvatar = findViewById(R.id.avatar);
        mUsername = findViewById(R.id.username);
        mPhone = findViewById(R.id.phone);
        mIdCard = findViewById(R.id.idCard);
        mMale = findViewById(R.id.male);
        mFemale = findViewById(R.id.female);
        mTodoBt = findViewById(R.id.todoBt);
    }
}