package com.xqh.smartcityall.per;

import android.app.AlertDialog;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;

import com.google.gson.Gson;
import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.URL;
import static com.xqh.smartcityall.utils.Const.getSp;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.isEmpty;
import static com.xqh.smartcityall.utils.Const.setSp;

public class LoginAc extends BaseAc {

    private com.google.android.material.textfield.TextInputEditText mUsername;
    private com.google.android.material.textfield.TextInputEditText mPassword;
    private android.widget.Button mTodoBt;
    private android.widget.TextView mRegisterTv;
    private android.widget.TextView mNetTv;

    @Override
    public String initTitle() {
        return "登录";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_login;
    }

    @Override
    public void initData() {
        mBack.setVisibility(View.GONE);
        mRegisterTv.setOnClickListener(v -> jump(RegisterAc.class));
        mTodoBt.setOnClickListener(v -> {
            String username = mUsername.getText().toString();
            String password = mPassword.getText().toString();
            if (username.isEmpty() || password.isEmpty()) {
                toast("未输入完成");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("username", username);
            map.put("password", password);
            post("/prod-api/api/login", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if(res.getCode() == 200) {
                        setSp("token", res.getToken());
                        jump(MainActivity.class);
                        finish();
                    }
                });
            });
        });
        mNetTv.setOnClickListener(v -> {
            EditText net = new EditText(activity);
            String url = getSp("url");
            if(!isEmpty(url)) {
                url = url.substring(7);
            }
            net.setText(url);
            net.setHint("设置网络如192.168.1.1:3306");
            new AlertDialog.Builder(activity)
                    .setView(net)
                    .setPositiveButton("保存", (dialog, which) -> {
                        String inp = net.getText().toString();
                        if (inp.isEmpty()) {
                            toast("未输入内容");
                            return;
                        }
                        if (inp.length() < 10) {
                            toast("ip格式错误");
                            return;
                        }
                        URL =  "http://"+inp;
                        Const.setSp("url", URL);
                        toast("保存成功");
                    })
                    .setNegativeButton("取消", null)
                    .create()
                    .show();
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mUsername = findViewById(R.id.username);
        mPassword = findViewById(R.id.password);
        mTodoBt = findViewById(R.id.todoBt);
        mRegisterTv = findViewById(R.id.registerTv);
        mNetTv = findViewById(R.id.netTv);
    }
}