package com.xqh.smartcityall.per;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

import java.util.HashMap;

public class PerFeedbackAc extends BaseAc {


    private com.google.android.material.textfield.TextInputEditText mFeedback;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "意见反馈";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_feedback;
    }

    @Override
    public void initData() {
        mTodoBt.setOnClickListener(v -> {
            String feedback = mFeedback.getText().toString();
            if(Const.isEmpty(feedback)) {
                toast("未输入内容");
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("content", feedback);
            map.put("title", "意见反馈");

            post("/prod-api/api/common/feedback", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if (res.getCode() == 200) {
                        mFeedback.setText("");
                    }
                });
            });

        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mFeedback = findViewById(R.id.feedback);
        mTodoBt = findViewById(R.id.todoBt);
    }
}