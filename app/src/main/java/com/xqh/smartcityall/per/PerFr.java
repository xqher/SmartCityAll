package com.xqh.smartcityall.per;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.UserInfo;
import com.xqh.smartcityall.utils.BaseFr;
import com.xqh.smartcityall.utils.Const;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;
import static com.xqh.smartcityall.utils.Const.setSp;

public class PerFr extends BaseFr {


    private ImageView mAvatar;
    private TextView mUsername;
    private TextView mItemInfo;
    private TextView mItemOrder;
    private TextView mItemPwdModify;
    private TextView mItemFeedback;
    private Button mTodoBt;
    private boolean[] login = {false};

    @Override
    public int initLayout() {
        return R.layout.fragment_per;
    }

    @Override
    public void initData() {

        mItemInfo.setOnClickListener(v -> {
            if (!login[0]) {
                toast("未登录");
                return;
            }
            jump(PerInfoAc.class);
        });

        mItemOrder.setOnClickListener(v -> {
            if (!login[0]) {
                toast("未登录");
                return;
            }
            jump(PerOrderAc.class);
        });

        mItemPwdModify.setOnClickListener(v -> {
            if (!login[0]) {
                toast("未登录");
                return;
            }
            jump(PerModifyPwdAc.class);
        });

        mItemFeedback.setOnClickListener(v -> {
            if (!login[0]) {
                toast("未登录");
                return;
            }
            jump(PerFeedbackAc.class);
        });

        mTodoBt.setOnClickListener(v -> {
            setSp("token", "");
            jump(LoginAc.class);
            activity.finish();
        });
    }

    private void loadData() {
        if (login[0]) {
            get("/prod-api/api/common/user/getInfo", res -> {
                runOnUi(()->{
                    UserInfo.UserBean user = new Gson().fromJson(res, UserInfo.class).getUser();
                    if (user != null) {
                        loadImg(user.getAvatar(), mAvatar);
                        mUsername.setText(getStr(user.getNickName()));
                    }
                });
            });
        } else {
            setSp("token", "");
            mUsername.setText("未登录");
        }

        mTodoBt.setText(login[0] ? "退出" : "登录");
    }

    @Override
    public void resumeData() {
        Const.isLogin(login, this::loadData);
    }

    @Override
    public void initView() {
        mAvatar = activity.findViewById(R.id.avatar);
        mUsername = activity.findViewById(R.id.username);
        mItemInfo = activity.findViewById(R.id.item_info);
        mItemOrder = activity.findViewById(R.id.item_order);
        mItemPwdModify = activity.findViewById(R.id.item_pwdModify);
        mItemFeedback = activity.findViewById(R.id.item_feedback);
        mTodoBt = activity.findViewById(R.id.todoBt);
    }
}
