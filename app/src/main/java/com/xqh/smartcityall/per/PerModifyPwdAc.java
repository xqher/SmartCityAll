package com.xqh.smartcityall.per;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

import java.util.HashMap;

public class PerModifyPwdAc extends BaseAc {


    private com.google.android.material.textfield.TextInputEditText mOldPwd;
    private com.google.android.material.textfield.TextInputEditText mNewPwd;
    private com.google.android.material.textfield.TextInputEditText mRepeatPwd;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "修改密码";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_pwd_modify;
    }

    @Override
    public void initData() {
        mTodoBt.setOnClickListener(v -> {
            String o = mOldPwd.getText().toString();
            String n = mNewPwd.getText().toString();
            String r = mRepeatPwd.getText().toString();
            if (Const.isEmpty(o)
                    || Const.isEmpty(n)
                    || Const.isEmpty(r)
            ) {
                toast("未填写完成");
                return;
            }
            if (!r.equals(n)) {
                toast("新密码与确认密码不一致");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("newPassword", n);
            map.put("oldPassword", o);

            put("/prod-api/api/common/user/resetPwd", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if (res.getCode() == 200) {
                        mOldPwd.setText("");
                        mNewPwd.setText("");
                        mRepeatPwd.setText("");
                    }
                });
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mOldPwd = findViewById(R.id.oldPwd);
        mNewPwd = findViewById(R.id.newPwd);
        mRepeatPwd = findViewById(R.id.repeatPwd);
        mTodoBt = findViewById(R.id.todoBt);
    }
}