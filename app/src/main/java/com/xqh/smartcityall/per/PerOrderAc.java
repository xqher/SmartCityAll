package com.xqh.smartcityall.per;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.page.NewsAdapter;
import com.xqh.smartcityall.page.NewsDetailAc;
import com.xqh.smartcityall.pojo.BusOrder;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.initRV;

public class PerOrderAc extends BaseAc {


    private com.google.android.material.tabs.TabLayout mTl;
    private androidx.recyclerview.widget.RecyclerView mListRv;

    @Override
    public String initTitle() {
        return "订单列表";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_per_order;
    }

    @Override
    public void initData() {
        BusOrderAdapter adapter = new BusOrderAdapter(activity, null);
        initRV(mListRv, adapter);

        get("/prod-api/api/bus/order/list?status=1", res -> {
            runOnUiThread(()->{
                BusOrder bean = new Gson().fromJson(res, BusOrder.class);
                if(bean.getCode()==200) {
                    adapter.setData(bean.getRows());
                }
            });
        });

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                get("/prod-api/api/bus/order/list?status="+(tab.getText().toString().equals("已支付")?1:0), res -> {
                    runOnUiThread(()->{
                        BusOrder bean = new Gson().fromJson(res, BusOrder.class);
                        if(bean.getCode()==200) {
                            adapter.setData(bean.getRows());
                        }
                    });
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
    }
}