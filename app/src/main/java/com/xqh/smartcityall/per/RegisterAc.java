package com.xqh.smartcityall.per;

import com.google.gson.Gson;
import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.setSp;

public class RegisterAc extends BaseAc {


    private com.google.android.material.textfield.TextInputEditText mUsername;
    private com.google.android.material.textfield.TextInputEditText mNickname;
    private com.google.android.material.textfield.TextInputEditText mPassword;
    private com.google.android.material.textfield.TextInputEditText mPhone;
    private com.google.android.material.textfield.TextInputEditText mIdCard;
    private com.google.android.material.textfield.TextInputEditText mEmail;
    private android.widget.RadioButton mMale;
    private android.widget.RadioButton mFemale;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "注册";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_register;
    }

    @Override
    public void initData() {
        mTodoBt.setOnClickListener(v -> {
            String username = mUsername.getText().toString();
            String nickname = mNickname.getText().toString();
            String email = mEmail.getText().toString();
            String phone = mPhone.getText().toString();
            String idCard = mIdCard.getText().toString();
            String sex = mMale.isChecked()?"0":"1";
            String password = mPassword.getText().toString();
            if (username.isEmpty()
                    || password.isEmpty()
                    || nickname.isEmpty()
                    || email.isEmpty()
                    || phone.isEmpty()
                    || idCard.isEmpty()
            ) {
                toast("未输入完成");
                return;
            }
            if(phone.length()<11) {
                toast("手机号码格式错误");
                return;
            }
            if(idCard.length()<18) {
                toast("证件号码格式错误");
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("userName", username);
            map.put("password", password);
            map.put("phonenumber", phone);
            map.put("nickName", nickname);
            map.put("idCard", idCard);
            map.put("email", email);
            map.put("sex", sex);

            post("/prod-api/api/register", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if(res.getCode() == 200) {
                        finish();
                    }
                });
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mUsername = findViewById(R.id.username);
        mNickname = findViewById(R.id.nickname);
        mPassword = findViewById(R.id.password);
        mPhone = findViewById(R.id.phone);
        mIdCard = findViewById(R.id.idCard);
        mEmail = findViewById(R.id.email);
        mMale = findViewById(R.id.male);
        mFemale = findViewById(R.id.female);
        mTodoBt = findViewById(R.id.todoBt);
    }
}