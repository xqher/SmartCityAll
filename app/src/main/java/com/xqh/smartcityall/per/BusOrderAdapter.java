package com.xqh.smartcityall.per;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.park.ParkDetailAc;
import com.xqh.smartcityall.pojo.BusOrder;
import com.xqh.smartcityall.pojo.ParkRecord;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;

public class BusOrderAdapter extends RecyclerView.Adapter<BusOrderAdapter.VH> {


    private List<BusOrder.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public BusOrder.RowsBean posBean;

    public BusOrderAdapter(Context ctx, List<BusOrder.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<BusOrder.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(BusOrder.RowsBean bean) {

            mCover.setVisibility(View.GONE);

            mTitle.setText(String.format("订单编号:%s", bean.getOrderNum()));
            mContent.setText("订单类型:智慧巴士");
            mDate.setText(String.format("%s", bean.getCreateTime()));
            mStatus.setText(String.format("价格:%s元", bean.getPrice()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                ((BaseAc)ctx).jumpId(ParkDetailAc.class, new Gson().toJson(bean));
            });
        }

        private void initView() {
            mCover = itemView.findViewById(R.id.cover);
            mTitle = itemView.findViewById(R.id.title);

            mTitle.setTextSize(20);

            mContent = itemView.findViewById(R.id.content);
            mDate = itemView.findViewById(R.id.date);
            mStatus = itemView.findViewById(R.id.status);
        }
    }
}
