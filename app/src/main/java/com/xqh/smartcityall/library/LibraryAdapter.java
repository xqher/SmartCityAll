package com.xqh.smartcityall.library;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.LibraryBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class LibraryAdapter extends RecyclerView.Adapter<LibraryAdapter.VH> {


    private List<LibraryBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public LibraryBean.RowsBean posBean;

    public LibraryAdapter(Context ctx, List<LibraryBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<LibraryBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mDate.setVisibility(View.GONE);
        }

        public void onItem(LibraryBean.RowsBean bean) {

            loadImg(bean.getImgUrl(), mCover);
            mTitle.setText(getStr(bean.getName()));
            mContent.setText(Html.fromHtml(String.format("地址:%s\n营业时间:%s", getStr(bean.getAddress()), getStr(bean.getBusinessHours())),Html.FROM_HTML_MODE_COMPACT));
            mStatus.setText(String.format("营业状态:%s", getStr(bean.getBusinessState()).equals("1")?"营业中":"暂停运营"));


            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
            });
        }

        private void initView() {
            mCover = itemView.findViewById(R.id.cover);
            mTitle = itemView.findViewById(R.id.title);
            mContent = itemView.findViewById(R.id.content);
            mDate = itemView.findViewById(R.id.date);
            mStatus = itemView.findViewById(R.id.status);

        }
    }
}
