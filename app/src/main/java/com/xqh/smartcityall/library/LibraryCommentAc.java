package com.xqh.smartcityall.library;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.LikeCommentAdapter;
import com.xqh.smartcityall.pojo.DefComment;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import static com.xqh.smartcityall.utils.Const.*;

public class LibraryCommentAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private com.google.android.material.textfield.TextInputEditText mCommentEt;
    private android.widget.Button mCommentBt;
    private int page = 1;
    private List<DefComment.DataBean> comments = new ArrayList<>();
    private LikeCommentAdapter adapter;
    private int stId = -1;
    @Override
    public String initTitle() {
        return "评论";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_comment_page;
    }

    @Override
    public void initData() {
        adapter = new LikeCommentAdapter(activity, null);
        adapter.onClick = ()->{
            HashMap<String, String> map = new HashMap<>();
            map.put("commentId", adapter.posBean.getId()+"");
            map.put("like", !adapter.posBean.isMyLikeState()+"");

            post("/prod-api/api/digital-library/library-comment/like", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                });
            });
        };
        initRV(mListRv, adapter);

        loadComment();

        mTodoBt.setOnClickListener(v -> {
            page++;
            loadComment();
        });

        mCommentBt.setOnClickListener(v -> {
            String content = mCommentEt.getText().toString();
            if(isEmpty(content)) {
                toast("未输入完成");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("libraryId", getJumpId());
            map.put("content", content);

            post("/prod-api/api/digital-library/library-comment", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    page = 1;
                    loadComment();
                    if(res.getCode()==200) {
                        mCommentEt.setText("");
                    }
                });
            });
        });
    }
    public void loadComment() {
        get(String.format("/prod-api/api/digital-library/library-comment/list?libraryId=%s&pageSize=20&pageNum=%s", getJumpId(), page), res -> {
            runOnUiThread(()->{
                DefComment bean = new Gson().fromJson(res, DefComment.class);
                if(bean.getCode()==200) {
                    if(bean.getData() == null || bean.getData().size()==0) {
                        toast("没有更多数据了");
                        return;
                    }
                    if(stId==bean.getData().get(0).getId()) {
                        toast("没有更多数据了");
                        Collections.sort(comments , (o1, o2) -> o2.getLikeCount()-o1.getLikeCount());
                        return;
                    }
                    stId = bean.getData().get(0).getId();
                    Collections.sort(bean.getData() , (o1, o2) -> o2.getLikeCount()-o1.getLikeCount());
                    comments.addAll(bean.getData());
                    adapter.setData(comments);
                }
            });
        });
    }
    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
        mCommentEt = findViewById(R.id.commentEt);
        mCommentBt = findViewById(R.id.commentBt);
    }
}