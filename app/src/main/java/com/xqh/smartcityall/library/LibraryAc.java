package com.xqh.smartcityall.library;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.page.NewsAdapter;
import com.xqh.smartcityall.page.NewsDetailAc;
import com.xqh.smartcityall.pojo.LibraryBean;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.initRV;

public class LibraryAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;

    @Override
    public String initTitle() {
        return "数字图书馆";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {
        LibraryAdapter adapter = new LibraryAdapter(activity, null);
        adapter.onClick=()->{
            jumpId(LibraryDetailAc.class, adapter.posBean.getId());
        };
        initRV(mListRv, adapter);

        get("/prod-api/api/digital-library/library/list", res -> {
            runOnUiThread(()->{
                LibraryBean bean = new Gson().fromJson(res, LibraryBean.class);
                if(bean.getCode()==200) {
                    Collections.sort(bean.getRows() , (o1, o2) -> Integer.parseInt(o2.getBusinessState()) - Integer.parseInt(o1.getBusinessState()));
                    adapter.setData(bean.getRows());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
    }
}