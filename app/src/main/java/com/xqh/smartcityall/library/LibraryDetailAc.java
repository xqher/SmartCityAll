package com.xqh.smartcityall.library;

import android.graphics.Matrix;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.LibraryDetail;
import com.xqh.smartcityall.pojo.NewsDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.lang.reflect.Field;

import static com.xqh.smartcityall.utils.Const.URL;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class LibraryDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageTitle;
    private android.widget.TextView mPageDetail;
    private android.widget.TextView mPageDesc;
    private android.widget.Button mTodoBt;

    private float scale = 1f;
    private float imgW, imgH;
    private Matrix matrix = new Matrix();
    private View mScaleLayout;

    @Override
    public String initTitle() {
        return "图书馆详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_library_detail;
    }

    @Override
    public void initData() {

        imgH = mCover.getHeight() / 2f;
        imgW = mCover.getWidth() / 2f;
        matrix.setScale(scale, scale, imgW, imgH);
        mCover.setImageMatrix(matrix);


        ScaleGestureDetector scaleGestureDetector = new ScaleGestureDetector(activity, new ScaleGestureDetector.SimpleOnScaleGestureListener() {

            @Override
            public boolean onScaleBegin(ScaleGestureDetector detector) {
                return true;
            }

            @Override
            public boolean onScale(ScaleGestureDetector detector) {
                scale *= detector.getScaleFactor();
                if (scale > 3 || scale < 0.1) {
                    return true;
                }
                matrix.setScale(scale, scale, imgW, imgH);
                mCover.setImageMatrix(matrix);
                return false;
            }
        });
        mScaleLayout.setOnTouchListener((v, event) -> {
            scaleGestureDetector.onTouchEvent(event);
            return true;
        });


        get("/prod-api/api/digital-library/library/" + getJumpId(), res -> {
            runOnUiThread(() -> {
                LibraryDetail bean = new Gson().fromJson(res, LibraryDetail.class);
                if (bean.getCode() == 200) {
                    LibraryDetail.DataBean data = bean.getData();
                    loadImg(data.getMapUrl(), mCover);
                    mPageTitle.setText(getStr(data.getName()));
                    mTitle.setText(getStr(data.getName()));
                    mPageDetail.setText(String.format("具体地址:%s\n营业时间:%s\n营业状态:%s",
                            getStr(data.getAddress()),
                            getStr(data.getBusinessHours()),
                            getStr(data.getBusinessState()).equals("1") ? "营业中" : "暂停运营"));
                    mTodoBt.setOnClickListener(v -> jumpId(LibraryCommentAc.class, getJumpId()));
                    mPageDesc.setText(getStr(data.getDescription()));

                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageTitle = findViewById(R.id.pageTitle);
        mPageDetail = findViewById(R.id.pageDetail);
        mPageDesc = findViewById(R.id.pageDesc);
        mTodoBt = findViewById(R.id.todoBt);
        mScaleLayout = findViewById(R.id.scaleLayout);
    }
}