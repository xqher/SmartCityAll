package com.xqh.smartcityall.chart;

import android.graphics.Color;
import android.os.Handler;
import android.os.Message;

import androidx.annotation.NonNull;

import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import static com.xqh.smartcityall.utils.Const.getStr;

public class ChartAc extends BaseAc {


    private com.github.mikephil.charting.charts.BarChart mBarChart;
    private com.github.mikephil.charting.charts.HorizontalBarChart mHBarChart;
    private com.github.mikephil.charting.charts.LineChart mLineChart;
    private com.github.mikephil.charting.charts.PieChart mPieChart;
    private com.github.mikephil.charting.charts.HorizontalBarChart mHBarChartB;
    private android.widget.Button mUpdateBt;

    @Override
    public String initTitle() {
        return "数据分析";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_chart;
    }

    @Override
    public void initData() {
        initBarChart();
        updateBarChart();
        initHBarChart();
        updateHBarChart();
        initLineChart();
        updateLineChart();
        initPieChart();
        updatePieChart();
        initHBarChartB();
        mUpdateBt.setOnClickListener(v -> {
            updateBarChart();
            updateHBarChart();
            updateLineChart();
            updatePieChart();
            initHBarChartB();
        });
        Handler handler = new Handler(getMainLooper()){
            @Override
            public void handleMessage(@NonNull Message msg) {
                super.handleMessage(msg);
                updateBarChart();
                updateHBarChart();
                updateLineChart();
                updatePieChart();
                initHBarChartB();
            }
        };
        new Thread(()->{
            while (true){
                try {
                    handler.sendEmptyMessage(1);
                    Thread.sleep(1200);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }).start();
    }
    private void initBarChart() {
        mBarChart.getLegend().setEnabled(true);
        mBarChart.getDescription().setEnabled(false);
        mBarChart.getAxisRight().setEnabled(false);
        mBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    }
    private void updateBarChart() {
        String[] labels = {"16号线", "14号线东段"};
        int[] colors = getRandomColor(labels.length);

        int stationCnt = new Random().nextInt(10) + new Random().nextInt(20) + 5;
        int[] values = new int[labels.length];

        for (int i = 0; i < stationCnt; i++) {
            int guestCnt = new Random().nextInt(20) + new Random().nextInt(10) + 5;
            int distance = new Random().nextInt(3) + 1;
            int cur = guestCnt * distance;
            values[i % labels.length] += cur;
        }

        mBarChart.getXAxis().setLabelCount(labels.length);
        mBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return labels[(int)value];
            }
        });


        List<BarEntry> entries = new ArrayList<>(labels.length);

        int max = 0;
        for (int i = 0; i < labels.length; i++) {
            entries.add(new BarEntry(i, values[i]));
            max = Math.max(max, values[i]);
        }

        mBarChart.getAxisLeft().setAxisMinimum(0);
        mBarChart.getAxisLeft().setAxisMaximum(max + 10);

        BarDataSet barDataSet = new BarDataSet(entries, "周转量");
        barDataSet.setValueTextSize(12);
        barDataSet.setColor(colors[0]);

        BarData barData = new BarData(barDataSet);
        barData.setBarWidth(.5f);
        mBarChart.animateXY(0, 1681);
        mBarChart.setData(barData);
        mBarChart.invalidate();

    }
    private void initHBarChart() {
        mHBarChart.getLegend().setEnabled(true);
        mHBarChart.getDescription().setEnabled(false);
        mHBarChart.setTouchEnabled(false);
        // 顺时针旋转90度
        mHBarChart.getAxisLeft().setEnabled(false);
        mHBarChart.getAxisRight().setDrawGridLines(false);
        mHBarChart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
    }
    private void updateHBarChart() {
        String[] labels = {"16号线", "14号线东段"};
        int[] colors = getRandomColor(labels.length);

        int stationCnt = new Random().nextInt(10) + new Random().nextInt(20) + 5;
        int[] values = new int[labels.length];

        for (int i = 0; i < stationCnt; i++) {
            int guestCnt = new Random().nextInt(20) + new Random().nextInt(10) + 5;
            int distance = new Random().nextInt(3) + 1;
            int cur = guestCnt * distance;
            values[i % labels.length] += cur;
        }

        mHBarChart.getXAxis().setLabelCount(labels.length);
        mHBarChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return labels[(int)value];
            }
        });


        List<BarEntry> entries = new ArrayList<>(labels.length);
        List<BarEntry> entriesB = new ArrayList<>(labels.length);

        int max = 0;
        for (int i = 0; i < labels.length; i++) {
            entries.add(new BarEntry(i, values[i]));
            entriesB.add(new BarEntry(i, values[i]/2f));
            max = Math.max(max, values[i]);
        }

        mHBarChart.getAxisLeft().setAxisMinimum(0);
        mHBarChart.getAxisLeft().setAxisMaximum(max + 10);

        BarDataSet barDataSet = new BarDataSet(entries, "周转量");
        barDataSet.setValueTextSize(12);
        barDataSet.setColor(colors[0]);

        BarDataSet barDataSet2 = new BarDataSet(entriesB, "量转周");
        barDataSet2.setValueTextSize(12);
        barDataSet2.setColor(colors[1]);


        BarData barData = new BarData(barDataSet, barDataSet2);
        barData.setBarWidth(.3f);

        mHBarChart.animateXY(0, 1681);
        mHBarChart.setData(barData);
        mHBarChart.invalidate();

    }
    private void initHBarChartB() {

        mHBarChartB.getLegend().setEnabled(true);
        mHBarChartB.getDescription().setEnabled(false);
        mHBarChartB.setTouchEnabled(false);
        // 顺时针旋转90度
        mHBarChartB.getAxisLeft().setEnabled(false);
        mHBarChartB.getAxisRight().setDrawGridLines(false);
        mHBarChartB.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);

        String[] labels = {"16号线", "14号线"};
        int[] colors = getRandomColor(labels.length);

        int stationCnt = new Random().nextInt(10) + new Random().nextInt(20) + 5;
        int[] values = new int[labels.length];

        for (int i = 0; i < stationCnt; i++) {
            int guestCnt = new Random().nextInt(20) + new Random().nextInt(10) + 5;
            int distance = new Random().nextInt(3) + 1;
            int cur = guestCnt * distance;
            values[i % labels.length] += cur;
        }

        mHBarChartB.getXAxis().setLabelCount(labels.length);
        mHBarChartB.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return labels[(int)value];
            }
        });


        List<BarEntry> entries = new ArrayList<>(labels.length);
        List<BarEntry> entriesB = new ArrayList<>(labels.length);

        int max = 0;
        for (int i = 0; i < labels.length; i++) {
            entries.add(new BarEntry(i, values[i]));
            entriesB.add(new BarEntry(i, values[i]/2f));
            max = Math.max(max, values[i]);
        }

        mHBarChartB.getAxisLeft().setAxisMinimum(0);
        mHBarChartB.getAxisLeft().setAxisMaximum(max + 10);

        BarDataSet barDataSet = new BarDataSet(entries, "周转量");
        barDataSet.setValueTextSize(12);
        barDataSet.setColor(colors[0]);

        BarDataSet barDataSet2 = new BarDataSet(entriesB, "量转周");
        barDataSet2.setValueTextSize(12);
        barDataSet2.setColor(colors[1]);


        BarData barData = new BarData(barDataSet, barDataSet2);

        barData.setBarWidth(.3f);
        barData.groupBars(-.3f,0.3f,0.05f);

        mHBarChartB.animateXY(0, 1681);
        mHBarChartB.setData(barData);
        mHBarChartB.invalidate();
    }
    private void initLineChart() {
        mLineChart.getLegend().setEnabled(true);
        mLineChart.getDescription().setEnabled(false);

        XAxis xAxis = mLineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setDrawGridLines(false);

        mLineChart.getAxisRight().setEnabled(false);
    }
    private void updateLineChart() {
        String[] labels = {"2022-10-22", "2022-10-23", "2022-10-24", "2022-10-25", "2022-10-26", "2022-10-27", "2022-10-28", "2022-10-29"};;
        String[] groups = {"西苑", "石厂", "阎村东", "香山"};
        int[] colors = getRandomColor(labels.length);

        int[][] values = new int[groups.length][labels.length];
        int max = 0;
        for(int i=0;i<groups.length;i++) {
            for(int j=0;j<labels.length;j++) {
                int cur = new Random().nextInt(100) + new Random().nextInt(55)+5;
                values[i%groups.length][j%labels.length] = cur;
                max = Math.max(max, cur);
            }
        }

        mLineChart.getXAxis().setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return labels[(int)value];
            }
        });
        mLineChart.getAxisLeft().setAxisMinimum(0);
        mLineChart.getAxisLeft().setAxisMaximum(max);

        List<LineDataSet> sets = new ArrayList<>();
        for(int i = 0;i<groups.length;i++) {
            List<Entry> entries = new ArrayList<>();
            for(int j=0;j<labels.length;j++) {
                entries.add(new Entry(j, values[i][j]));
            }
            LineDataSet dataSet = new LineDataSet(entries, groups[i]);
            dataSet.setColor(colors[i]);
            sets.add(dataSet);
        }

        LineData lineData = new LineData(sets.toArray(new LineDataSet[0]));
        mLineChart.setData(lineData);
        mLineChart.animateXY(0, 1681);
        mLineChart.invalidate();
    }

    private String[] getRandomDate(int length) {
        String[] labels = new String[length];
        int month = new Random().nextInt(11) + 1;
        int date = new Random().nextInt(27) + 1;
        int year = new Random().nextInt(22) + 1;
        for(int i=0;i<labels.length;i++) {
            labels[i] = String.format("20%s-%s-%s", year+"", month+"", (date++)+"");
        }
        return labels;
    }

    private int[] getRandomColor(int length) {
        int[] colors = new int[length];

        for(int i=0;i<length;i++) {
            int r = new Random().nextInt(256);
            int b = new Random().nextInt(256);
            int g = new Random().nextInt(256);
            colors[i] =  Color.rgb(r, g, b);
        }
        return  colors;
    }

    private void initPieChart() {
    }
    private void updatePieChart() {
        Legend legend = mPieChart.getLegend();
        legend.setOrientation(Legend.LegendOrientation.VERTICAL);
        legend.setEnabled(false);

        mPieChart.getDescription().setEnabled(false);
        mPieChart.setExtraOffsets(0,20,0,15);
        mPieChart.animateXY(2000,0);
        mPieChart.setEntryLabelColor(Color.parseColor("#000000"));
        mPieChart.setUsePercentValues(true);

        PieDataSet pieDataSet = new PieDataSet(Arrays.asList(
                new PieEntry(new Random().nextInt(50)+5/100f, "鼠1"),
                new PieEntry(new Random().nextInt(50)+5/100f, "鼠2"),
                new PieEntry(new Random().nextInt(50)+5/100f, "鼠3"),
                new PieEntry(new Random().nextInt(50)+5/100f, "鼠4"),
                new PieEntry(new Random().nextInt(50)+5/100f, "鼠5"),
                new PieEntry(new Random().nextInt(50)+5/100f, "鼠6")

        ), "鼠鼠");
        //线的长度
        pieDataSet.setValueLinePart1Length(0.5f);


        //数值显示在外面
        pieDataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        pieDataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        //间隔
        pieDataSet.setSliceSpace(5);

        pieDataSet.setValueTextSize(10);
        pieDataSet.setColors(getRandomColor(10));

        PieData pieData = new PieData(pieDataSet);

        DecimalFormat decimalFormat = new DecimalFormat("##.##");
        pieData.setValueFormatter(new ValueFormatter() {
            @Override
            public String getFormattedValue(float value) {
                return decimalFormat.format(value)+"%";
            }
        });
        pieData.setValueTextSize(10);

        mPieChart.setData(pieData);
        mPieChart.invalidate();
    }
    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBarChart = findViewById(R.id.BarChart);
        mHBarChart = findViewById(R.id.hBarChart);
        mLineChart = findViewById(R.id.lineChart);
        mPieChart = findViewById(R.id.pieChart);
        mUpdateBt = findViewById(R.id.updateBt);
        mHBarChartB = findViewById(R.id.hBarChartB);
    }
}