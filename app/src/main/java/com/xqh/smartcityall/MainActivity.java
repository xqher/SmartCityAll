package com.xqh.smartcityall;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.xqh.smartcityall.page.HomeFr;
import com.xqh.smartcityall.page.NewsFr;
import com.xqh.smartcityall.page.ServeFr;
import com.xqh.smartcityall.per.PerFr;
import com.xqh.smartcityall.restrict.RestrictFr;
import com.xqh.smartcityall.utils.BaseAc;

public class MainActivity extends BaseAc {

    private android.widget.FrameLayout mFl;
    private com.google.android.material.tabs.TabLayout mTl;

    @Override
    public String initTitle() {
        return "智慧城市";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_main;
    }

    @Override
    public void initData() {
        mBack.setVisibility(View.GONE);
        setFr(new HomeFr(), mFl);
        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                String t = tab.getText().toString();
                if(t.equals("首页")) {
                    mTitle.setText("智慧城市");
                    setFr(new HomeFr(), mFl);
                }else{
                    mTitle.setText(t);
                }
                if(t.equals("全部服务")) {
                    setFr(new ServeFr(), mFl);
                }
                if(t.equals("智慧党建")) {
                    setFr(new RestrictFr(), mFl);
                }
                if(t.equals("新闻")) {
                    setFr(new NewsFr(), mFl);
                }
                if(t.equals("个人中心")) {
                    setFr(new PerFr(), mFl);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void resumeData() {

    }
    public void showMore() {
        mTl.selectTab(mTl.getTabAt(1));
    }
    @Override
    public void initView() {

        mFl = findViewById(R.id.fl);
        mTl = findViewById(R.id.tl);
    }
}