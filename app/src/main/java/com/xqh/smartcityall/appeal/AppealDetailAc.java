package com.xqh.smartcityall.appeal;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AppealDetail;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class AppealDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageTitle;
    private android.widget.TextView mPageDesc;

    @Override
    public String initTitle() {
        return "诉求详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_appeal_detail;
    }

    @Override
    public void initData() {
        get("/prod-api/api/gov-service-hotline/appeal/"+getJumpId(), res -> {
            runOnUiThread(() -> {
                AppealDetail bean = new Gson().fromJson(res, AppealDetail.class);
                if (bean.getCode() == 200) {
                    AppealDetail.DataBean data = bean.getData();
                    loadImg(getStr(data.getImgUrl()), mCover);
                    mPageTitle.setText(getStrHtml("%s", getStr(data.getTitle())));
                    mPageDesc.setText(getStrHtml(
                            "<h4>诉求</h4>&emsp;%s<h4>承办单位</h4>&emsp;%s<h4>提交时间</h4>&emsp;%s<h4>处理状态</h4>&emsp;%s<h4>处理结果</h4>&emsp;%s",
                            getStr(data.getContent()),
                            getStr(data.getUndertaker()),
                            getStr(data.getCreateTime()),
                            getStr(data.getState()).equals("1")?"已处理":"未处理",
                            getStr(data.getDetailResult())
                    ));
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageTitle = findViewById(R.id.pageTitle);
        mPageDesc = findViewById(R.id.pageDesc);
    }
}