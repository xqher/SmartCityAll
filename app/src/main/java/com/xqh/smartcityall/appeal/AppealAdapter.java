package com.xqh.smartcityall.appeal;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AppealBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;

public class AppealAdapter extends RecyclerView.Adapter<AppealAdapter.VH> {


    private List<AppealBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public AppealBean.RowsBean posBean;

    public AppealAdapter(Context ctx, List<AppealBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<AppealBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mCover.setVisibility(View.GONE);

        }

        public void onItem(AppealBean.RowsBean bean) {

            mTitle.setText(getStr(bean.getTitle()));
            mContent.setText(getStr("承办单位:%s",getStr(bean.getUndertaker())));
            mDate.setText(getStr("%s", getStr(bean.getCreateTime())));
            mStatus.setText(getStr(bean.getState()).equals("1")?"已处理":"未处理");

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(AppealDetailAc.class, bean.getId());
            });
        }

        private void initView() {
            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mDate = findViewById(R.id.date);
            mStatus = findViewById(R.id.status);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
