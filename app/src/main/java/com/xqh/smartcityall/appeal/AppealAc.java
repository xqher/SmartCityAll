package com.xqh.smartcityall.appeal;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.pojo.AppealBean;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefRowType;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;

public class AppealAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private androidx.recyclerview.widget.RecyclerView mIconRv;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private AppealAdapter listAdapter;

    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_appeal;
    }

    @Override
    public void initData() {

        get("/prod-api/api/gov-service-hotline/ad-banner/list", res -> {
            runOnUiThread(() -> {
                DefBanner bean = new Gson().fromJson(res, DefBanner.class);
                if (bean.getCode() == 200) {
                    initBanner(mBanner, bean.getData(), id -> {
                    });
                }
            });
        });


        listAdapter = new AppealAdapter(activity, null);
        initRV(mListRv, listAdapter);

        IconAdapter iconAdapter = new IconAdapter(activity, null, 4);
        iconAdapter.onClick = ()->{
            jumpId(iconAdapter.posBean.getIconId().equals("23")?AppealCreateAc.class:AppealListAc.class, iconAdapter.posBean.getIconId());
        };
        initGRV(mIconRv, iconAdapter, 8);

        get("/prod-api/api/gov-service-hotline/appeal-category/list", res -> {
            runOnUiThread(() -> {
                DefRowType bean = new Gson().fromJson(res, DefRowType.class);
                if (bean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    Collections.reverse(bean.getRows());

                    DefRowType.RowsBean last = bean.getRows().remove(0);
                    bean.getRows().add(last);

                    for (DefRowType.RowsBean data : bean.getRows()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId()+""));
                        show.add(icon);
                    }
                    iconAdapter.setData(show);
                }
            });
        });

    }

    @Override
    public void resumeData() {
        get("/prod-api/api/gov-service-hotline/appeal/my-list", res -> {
            runOnUiThread(() -> {
                AppealBean bean = new Gson().fromJson(res, AppealBean.class);
                if(bean.getCode() == 200) {
                    listAdapter.setData(bean.getRows());
                }
            });
        });
    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mIconRv = findViewById(R.id.iconRv);
        mListRv = findViewById(R.id.listRv);
    }
}