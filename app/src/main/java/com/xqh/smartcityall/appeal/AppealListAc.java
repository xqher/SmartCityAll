package com.xqh.smartcityall.appeal;

import android.view.View;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AppealBean;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initRV;

public class AppealListAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private AppealAdapter listAdapter;
    @Override
    public String initTitle() {
        return "诉求列表";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {
        listAdapter = new AppealAdapter(activity, null);
        initRV(mListRv, listAdapter);

        mTodoBt.setVisibility(View.VISIBLE);
        mTodoBt.setText("新建诉求");
        mTodoBt.setOnClickListener(v -> jumpId(AppealCreateAc.class, getJumpId()));
    }

    @Override
    public void resumeData() {
        get("/prod-api/api/gov-service-hotline/appeal/list?appealCategoryId="+getJumpId(), res -> {
            runOnUiThread(() -> {
                AppealBean bean = new Gson().fromJson(res, AppealBean.class);
                if(bean.getCode() == 200) {
                    listAdapter.setData(bean.getRows());
                }
            });
        });
    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}