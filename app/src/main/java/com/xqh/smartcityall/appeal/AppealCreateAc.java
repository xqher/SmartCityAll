package com.xqh.smartcityall.appeal;

import android.content.Intent;
import android.provider.MediaStore;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.DefRowType;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;

public class AppealCreateAc extends BaseAc {


    private android.widget.Spinner mSpinner;
    private com.google.android.material.textfield.TextInputEditText mTopic;
    private com.google.android.material.textfield.TextInputEditText mUnder;
    private com.google.android.material.textfield.TextInputEditText mPhone;
    private com.google.android.material.textfield.TextInputEditText mContent;
    private android.widget.Button mUploadBt;
    private android.widget.Button mTodoBt;

    private List<DefRowType.RowsBean> types = new ArrayList<>();

    @Override
    public String initTitle() {
        return "新建诉求";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_appeal_create;
    }

    @Override
    public void initData() {
        mUploadBt.setOnClickListener(v -> {
            toast("懒得写");
        });
        get("/prod-api/api/gov-service-hotline/appeal-category/list", json -> {
            runOnUiThread(()->{
                DefRowType bean = new Gson().fromJson(json, DefRowType.class);
                if(bean.getCode()==200) {
                    types = bean.getRows();
                    List<String> tmp = new ArrayList<>();
                    for(int i = 0;i<types.size();i++) {
                        DefRowType.RowsBean row = types.get(i);
                        tmp.add(row.getName());
                    }
                    mSpinner.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, tmp));
                    for(int i = 0;i<types.size();i++) {
                        DefRowType.RowsBean row = types.get(i);
                        if((row.getId()+"").equals(getJumpId())) {
                            mSpinner.setSelection(i);
                        }
                    }
                }
            });
        });
        mTodoBt.setOnClickListener(v -> {
            String topic = mTopic.getText().toString();
            String under = mUnder.getText().toString();
            String phone = mPhone.getText().toString();
            String content = mContent.getText().toString();
            if (
                    topic.isEmpty()
                    || under.isEmpty()
                    || phone.isEmpty()
                    || content.isEmpty()
            ) {
                toast("未输入完成");
                return;
            }
            if(phone.length()<11) {
                toast("手机号码格式错误");
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("appealCategoryId", types.get(mSpinner.getSelectedItemPosition()).getId()+"");
            map.put("title", topic);
            map.put("content", content);
            map.put("undertaker", under);

            post("/prod-api/api/gov-service-hotline/appeal", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if(res.getCode() == 200) {
                        finish();
                    }
                });
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mSpinner = findViewById(R.id.spinner);
        mTopic = findViewById(R.id.topic);
        mUnder = findViewById(R.id.under);
        mPhone = findViewById(R.id.phone);
        mContent = findViewById(R.id.content);
        mUploadBt = findViewById(R.id.uploadBt);
        mTodoBt = findViewById(R.id.todoBt);
    }
}