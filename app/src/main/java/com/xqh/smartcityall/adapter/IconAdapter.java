package com.xqh.smartcityall.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.*;

public class IconAdapter extends RecyclerView.Adapter<IconAdapter.VH> {


    private List<IconBean> list;
    private Context ctx;
    public Runnable onClick;
    public IconBean posBean;

    private int divide;

    public IconAdapter(Context ctx, List<IconBean> list, int divide) {
        this.ctx = ctx;
        this.list = list;
        this.divide = divide;
    }

    public IconAdapter(Context ctx, List<IconBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<IconBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_icon, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_icon;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mIcon;
        private TextView mName;

        public VH(@NonNull View itemView) {
            super(itemView);
            if (divide > 0) {
                itemView.setLayoutParams(new LinearLayout.LayoutParams(activity.getResources().getDisplayMetrics().widthPixels / divide, -2));
            }
            initView();
        }

        public void onItem(IconBean bean) {

            if (bean.getIconRes() != 0) {
                mIcon.setImageResource(bean.getIconRes());
            } else {
                loadImg(bean.getIconImg(), mIcon);
            }
            mName.setText(getStr(bean.getIconName()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
            });
        }

        private void initView() {
            mIcon = findViewById(R.id.icon);
            mName = findViewById(R.id.name);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
