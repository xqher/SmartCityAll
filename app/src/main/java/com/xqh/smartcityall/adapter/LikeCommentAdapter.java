package com.xqh.smartcityall.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefComment;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;

public class LikeCommentAdapter extends RecyclerView.Adapter<LikeCommentAdapter.VH> {


    private List<DefComment.DataBean> list;
    private Context ctx;
    public Runnable onClick;
    public DefComment.DataBean posBean;

    public LikeCommentAdapter(Context ctx, List<DefComment.DataBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<DefComment.DataBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_comment;
        private ImageView mAvatar;
        private TextView mName;
        private TextView mContent;
        private ImageView mToLike;
        private TextView mLikeNum;
        private TextView mTime;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mAvatar.setVisibility(View.GONE);
            mTime.setVisibility(View.GONE);
            mStatus.setVisibility(View.GONE);
        }

        public void onItem(DefComment.DataBean bean, int pos) {

            mName.setText(getStr(bean.getUserName()));
            mContent.setText(Html.fromHtml(String.format("%s", getStr(bean.getContent())), Html.FROM_HTML_MODE_COMPACT));
            mLikeNum.setText(String.format("%s", getStr(bean.getLikeCount()+"")));
            mToLike.setImageResource(bean.isMyLikeState()?R.drawable.ic_liked:R.drawable.ic_unlike);

            mToLike.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                bean.setMyLikeState(!bean.isMyLikeState());
                bean.setLikeCount(bean.getLikeCount()+(bean.isMyLikeState()?1:-1));
                notifyItemChanged(pos);
            });
        }

        private void initView() {
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mContent = itemView.findViewById(R.id.content);
            mToLike = itemView.findViewById(R.id.toLike);
            mLikeNum = itemView.findViewById(R.id.likeNum);
            mTime = itemView.findViewById(R.id.time);
            mStatus = itemView.findViewById(R.id.status);

        }
    }
}
