package com.xqh.smartcityall.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ItemManifest;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ManifestAdapter extends RecyclerView.Adapter<ManifestAdapter.VH> {


    private List<ItemManifest> list;
    private Context ctx;
    public Runnable onClick;
    public ItemManifest posBean;

    public ManifestAdapter(Context ctx, List<ItemManifest> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<ItemManifest> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_manifest, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_manifest;
        private BaseAc activity = (BaseAc) ctx;
        private TextView mName;
        private TextView mValue;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(ItemManifest bean) {
            mName.setText(getStr(bean.getName()));
            mValue.setText(getStr(bean.getVal()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }

            });
        }

        private void initView() {
            mName = findViewById(R.id.name);
            mValue = findViewById(R.id.value);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
