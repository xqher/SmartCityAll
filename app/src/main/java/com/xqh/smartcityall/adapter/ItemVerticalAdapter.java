package com.xqh.smartcityall.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ItemVerticalAdapter extends RecyclerView.Adapter<ItemVerticalAdapter.VH> {


    private List<ItemList> list;
    private Context ctx;
    public Runnable onClick;
    public ItemList posBean;

    public ItemVerticalAdapter(Context ctx, List<ItemList> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<ItemList> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_vertical, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_vertical;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mIcon;
        private TextView mName;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(ItemList bean) {
            if (bean.isCur()) {
                mIcon.setImageResource(R.drawable.ic_bus);
            } else {
                mIcon.setImageResource(R.drawable.ic_circle);
            }
            mName.setText(getStr(bean.getTitle()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }

            });
        }

        private void initView() {
            mIcon = findViewById(R.id.icon);
            mName = findViewById(R.id.name);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
