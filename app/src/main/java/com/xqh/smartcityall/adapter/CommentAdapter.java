package com.xqh.smartcityall.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AvatarComment;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.isEmpty;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.VH> {


    private List<AvatarComment.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public AvatarComment.RowsBean posBean;

    public CommentAdapter(Context ctx, List<AvatarComment.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<AvatarComment.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_comment;
        private ImageView mAvatar;
        private TextView mName;
        private TextView mContent;
        private ImageView mToLike;
        private TextView mLikeNum;
        private TextView mTime;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mAvatar.setVisibility(View.GONE);
            mStatus.setVisibility(View.GONE);
            mToLike.setVisibility(View.GONE);
            mLikeNum.setVisibility(View.GONE);
//            mTime.setVisibility(View.GONE);
        }

        public void onItem(AvatarComment.RowsBean bean, int pos) {
            mName.setText(isEmpty(getStr(bean.getUserName()))?"匿名":getStr(bean.getUserName()));
            mContent.setText(Html.fromHtml(String.format("%s", getStr(bean.getContent())), Html.FROM_HTML_MODE_COMPACT));
            mTime.setText(bean.getCreateTime());
            mToLike.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
            });
        }

        private void initView() {
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mContent = itemView.findViewById(R.id.content);
            mToLike = itemView.findViewById(R.id.toLike);
            mLikeNum = itemView.findViewById(R.id.likeNum);
            mTime = itemView.findViewById(R.id.time);
            mStatus = itemView.findViewById(R.id.status);

        }
    }
}
