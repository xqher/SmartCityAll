package com.xqh.smartcityall.adapter;

import android.content.Context;
import android.os.Build;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ItemAdapter extends RecyclerView.Adapter<ItemAdapter.VH> {


    private List<ItemList> list;
    private Context ctx;
    public Runnable onClick;
    public ItemList posBean;

    public ItemAdapter(Context ctx, List<ItemList> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<ItemList> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
        }

        public void onItem(ItemList bean) {
            if (bean.getCoverId() != 0) {
                mCover.setImageResource(bean.getCoverId());
            } else {
                loadImg(bean.getCover(), mCover);
            }
            mTitle.setText(getStr(bean.getTitle()));
            mContent.setText(Html.fromHtml(getStr(bean.getContent()),Html.FROM_HTML_MODE_COMPACT));
            mDate.setText(getStr(bean.getTime()));
            mStatus.setText(getStr(bean.getStatus()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }

            });
        }

        private void initView() {
            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mDate = findViewById(R.id.date);
            mStatus = findViewById(R.id.status);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
