package com.xqh.smartcityall;

import android.app.AlertDialog;
import android.os.Handler;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import com.xqh.smartcityall.per.LoginAc;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;
import com.youth.banner.indicator.CircleIndicator;
import com.youth.banner.listener.OnPageChangeListener;

import java.util.Arrays;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.URL;
import static com.xqh.smartcityall.utils.Const.getSp;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.isEmpty;
import static com.xqh.smartcityall.utils.Const.setSp;

public class BannerActivity extends BaseAc {

    private com.youth.banner.Banner mBanner;
    private android.widget.TextView mNetTv;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "智慧城市";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_banner;
    }

    @Override
    public void initData() {

        if (!Const.getSp("first").isEmpty()) {
            boolean[] login = {false};
            Const.URL = getSp("url");
            Const.isLogin(login, ()->{
                if(login[0]) {
                    jump(MainActivity.class);
                }else {
                    toast("未登录或网络设置错误！");
                    setSp("token","");
                    jump(LoginAc.class);
                }
                finish();
            });
        } else {
            List<Integer> list = Arrays.asList(
                    R.drawable.introa,
                    R.drawable.introb,
                    R.drawable.introc,
                    R.drawable.introd,
                    R.drawable.introe
            );
            mBanner.setAdapter(new BannerImageAdapter<Integer>(list) {
                @Override
                public void onBindView(BannerImageHolder bannerImageHolder, Integer integer, int i, int i1) {
                    bannerImageHolder.imageView.setImageResource(integer);
                }
            }, false).setIndicator(new CircleIndicator(activity));
            mBanner.addOnPageChangeListener(new OnPageChangeListener() {
                @Override
                public void onPageScrolled(int i, float v, int i1) {

                }

                @Override
                public void onPageSelected(int i) {
                    if (i == list.size() - 1) {
                        mNetTv.setVisibility(View.VISIBLE);
                        mTodoBt.setVisibility(View.VISIBLE);
                    } else {
                        mNetTv.setVisibility(View.GONE);
                        mTodoBt.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onPageScrollStateChanged(int i) {

                }
            });
            mNetTv.setOnClickListener(v -> {
                EditText net = new EditText(activity);
                String url = getSp("url");
                if(!isEmpty(url)) {
                    url = url.substring(7);
                }
                net.setText(url);
                net.setHint("设置网络如192.168.1.1:3306");
                new AlertDialog.Builder(activity)
                        .setView(net)
                        .setPositiveButton("保存", (dialog, which) -> {
                            String inp = net.getText().toString();
                            if (inp.isEmpty()) {
                                toast("未输入内容");
                                return;
                            }
                            if (inp.length() < 10) {
                                toast("ip格式错误");
                                return;
                            }
                            URL =  "http://"+inp;
                            Const.setSp("url", URL);
                            toast("保存成功");
                        })
                        .setNegativeButton("取消", null)
                        .create()
                        .show();
            });
            mTodoBt.setOnClickListener(v -> {
                if (Const.getSp("url").isEmpty()) {
                    toast("未设置网络");
                    return;
                }
                setSp("first", "1");

                boolean[] login = {false};
                Const.isLogin(login,()->{
                    if(login[0]) {
                        jump(MainActivity.class);
                    }else {
                        setSp("token","");
                        jump(LoginAc.class);
                    }
                    finish();
                });

            });
        }
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mNetTv = findViewById(R.id.netTv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}