package com.xqh.smartcityall.pojo;

import java.util.List;

public class ConsultBean {
    /**
     * total : 1
     * rows : [{"searchValue":null,"createBy":null,"createTime":"2022-11-13 19:13:46","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":170,"fromUserId":1113055,"lawyerId":10,"legalExpertiseId":7,"content":"没有设置咨询状态的接口我咋做评价页面(","imageUrls":"/prod-api/profile/upload/2022/10/20/76ee6f63-cb5a-4794-bf04-a0874bb03ead.jpg","phone":"13725596768","state":"0","score":0,"evaluate":"","lawyerName":"陈宇律师","legalExpertiseName":null,"likeCount":90}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-11-13 19:13:46
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 170
         * fromUserId : 1113055
         * lawyerId : 10
         * legalExpertiseId : 7
         * content : 没有设置咨询状态的接口我咋做评价页面(
         * imageUrls : /prod-api/profile/upload/2022/10/20/76ee6f63-cb5a-4794-bf04-a0874bb03ead.jpg
         * phone : 13725596768
         * state : 0
         * score : 0
         * evaluate :
         * lawyerName : 陈宇律师
         * legalExpertiseName : null
         * likeCount : 90
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int fromUserId;
        private int lawyerId;
        private int legalExpertiseId;
        private String content;
        private String imageUrls;
        private String phone;
        private String state;
        private int score;
        private String evaluate;
        private String lawyerName;
        private Object legalExpertiseName;
        private int likeCount;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getFromUserId() {
            return fromUserId;
        }

        public void setFromUserId(int fromUserId) {
            this.fromUserId = fromUserId;
        }

        public int getLawyerId() {
            return lawyerId;
        }

        public void setLawyerId(int lawyerId) {
            this.lawyerId = lawyerId;
        }

        public int getLegalExpertiseId() {
            return legalExpertiseId;
        }

        public void setLegalExpertiseId(int legalExpertiseId) {
            this.legalExpertiseId = legalExpertiseId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImageUrls() {
            return imageUrls;
        }

        public void setImageUrls(String imageUrls) {
            this.imageUrls = imageUrls;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public int getScore() {
            return score;
        }

        public void setScore(int score) {
            this.score = score;
        }

        public String getEvaluate() {
            return evaluate;
        }

        public void setEvaluate(String evaluate) {
            this.evaluate = evaluate;
        }

        public String getLawyerName() {
            return lawyerName;
        }

        public void setLawyerName(String lawyerName) {
            this.lawyerName = lawyerName;
        }

        public Object getLegalExpertiseName() {
            return legalExpertiseName;
        }

        public void setLegalExpertiseName(Object legalExpertiseName) {
            this.legalExpertiseName = legalExpertiseName;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(int likeCount) {
            this.likeCount = likeCount;
        }

        public static class ParamsBean {
        }
    }
}
