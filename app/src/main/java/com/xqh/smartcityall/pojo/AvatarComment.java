package com.xqh.smartcityall.pojo;

import java.util.List;

public class AvatarComment {
    /**
     * total : 242
     * rows : [{"searchValue":null,"createBy":null,"createTime":"2022-11-12 21:12:45","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2124,"userId":1112338,"activityId":15,"content":"","commentTime":"2022-11-12 21:12:45","activityName":"现金流桌游","userName":"1312508730","nickName":"刘文涛5455","avatar":"1312316"},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 20:17:12","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2122,"userId":1112338,"activityId":15,"content":"hhhh","commentTime":"2022-11-12 20:17:12","activityName":"现金流桌游","userName":"1312508730","nickName":"刘文涛5455","avatar":"1312316"},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 20:14:44","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2121,"userId":1112338,"activityId":15,"content":"lwt","commentTime":"2022-11-12 20:14:44","activityName":"现金流桌游","userName":"1312508730","nickName":"刘文涛5455","avatar":"1312316"},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 18:58:39","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2120,"userId":1112674,"activityId":15,"content":"good","commentTime":"2022-11-12 18:58:39","activityName":"现金流桌游","userName":"YMX","nickName":"小袁同学","avatar":""},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 18:56:25","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2119,"userId":1112674,"activityId":15,"content":"6666","commentTime":"2022-11-12 18:56:25","activityName":"现金流桌游","userName":"YMX","nickName":"小袁同学","avatar":""},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 16:00:01","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2117,"userId":1113292,"activityId":15,"content":"sssaf","commentTime":"2022-11-12 16:00:01","activityName":"现金流桌游","userName":"ssa123","nickName":"ssa123","avatar":""},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 15:52:55","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2116,"userId":1113292,"activityId":15,"content":"lala ","commentTime":"2022-11-12 15:52:55","activityName":"现金流桌游","userName":"ssa123","nickName":"ssa123","avatar":""},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 15:52:39","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2115,"userId":1113292,"activityId":15,"content":"sdafadsfsad","commentTime":"2022-11-12 15:52:39","activityName":"现金流桌游","userName":"ssa123","nickName":"ssa123","avatar":""},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 15:33:29","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2114,"userId":1111883,"activityId":15,"content":"etyfv","commentTime":"2022-11-12 15:33:29","activityName":"现金流桌游","userName":"xzycyq","nickName":"xzy","avatar":"/profile/upload/2022/11/12/e049b3cd-4b75-4759-a048-f99b1c02c05f.jpg"},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 15:33:24","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2113,"userId":1111883,"activityId":15,"content":"fbwuegf","commentTime":"2022-11-12 15:33:24","activityName":"现金流桌游","userName":"xzycyq","nickName":"xzy","avatar":"/profile/upload/2022/11/12/e049b3cd-4b75-4759-a048-f99b1c02c05f.jpg"}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-11-12 21:12:45
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 2124
         * userId : 1112338
         * activityId : 15
         * content :
         * commentTime : 2022-11-12 21:12:45
         * activityName : 现金流桌游
         * userName : 1312508730
         * nickName : 刘文涛5455
         * avatar : 1312316
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int userId;
        private int activityId;
        private String content;
        private String commentTime;
        private String activityName;
        private String userName;
        private String nickName;
        private String avatar;

        private boolean myLikeState;
        private int likeCount;

        public boolean isMyLikeState() {
            return myLikeState;
        }

        public void setMyLikeState(boolean myLikeState) {
            this.myLikeState = myLikeState;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(int likeCount) {
            this.likeCount = likeCount;
        }

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getActivityId() {
            return activityId;
        }

        public void setActivityId(int activityId) {
            this.activityId = activityId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getCommentTime() {
            return commentTime;
        }

        public void setCommentTime(String commentTime) {
            this.commentTime = commentTime;
        }

        public String getActivityName() {
            return activityName;
        }

        public void setActivityName(String activityName) {
            this.activityName = activityName;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getNickName() {
            return nickName;
        }

        public void setNickName(String nickName) {
            this.nickName = nickName;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public static class ParamsBean {
        }
    }
}
