package com.xqh.smartcityall.pojo;

import java.util.List;

public class PostDetail {
    /**
     * msg : 操作成功
     * code : 200
     * data : {"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":8,"name":"钻石湾海滨驿站","coverImgUrl":"/prod-api/profile/upload/image/2022/03/14/b86fa8e1-b556-48a5-893e-e47f0dcbd6a1.jpg","imageUrls":"/prod-api/profile/upload/image/2022/03/14/651419af-5cbf-453c-8220-f97d5b91b916.jpg,/prod-api/profile/upload/image/2022/03/14/40403db4-9406-467b-8bd0-3a18b470d4a1.jpg,/prod-api/profile/upload/image/2022/03/14/386f74ef-6b76-417b-9ab5-24d2c42152d0.jpg,/prod-api/profile/upload/image/2022/03/14/a9ce5e76-76e6-4d46-83fc-f051cc534112.jpg,/prod-api/profile/upload/image/2022/03/14/8aff1cda-3f8f-44f3-ab24-20b5f05717fe.jpg,/prod-api/profile/upload/image/2022/03/14/b3db5d8c-0eaf-4638-8d82-3c00c5c53e1c.jpg,/prod-api/profile/upload/image/2022/03/14/e68c5e31-db0a-4e05-a791-2e0d031126da.jpg","bedsCountBoy":123,"bedsCountGirl":121,"address":"辽宁大连甘井子甘井子街道和硕街8号钻石湾未来里G3公寓1507 , 大连","phone":"17108303960-759","workTime":"8:00-11:00 13:00-16:00","introduce":"隔窗望去海景，夜晚特别好看，市内清新风格的装修，配备舒适的大床和床垫，相信您一定会舒舒服服的睡个好觉！除此之外卧室还有空调、衣柜、挂墙液晶电视、免费百兆wifi、以及独立的卫生间哦24小时热水是妥妥的！如果您想做饭，厨房免费提供给您，快来大展厨艺吧！","internalFacilities":"空调、衣柜、挂墙液晶电视、免费百兆wifi、以及独立的卫生间","surroundingFacilities":"钻石湾，山姆会员店","specialService":"免费厨房，可自己做饭","imageUrlList":["/prod-api/profile/upload/image/2022/03/14/651419af-5cbf-453c-8220-f97d5b91b916.jpg","/prod-api/profile/upload/image/2022/03/14/40403db4-9406-467b-8bd0-3a18b470d4a1.jpg","/prod-api/profile/upload/image/2022/03/14/386f74ef-6b76-417b-9ab5-24d2c42152d0.jpg","/prod-api/profile/upload/image/2022/03/14/a9ce5e76-76e6-4d46-83fc-f051cc534112.jpg","/prod-api/profile/upload/image/2022/03/14/8aff1cda-3f8f-44f3-ab24-20b5f05717fe.jpg","/prod-api/profile/upload/image/2022/03/14/b3db5d8c-0eaf-4638-8d82-3c00c5c53e1c.jpg","/prod-api/profile/upload/image/2022/03/14/e68c5e31-db0a-4e05-a791-2e0d031126da.jpg"]}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : null
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 8
         * name : 钻石湾海滨驿站
         * coverImgUrl : /prod-api/profile/upload/image/2022/03/14/b86fa8e1-b556-48a5-893e-e47f0dcbd6a1.jpg
         * imageUrls : /prod-api/profile/upload/image/2022/03/14/651419af-5cbf-453c-8220-f97d5b91b916.jpg,/prod-api/profile/upload/image/2022/03/14/40403db4-9406-467b-8bd0-3a18b470d4a1.jpg,/prod-api/profile/upload/image/2022/03/14/386f74ef-6b76-417b-9ab5-24d2c42152d0.jpg,/prod-api/profile/upload/image/2022/03/14/a9ce5e76-76e6-4d46-83fc-f051cc534112.jpg,/prod-api/profile/upload/image/2022/03/14/8aff1cda-3f8f-44f3-ab24-20b5f05717fe.jpg,/prod-api/profile/upload/image/2022/03/14/b3db5d8c-0eaf-4638-8d82-3c00c5c53e1c.jpg,/prod-api/profile/upload/image/2022/03/14/e68c5e31-db0a-4e05-a791-2e0d031126da.jpg
         * bedsCountBoy : 123
         * bedsCountGirl : 121
         * address : 辽宁大连甘井子甘井子街道和硕街8号钻石湾未来里G3公寓1507 , 大连
         * phone : 17108303960-759
         * workTime : 8:00-11:00 13:00-16:00
         * introduce : 隔窗望去海景，夜晚特别好看，市内清新风格的装修，配备舒适的大床和床垫，相信您一定会舒舒服服的睡个好觉！除此之外卧室还有空调、衣柜、挂墙液晶电视、免费百兆wifi、以及独立的卫生间哦24小时热水是妥妥的！如果您想做饭，厨房免费提供给您，快来大展厨艺吧！
         * internalFacilities : 空调、衣柜、挂墙液晶电视、免费百兆wifi、以及独立的卫生间
         * surroundingFacilities : 钻石湾，山姆会员店
         * specialService : 免费厨房，可自己做饭
         * imageUrlList : ["/prod-api/profile/upload/image/2022/03/14/651419af-5cbf-453c-8220-f97d5b91b916.jpg","/prod-api/profile/upload/image/2022/03/14/40403db4-9406-467b-8bd0-3a18b470d4a1.jpg","/prod-api/profile/upload/image/2022/03/14/386f74ef-6b76-417b-9ab5-24d2c42152d0.jpg","/prod-api/profile/upload/image/2022/03/14/a9ce5e76-76e6-4d46-83fc-f051cc534112.jpg","/prod-api/profile/upload/image/2022/03/14/8aff1cda-3f8f-44f3-ab24-20b5f05717fe.jpg","/prod-api/profile/upload/image/2022/03/14/b3db5d8c-0eaf-4638-8d82-3c00c5c53e1c.jpg","/prod-api/profile/upload/image/2022/03/14/e68c5e31-db0a-4e05-a791-2e0d031126da.jpg"]
         */

        private Object searchValue;
        private Object createBy;
        private Object createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String name;
        private String coverImgUrl;
        private String imageUrls;
        private int bedsCountBoy;
        private int bedsCountGirl;
        private String address;
        private String phone;
        private String workTime;
        private String introduce;
        private String internalFacilities;
        private String surroundingFacilities;
        private String specialService;
        private List<String> imageUrlList;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getCoverImgUrl() {
            return coverImgUrl;
        }

        public void setCoverImgUrl(String coverImgUrl) {
            this.coverImgUrl = coverImgUrl;
        }

        public String getImageUrls() {
            return imageUrls;
        }

        public void setImageUrls(String imageUrls) {
            this.imageUrls = imageUrls;
        }

        public int getBedsCountBoy() {
            return bedsCountBoy;
        }

        public void setBedsCountBoy(int bedsCountBoy) {
            this.bedsCountBoy = bedsCountBoy;
        }

        public int getBedsCountGirl() {
            return bedsCountGirl;
        }

        public void setBedsCountGirl(int bedsCountGirl) {
            this.bedsCountGirl = bedsCountGirl;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getWorkTime() {
            return workTime;
        }

        public void setWorkTime(String workTime) {
            this.workTime = workTime;
        }

        public String getIntroduce() {
            return introduce;
        }

        public void setIntroduce(String introduce) {
            this.introduce = introduce;
        }

        public String getInternalFacilities() {
            return internalFacilities;
        }

        public void setInternalFacilities(String internalFacilities) {
            this.internalFacilities = internalFacilities;
        }

        public String getSurroundingFacilities() {
            return surroundingFacilities;
        }

        public void setSurroundingFacilities(String surroundingFacilities) {
            this.surroundingFacilities = surroundingFacilities;
        }

        public String getSpecialService() {
            return specialService;
        }

        public void setSpecialService(String specialService) {
            this.specialService = specialService;
        }

        public List<String> getImageUrlList() {
            return imageUrlList;
        }

        public void setImageUrlList(List<String> imageUrlList) {
            this.imageUrlList = imageUrlList;
        }

        public static class ParamsBean {
        }
    }
}
