package com.xqh.smartcityall.pojo;

import java.util.List;

public class AppealBean {
    /**
     * total : 13
     * rows : [{"searchValue":null,"createBy":null,"createTime":"2022-03-12 07:49:22","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":17,"userId":2,"appealCategoryId":13,"title":"学校放学时间早","content":"我看其他小学都是6点放学，为什么XX小学就四点放学呢？","undertaker":"教育局","state":"1","detailResult":"该小学为私立小学，相关政策请按照该学校内部制定执行。","imgUrl":null,"appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-03-12 07:50:30","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":18,"userId":2,"appealCategoryId":13,"title":"小学生艺术问题建议","content":"现在很多学生只顾着学习文化课，没有学习艺术的时间，希望能够多多增加此类课程让孩子们丰富才艺。","undertaker":"教育局","state":"1","detailResult":"我们已收到您的建议，我们会考虑安排此类型的课程。","imgUrl":null,"appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-04 09:55:12","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":75,"userId":1111149,"appealCategoryId":13,"title":"教育问题","content":"什么时候可以免费上学","undertaker":"教育厅","state":"0","detailResult":"","imgUrl":"/profile/upload/2022/10/04/66a81e30-4adc-4dd6-abbf-2ac75ac68101.jpg","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-05 21:19:01","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":82,"userId":1111192,"appealCategoryId":13,"title":"周豪是傻逼","content":"周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼周豪是傻逼","undertaker":"湖南化工","state":"0","detailResult":"","imgUrl":"","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-08 16:53:03","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":106,"userId":1111425,"appealCategoryId":13,"title":"关于如何预防青少年脑瘫问题","content":"阿巴阿巴阿巴阿巴","undertaker":"北京大学","state":"0","detailResult":"","imgUrl":"/profile/upload/2022/10/08/a4070dfb-e1d1-4e96-bff6-44fffeeb7d6e.jpg","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-16 19:47:57","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":184,"userId":1111287,"appealCategoryId":13,"title":"sada","content":"asd","undertaker":"asd","state":"0","detailResult":"","imgUrl":"blob:http://localhost:8083/630bad8b-2256-47ef-9183-57174b0476f6","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-28 19:10:58","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":216,"userId":1112209,"appealCategoryId":13,"title":"dx","content":"dx","undertaker":"dx","state":"0","detailResult":"","imgUrl":"","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-28 20:08:23","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":222,"userId":1112311,"appealCategoryId":13,"title":"32132","content":"31232132","undertaker":"教育相关","state":"0","detailResult":"","imgUrl":"","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-28 20:10:05","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":223,"userId":1112311,"appealCategoryId":13,"title":"","content":"","undertaker":"教育相关","state":"0","detailResult":"","imgUrl":"","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-10-28 20:11:12","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":224,"userId":1112311,"appealCategoryId":13,"title":"432","content":"3445","undertaker":"教育相关","state":"0","detailResult":"","imgUrl":"","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 11:16:26","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":273,"userId":1112311,"appealCategoryId":13,"title":"2313","content":"23213","undertaker":"123132","state":"0","detailResult":"","imgUrl":"blob:http://localhost:8080/357b817c-c2f7-471b-9529-e695d7d34600","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-11-12 11:21:58","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":274,"userId":1112311,"appealCategoryId":13,"title":"23232","content":"3244343432","undertaker":"43443","state":"0","detailResult":"","imgUrl":"blob:http://localhost:8080/77e70b5a-d55a-472a-80f2-4a7bfcda1836","appealCategoryName":"教育相关"},{"searchValue":null,"createBy":null,"createTime":"2022-11-13 10:06:46","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":277,"userId":1113183,"appealCategoryId":13,"title":"11111223423","content":"erfhfdddf\n","undertaker":"eeerrrr\n","state":"0","detailResult":"","imgUrl":null,"appealCategoryName":"教育相关"}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-03-12 07:49:22
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 17
         * userId : 2
         * appealCategoryId : 13
         * title : 学校放学时间早
         * content : 我看其他小学都是6点放学，为什么XX小学就四点放学呢？
         * undertaker : 教育局
         * state : 1
         * detailResult : 该小学为私立小学，相关政策请按照该学校内部制定执行。
         * imgUrl : null
         * appealCategoryName : 教育相关
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int userId;
        private int appealCategoryId;
        private String title;
        private String content;
        private String undertaker;
        private String state;
        private String detailResult;
        private Object imgUrl;
        private String appealCategoryName;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getAppealCategoryId() {
            return appealCategoryId;
        }

        public void setAppealCategoryId(int appealCategoryId) {
            this.appealCategoryId = appealCategoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getUndertaker() {
            return undertaker;
        }

        public void setUndertaker(String undertaker) {
            this.undertaker = undertaker;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDetailResult() {
            return detailResult;
        }

        public void setDetailResult(String detailResult) {
            this.detailResult = detailResult;
        }

        public Object getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(Object imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getAppealCategoryName() {
            return appealCategoryName;
        }

        public void setAppealCategoryName(String appealCategoryName) {
            this.appealCategoryName = appealCategoryName;
        }

        public static class ParamsBean {
        }
    }
}
