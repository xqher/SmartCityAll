package com.xqh.smartcityall.pojo;

import java.util.List;

public class HouseBean {
    /**
     * total : 1551
     * rows : [{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":1,"sourceName":"泉水B6区泉水小学北站附近多层一室一厅得房率高\n","address":"泉水B6区泉水小学北站附近多层一室一厅得房率高","areaSize":52,"tel":"18546474545","price":"16262/㎡","houseType":"二手","pic":"/prod-api/profile/upload/image/2021/05/17/71ac2d26-4504-412d-81f1-0749f64b42d7.png","description":"房主现在比较着急 房子如果真看好的话 价格可议 手续这边齐全 房子没有抵押 产权证在手 随时配合过户 房主现在已经搬走了 房子现在是空置状态"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":2,"sourceName":"西安路08年框架电梯房 70年LOFT 一室一厅封闭小区","address":"西安路08年框架电梯房 70年LOFT 一室一厅封闭小区","areaSize":88,"tel":"18546474547","price":"28300/㎡","houseType":"租房","pic":"/prod-api/profile/upload/image/2021/05/17/dc8951f9-8eff-40df-8713-166b8ccf7a66.jpg","description":"第五郡经典一室小房好楼层自住保持非常好小区一共36栋楼,8栋小高,28栋洋,小区绿树"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":3,"sourceName":"大华实验双学 区长江路 好楼层 单价超低 看房方便","address":"数码路118号","areaSize":100,"tel":"18546474549","price":"21000/㎡","houseType":"二手","pic":"/prod-api/profile/upload/image/2021/05/17/ca7d9a19-143d-4579-9cbe-574bb3be737f.jpg","description":"我去看过这个房子，位置好，不挡光，中间楼层，精装修赠送全部家具家电，拎包即住，看房方便，照片视频等都是实景拍摄，欢迎随时咨询。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":4,"sourceName":"万达金石天成，首付10万，精装婚房，即买即住，随时看房。","address":"高新区万达广场","areaSize":88,"tel":"18546474543","price":"1500/月","houseType":"租房","pic":"/prod-api/profile/upload/image/2021/05/17/8abdd6f4-0014-4a30-8ea0-b06835c0493a.png","description":"房子是外走廊的，桥北不过道，离金三小学很近，真便宜，65万！看好还能谈！1、房子的地角非常好，在金三小学北侧不足百米，位置安静，无坡，出行方便，生活便利。2、是4楼的房子，楼间距大，采光好！3、一室户型，南北向，通风好，居住舒适。4，简单的装修，保持的干净利索，户型标准！"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":5,"sourceName":"金二76中双重点学 区 小面积总房款低 三楼楼层好","address":"华东路288号","areaSize":122,"tel":"18546474548","price":"2000/月","houseType":"租房","pic":"/prod-api/profile/upload/image/2021/05/17/e3db565e-8872-492d-a03b-047ee11c47ab.jpg","description":"房型正气，户型为南北通厅，客厅朝南，有大落地窗另赠送约6平米外置阳台，不算产权证面积，南卧室，精装修。没有一点浪费面积，得房率高达83%。全明户型，采光好。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":6,"sourceName":"中芥费三千第五郡 1室2厅 南北通透 56平居家自住 保养好","address":"中山路南路118号","areaSize":112,"tel":"18546474534","price":"18200/㎡","houseType":"楼盘","pic":"/prod-api/profile/upload/image/2021/05/17/c2a7f67a-d26b-4e76-9bef-6fd4eeafe712.jpg","description":"交通：轻轨3号线，大连开发区至金石滩中巴，金石滩区内公交。幼儿园：香港伟才幼儿园，小学：金石滩实验小学中学：金石滩实验中学，金石滩十三中，枫叶初中，枫叶高中。大学：鲁美，沈阳音乐学院，大连民族大学。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":7,"sourceName":"精装loft，一居室，配套完善，单身青年看过来，你该脱单了","address":"联合路65号","areaSize":97,"tel":"18546474521","price":"21200/㎡","houseType":"楼盘","pic":"/prod-api/profile/upload/image/2021/05/17/1923a63b-3946-4c64-8479-0b438c12d951.jpg","description":"交通：轻轨3号线，大连开发区至金石滩中巴，金石滩区内公交。幼儿园：香港伟才幼儿园，小学：金石滩实验小学中学：金石滩实验中学，金石滩十三中，枫叶初中，枫叶高中。大学：鲁美，沈阳音乐学院，大连民族大学。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":8,"sourceName":"阳光生态城一室一厅南向步梯洋房中间楼层精装修采光好近市场","address":"联合路67号","areaSize":111,"tel":"18546474552","price":"22000/㎡","houseType":"中介","pic":"/prod-api/profile/upload/image/2021/05/17/da2cfbaf-a360-4c8f-a252-4c66c68cc738.jpg","description":"交通：轻轨3号线，大连开发区至金石滩中巴，金石滩区内公交。幼儿园：香港伟才幼儿园，小学：金石滩实验小学中学：金石滩实验中学，金石滩十三中，枫叶初中，枫叶高中。大学：鲁美，沈阳音乐学院，大连民族大学。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":9,"sourceName":"城中村一室一厅南向","address":"联合路77号","areaSize":111,"tel":"18546474552","price":"22000/㎡","houseType":"中介","pic":"/prod-api/profile/upload/image/2021/05/17/572c0999-805a-4469-bf75-6277c21bd9fb.jpg","description":"交通：轻轨2号线，大连开发区至金石滩中巴，金石滩区内公交。幼儿园：香港伟才幼儿园，小学：金石滩实验小学中学：金石滩实验中学，金石滩十三中，枫叶初中，枫叶高中。大学：鲁美，沈阳音乐学院，大连民族大学。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":10,"sourceName":"绿水山居，一居室，配套完善","address":"人民路96号","areaSize":97,"tel":"18546474521","price":"21200/㎡","houseType":"中介","pic":"/prod-api/profile/upload/image/2021/05/17/170dbb3f-afb7-4fd8-8ed3-6a8a0597b1c5.jpg","description":"交通：轻轨1号线，大连开发区至金石滩中巴，金石滩区内公交。幼儿园：香港伟才幼儿园，小学：金石滩实验小学中学：金石滩实验中学，金石滩十三中，枫叶初中，枫叶高中。大学：鲁美，沈阳音乐学院，大连民族大学。"},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":11,"sourceName":"佳兆业·水岸兰亭","address":"[ 开发区 - 保税区 ] 通港路218号  ","areaSize":126,"tel":" 400 017 8372","price":"9500元/㎡","houseType":"楼盘","pic":"/prod-api/profile/upload/image/2021/05/17/2b289ef6-8f42-4e87-9a0c-057051fd76c3.jpg","description":"佳兆业·水岸兰亭楼盘频道，提供大连佳兆业·水岸兰亭房价，楼盘户型，楼盘产权年限，物业费，开发商，售楼电话，绿化率，周边配套，商场，超市，学校，医院，银行，周边地图交通等楼盘信息。"}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : null
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 1
         * sourceName : 泉水B6区泉水小学北站附近多层一室一厅得房率高
         * address : 泉水B6区泉水小学北站附近多层一室一厅得房率高
         * areaSize : 52
         * tel : 18546474545
         * price : 16262/㎡
         * houseType : 二手
         * pic : /prod-api/profile/upload/image/2021/05/17/71ac2d26-4504-412d-81f1-0749f64b42d7.png
         * description : 房主现在比较着急 房子如果真看好的话 价格可议 手续这边齐全 房子没有抵押 产权证在手 随时配合过户 房主现在已经搬走了 房子现在是空置状态
         */

        private Object searchValue;
        private Object createBy;
        private Object createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String sourceName;
        private String address;
        private int areaSize;
        private String tel;
        private String price;
        private String houseType;
        private String pic;
        private String description;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public Object getCreateTime() {
            return createTime;
        }

        public void setCreateTime(Object createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getSourceName() {
            return sourceName;
        }

        public void setSourceName(String sourceName) {
            this.sourceName = sourceName;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public int getAreaSize() {
            return areaSize;
        }

        public void setAreaSize(int areaSize) {
            this.areaSize = areaSize;
        }

        public String getTel() {
            return tel;
        }

        public void setTel(String tel) {
            this.tel = tel;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getHouseType() {
            return houseType;
        }

        public void setHouseType(String houseType) {
            this.houseType = houseType;
        }

        public String getPic() {
            return pic;
        }

        public void setPic(String pic) {
            this.pic = pic;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public static class ParamsBean {
        }
    }
}
