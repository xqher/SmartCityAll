package com.xqh.smartcityall.pojo;

public class PostFileDetail {
    /**
     * msg : 操作成功
     * code : 200
     * data : {"searchValue":null,"createBy":null,"createTime":"2022-03-14 08:30:30","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":18,"areaId":7,"title":"大连市就业重点群体政府补贴技能培训操作流程","author":"大连市人社局","content":"<p class=\"ql-align-justify\">\t一、申报条件<\/p><p class=\"ql-align-justify\">\t1.有劳动能力的16-24周岁贫困家庭子女；<\/p><p class=\"ql-align-justify\">\t2.城乡未继续升学的初高中毕业生；<\/p><p class=\"ql-align-justify\">\t3.农村转移就业劳动者；<\/p><p class=\"ql-align-justify\">\t4.下岗失业人员；<\/p><p class=\"ql-align-justify\">\t5.残疾人；<\/p><p class=\"ql-align-justify\">\t6.离校未就业高校毕业生；<\/p><p class=\"ql-align-justify\">\t7.确有劳动能力、就业意愿和培训需求、不具有按月领取养老金资格的人员；<\/p><p class=\"ql-align-justify\">\t8.退役军人；<\/p><p class=\"ql-align-justify\">\t9.灵活就业人员;<\/p><p class=\"ql-align-justify\">\t10.距刑满释放不足一年的服刑人员、戒毒人员（含强制隔离戒毒人员和社区戒毒社区康复人员）;<\/p><p class=\"ql-align-justify\">\t11.城乡贫困劳动力。<\/p><p class=\"ql-align-justify\">\t以上条件符合其一即可。<\/p><p class=\"ql-align-justify\">\t二、申报材料<\/p><p class=\"ql-align-justify\">\t（一）个人申报培训，需提供如下材料：<\/p><p class=\"ql-align-justify\">\t1.本人身份证；<\/p><p class=\"ql-align-justify\">\t2.人员类别材料（贫困家庭子女需提供低保家庭或贫困残疾人家庭或建档立卡贫困家庭证明；城乡未继续升学的初高中毕业生需提供毕业证；农村转移就业劳动者需提供户口簿；下岗失业人员需提供《就业创业证》或《就业失业登记证》；残疾人需提供《残疾人证》；离校未就业高校毕业生提供毕业证；退役军人提供户籍所在地退役军人事务管理部门出具证明；服刑人员、戒毒人员提供户籍所在地司法部门或公安部门出具证明；城乡贫困劳动力需提供农村特困家庭、农村居民最低生活保障家庭、建档立卡贫困家庭、农村困难残疾家庭、城镇特困家庭、城镇居民最低生活保障家庭证明；非大连户籍人员除上述证件外，还需提供居住证）。<\/p><p class=\"ql-align-justify\">\t（二）培训机构申领培训补贴（含鉴定补贴），需提供如下材料：<\/p><p class=\"ql-align-justify\">\t1.&nbsp;取得相应证书学员花名册及培训补贴相关申报表（信息系统打印）；<\/p><p class=\"ql-align-justify\">\t2.&nbsp;学员取得的职业资格证书（或职业技能等级证书、专项职业能力证书、培训合格证书）编号或复印件；<\/p><p class=\"ql-align-justify\">\t3.培训机构银行开户行及账号（首次申请时提供）;<\/p><p class=\"ql-align-justify\">\t4.职业培训机构的行政事业性收费票据（或税务发票）(资金申请审核通过后提供)等。<\/p><p class=\"ql-align-justify\">\t三、办理流程<\/p><p class=\"ql-align-justify\">\t1.申请培训资格。退役军人到户籍所在地退役军人事务管理部门开具证明；服刑人员、戒毒人员提供户籍所在地司法部门出具证明；其余人员持相关证件到就近街道（乡镇）劳动保障事务所申领\u201c职业技能培训券\u201d。<\/p><p class=\"ql-align-justify\">\t2.培训报名。退役军人持退役军人事务管理部门开具证明，服刑人员、戒毒人员持司法部门开具证明，其余人员持\u201c职业技能培训券\u201d，自主选择政府认定培训机构报名参加培训；<\/p><p class=\"ql-align-justify\">\t3.组织实施。培训机构开班备案审核通过后，组织学员开展培训。<\/p><p>\t4.补贴申请。就业重点群体在结束培训及鉴定考试，并取得职业资格证书（或职业技能等级证书、专项职业能力证书、培训合格证书）后，培训机构向所在地区就业培训管理部门提出补贴申请，并提交申请材料；<\/p><p class=\"ql-align-justify\">\t5.补贴发放。申请材料审核通过且公示5个工作日，并信用核查无异议后，将补贴资金支付到培训机构银行账户。<\/p><p>\t四、补贴标准<\/p><p>\t按照大连市政府补贴培训职业（工种）目录（2020年版）补贴标准执行。<\/p><p>\t&nbsp;<\/p><p>\t2020年10月27日修订<\/p>"}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-03-14 08:30:30
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 18
         * areaId : 7
         * title : 大连市就业重点群体政府补贴技能培训操作流程
         * author : 大连市人社局
         * content : <p class="ql-align-justify">	一、申报条件</p><p class="ql-align-justify">	1.有劳动能力的16-24周岁贫困家庭子女；</p><p class="ql-align-justify">	2.城乡未继续升学的初高中毕业生；</p><p class="ql-align-justify">	3.农村转移就业劳动者；</p><p class="ql-align-justify">	4.下岗失业人员；</p><p class="ql-align-justify">	5.残疾人；</p><p class="ql-align-justify">	6.离校未就业高校毕业生；</p><p class="ql-align-justify">	7.确有劳动能力、就业意愿和培训需求、不具有按月领取养老金资格的人员；</p><p class="ql-align-justify">	8.退役军人；</p><p class="ql-align-justify">	9.灵活就业人员;</p><p class="ql-align-justify">	10.距刑满释放不足一年的服刑人员、戒毒人员（含强制隔离戒毒人员和社区戒毒社区康复人员）;</p><p class="ql-align-justify">	11.城乡贫困劳动力。</p><p class="ql-align-justify">	以上条件符合其一即可。</p><p class="ql-align-justify">	二、申报材料</p><p class="ql-align-justify">	（一）个人申报培训，需提供如下材料：</p><p class="ql-align-justify">	1.本人身份证；</p><p class="ql-align-justify">	2.人员类别材料（贫困家庭子女需提供低保家庭或贫困残疾人家庭或建档立卡贫困家庭证明；城乡未继续升学的初高中毕业生需提供毕业证；农村转移就业劳动者需提供户口簿；下岗失业人员需提供《就业创业证》或《就业失业登记证》；残疾人需提供《残疾人证》；离校未就业高校毕业生提供毕业证；退役军人提供户籍所在地退役军人事务管理部门出具证明；服刑人员、戒毒人员提供户籍所在地司法部门或公安部门出具证明；城乡贫困劳动力需提供农村特困家庭、农村居民最低生活保障家庭、建档立卡贫困家庭、农村困难残疾家庭、城镇特困家庭、城镇居民最低生活保障家庭证明；非大连户籍人员除上述证件外，还需提供居住证）。</p><p class="ql-align-justify">	（二）培训机构申领培训补贴（含鉴定补贴），需提供如下材料：</p><p class="ql-align-justify">	1.&nbsp;取得相应证书学员花名册及培训补贴相关申报表（信息系统打印）；</p><p class="ql-align-justify">	2.&nbsp;学员取得的职业资格证书（或职业技能等级证书、专项职业能力证书、培训合格证书）编号或复印件；</p><p class="ql-align-justify">	3.培训机构银行开户行及账号（首次申请时提供）;</p><p class="ql-align-justify">	4.职业培训机构的行政事业性收费票据（或税务发票）(资金申请审核通过后提供)等。</p><p class="ql-align-justify">	三、办理流程</p><p class="ql-align-justify">	1.申请培训资格。退役军人到户籍所在地退役军人事务管理部门开具证明；服刑人员、戒毒人员提供户籍所在地司法部门出具证明；其余人员持相关证件到就近街道（乡镇）劳动保障事务所申领“职业技能培训券”。</p><p class="ql-align-justify">	2.培训报名。退役军人持退役军人事务管理部门开具证明，服刑人员、戒毒人员持司法部门开具证明，其余人员持“职业技能培训券”，自主选择政府认定培训机构报名参加培训；</p><p class="ql-align-justify">	3.组织实施。培训机构开班备案审核通过后，组织学员开展培训。</p><p>	4.补贴申请。就业重点群体在结束培训及鉴定考试，并取得职业资格证书（或职业技能等级证书、专项职业能力证书、培训合格证书）后，培训机构向所在地区就业培训管理部门提出补贴申请，并提交申请材料；</p><p class="ql-align-justify">	5.补贴发放。申请材料审核通过且公示5个工作日，并信用核查无异议后，将补贴资金支付到培训机构银行账户。</p><p>	四、补贴标准</p><p>	按照大连市政府补贴培训职业（工种）目录（2020年版）补贴标准执行。</p><p>	&nbsp;</p><p>	2020年10月27日修订</p>
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int areaId;
        private String title;
        private String author;
        private String content;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getAreaId() {
            return areaId;
        }

        public void setAreaId(int areaId) {
            this.areaId = areaId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public static class ParamsBean {
        }
    }
}
