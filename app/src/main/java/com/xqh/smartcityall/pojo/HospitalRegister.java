package com.xqh.smartcityall.pojo;

import java.util.List;

public class HospitalRegister {
    /**
     * total : 2
     * rows : [{"searchValue":null,"createBy":null,"createTime":"2022-10-16 11:04:11","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":707,"orderNo":"1665889451375","patientName":"xqh","categoryId":6,"type":"1","money":6,"reserveTime":"2022-10-17 10:30","status":null,"categoryName":"骨科","userId":1111859},{"searchValue":null,"createBy":null,"createTime":"2022-10-16 11:04:30","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":708,"orderNo":"1665889470075","patientName":"xqh","categoryId":6,"type":"2","money":6,"reserveTime":"2022-10-17 10:30","status":null,"categoryName":"骨科","userId":1111859}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-10-16 11:04:11
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 707
         * orderNo : 1665889451375
         * patientName : xqh
         * categoryId : 6
         * type : 1
         * money : 6.0
         * reserveTime : 2022-10-17 10:30
         * status : null
         * categoryName : 骨科
         * userId : 1111859
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String orderNo;
        private String patientName;
        private int categoryId;
        private String type;
        private double money;
        private String reserveTime;
        private Object status;
        private String categoryName;
        private int userId;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getOrderNo() {
            return orderNo;
        }

        public void setOrderNo(String orderNo) {
            this.orderNo = orderNo;
        }

        public String getPatientName() {
            return patientName;
        }

        public void setPatientName(String patientName) {
            this.patientName = patientName;
        }

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public double getMoney() {
            return money;
        }

        public void setMoney(double money) {
            this.money = money;
        }

        public String getReserveTime() {
            return reserveTime;
        }

        public void setReserveTime(String reserveTime) {
            this.reserveTime = reserveTime;
        }

        public Object getStatus() {
            return status;
        }

        public void setStatus(Object status) {
            this.status = status;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public static class ParamsBean {
        }
    }
}
