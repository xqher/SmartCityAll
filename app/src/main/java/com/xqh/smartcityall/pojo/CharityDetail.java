package com.xqh.smartcityall.pojo;

import java.util.List;

public class CharityDetail {
    /**
     * msg : 操作成功
     * code : 200
     * data : {"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:01:47","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":17,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/416304b3-48d7-41df-8f3f-010f6b4e7127.jpeg","typeId":13,"name":"美人鱼的安全港湾","author":"腾讯公益","activityAt":"2018-03-30","moneyTotal":15630201,"moneyNow":125334186,"description":"<p>母子相伴畅游（中华白海豚一生经历多次色彩变幻，刚刚出生的幼豚为深灰色，青年白海豚为浅灰色，成年白海豚则为漂亮的粉红色，老年白海豚更接近白色）<\/p><p>中华白海豚，是我国鲸豚动物中目前仅存的唯一一种国家一级保护动物，随着其踪迹在海南岛西南海域出现，改变了学术界和动物保护界关于海南岛周边没有中华白海豚分布的观点，把中华白海豚在我国水域的分布范围向南推进了近400公里。<\/p><p><br><\/p><p><span style=\"color: rgb(119, 119, 119);\">它们被称为\u201c妈祖鱼\u201d、\u201c海上大熊猫\u201d，有时也称\u201c美人鱼\u201d，是1997年香港回归祖国的吉祥物，是联系大陆和港澳台地区的纽带。中华白海豚性情活泼，在风和日丽的天气，常在水面跳跃嬉戏，有时甚至会全身跃出水面近1米高，幸运的话，在近海的岸边或能一睹它们的风采。<\/span><\/p><p><br><\/p><p><span style=\"color: rgb(119, 119, 119);\">然而，这样一种可爱的造物精灵、近海生态系统食物链的顶级捕食者,近年来却饱受着日益恶化的生存考验。一方面,白海豚胎生哺乳的特性,出生率低、成活率低,伶仃洋等海域老龄化超过一半；另一方面,大桥、核电站、穿梭不息的船只、过度捕捞的渔业、水下的爆破声、不断入海的废水\u2026\u2026都像一个个魔咒,让他们的生存之路越来越艰难。<\/span><\/p><p><br><\/p><p>蓝色部分为中华白海豚在海南岛西南海洋的分布区域及重点保护海域<\/p><p>保护中华白海豚，从来不是一个选择题。中国生物多样性保护与绿色发展基金会（简称中国绿发会）联合中国科学院深海科学与工程研究所（简称\u201c中科院深海所\u201d）、三亚蓝丝带海洋保护协会，迅速在我国海南岛西南海域发现中华白海豚的区域，开展\u201c中华白海豚保护地\u201d建设，并组建科研团队、宣传团队、志愿者团队\u2026\u2026开展各种保护工作，只为给中华白海豚一个快乐生存、繁衍的港湾。<\/p><p><br><\/p><p><br><\/p><h3>目前，中国绿发会已在南海海域开展多项生物多样性保护工作：<\/h3><p>&nbsp;1.与厦门大学合作开展中国沿海海洋濒危物种生态调查与数据库建设，以对濒危物种绿海龟的保护为重点，通过生态调查和定位跟踪，掌握绿海龟的洄游廊道及关键生境。项目总计92万元，目前，我会已拨付厦大科研团队46万。<\/p><p>&nbsp;2.2016年年初，举办2016生态系统服务与岛礁保护修复研讨会，重点围绕珊瑚礁和海岛的保护与修复等海洋生态问题展开讨论。<\/p><p>&nbsp;&nbsp;3.与海南南海热带海洋生物及病害研究所合作，共同在永乐环礁开展南海基地建设和中国珊瑚保护地的建设，重点开展绿海龟、珊瑚等海洋生物多样性的保护、研究工作。<\/p><p>&nbsp;&nbsp;4.与三亚蓝丝带海洋保护协会、中科院深海所共同筹备开展中华白海豚保护地前期工作<\/p>","isRecommend":"0","detailsList":[{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":35,"activityId":17,"itemName":"电脑","itemMoney":120000},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":36,"activityId":17,"itemName":"房屋租赁","itemMoney":1210000},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":37,"activityId":17,"itemName":"专家劳务费","itemMoney":1000000},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":38,"activityId":17,"itemName":"宣传团队支援费","itemMoney":12100201},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":39,"activityId":17,"itemName":"宣传手册设计费","itemMoney":1200000}],"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":13,"name":"自然保护","sort":4,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png"},"donateCount":31}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-03-14 12:01:47
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 17
         * imgUrl : /prod-api/profile/upload/image/2022/03/14/416304b3-48d7-41df-8f3f-010f6b4e7127.jpeg
         * typeId : 13
         * name : 美人鱼的安全港湾
         * author : 腾讯公益
         * activityAt : 2018-03-30
         * moneyTotal : 15630201
         * moneyNow : 125334186
         * description : <p>母子相伴畅游（中华白海豚一生经历多次色彩变幻，刚刚出生的幼豚为深灰色，青年白海豚为浅灰色，成年白海豚则为漂亮的粉红色，老年白海豚更接近白色）</p><p>中华白海豚，是我国鲸豚动物中目前仅存的唯一一种国家一级保护动物，随着其踪迹在海南岛西南海域出现，改变了学术界和动物保护界关于海南岛周边没有中华白海豚分布的观点，把中华白海豚在我国水域的分布范围向南推进了近400公里。</p><p><br></p><p><span style="color: rgb(119, 119, 119);">它们被称为“妈祖鱼”、“海上大熊猫”，有时也称“美人鱼”，是1997年香港回归祖国的吉祥物，是联系大陆和港澳台地区的纽带。中华白海豚性情活泼，在风和日丽的天气，常在水面跳跃嬉戏，有时甚至会全身跃出水面近1米高，幸运的话，在近海的岸边或能一睹它们的风采。</span></p><p><br></p><p><span style="color: rgb(119, 119, 119);">然而，这样一种可爱的造物精灵、近海生态系统食物链的顶级捕食者,近年来却饱受着日益恶化的生存考验。一方面,白海豚胎生哺乳的特性,出生率低、成活率低,伶仃洋等海域老龄化超过一半；另一方面,大桥、核电站、穿梭不息的船只、过度捕捞的渔业、水下的爆破声、不断入海的废水……都像一个个魔咒,让他们的生存之路越来越艰难。</span></p><p><br></p><p>蓝色部分为中华白海豚在海南岛西南海洋的分布区域及重点保护海域</p><p>保护中华白海豚，从来不是一个选择题。中国生物多样性保护与绿色发展基金会（简称中国绿发会）联合中国科学院深海科学与工程研究所（简称“中科院深海所”）、三亚蓝丝带海洋保护协会，迅速在我国海南岛西南海域发现中华白海豚的区域，开展“中华白海豚保护地”建设，并组建科研团队、宣传团队、志愿者团队……开展各种保护工作，只为给中华白海豚一个快乐生存、繁衍的港湾。</p><p><br></p><p><br></p><h3>目前，中国绿发会已在南海海域开展多项生物多样性保护工作：</h3><p>&nbsp;1.与厦门大学合作开展中国沿海海洋濒危物种生态调查与数据库建设，以对濒危物种绿海龟的保护为重点，通过生态调查和定位跟踪，掌握绿海龟的洄游廊道及关键生境。项目总计92万元，目前，我会已拨付厦大科研团队46万。</p><p>&nbsp;2.2016年年初，举办2016生态系统服务与岛礁保护修复研讨会，重点围绕珊瑚礁和海岛的保护与修复等海洋生态问题展开讨论。</p><p>&nbsp;&nbsp;3.与海南南海热带海洋生物及病害研究所合作，共同在永乐环礁开展南海基地建设和中国珊瑚保护地的建设，重点开展绿海龟、珊瑚等海洋生物多样性的保护、研究工作。</p><p>&nbsp;&nbsp;4.与三亚蓝丝带海洋保护协会、中科院深海所共同筹备开展中华白海豚保护地前期工作</p>
         * isRecommend : 0
         * detailsList : [{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":35,"activityId":17,"itemName":"电脑","itemMoney":120000},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":36,"activityId":17,"itemName":"房屋租赁","itemMoney":1210000},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":37,"activityId":17,"itemName":"专家劳务费","itemMoney":1000000},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":38,"activityId":17,"itemName":"宣传团队支援费","itemMoney":12100201},{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":39,"activityId":17,"itemName":"宣传手册设计费","itemMoney":1200000}]
         * type : {"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":13,"name":"自然保护","sort":4,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png"}
         * donateCount : 31
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String imgUrl;
        private int typeId;
        private String name;
        private String author;
        private String activityAt;
        private int moneyTotal;
        private int moneyNow;
        private String description;
        private String isRecommend;
        private TypeBean type;
        private int donateCount;
        private List<DetailsListBean> detailsList;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public int getTypeId() {
            return typeId;
        }

        public void setTypeId(int typeId) {
            this.typeId = typeId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getActivityAt() {
            return activityAt;
        }

        public void setActivityAt(String activityAt) {
            this.activityAt = activityAt;
        }

        public int getMoneyTotal() {
            return moneyTotal;
        }

        public void setMoneyTotal(int moneyTotal) {
            this.moneyTotal = moneyTotal;
        }

        public int getMoneyNow() {
            return moneyNow;
        }

        public void setMoneyNow(int moneyNow) {
            this.moneyNow = moneyNow;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIsRecommend() {
            return isRecommend;
        }

        public void setIsRecommend(String isRecommend) {
            this.isRecommend = isRecommend;
        }

        public TypeBean getType() {
            return type;
        }

        public void setType(TypeBean type) {
            this.type = type;
        }

        public int getDonateCount() {
            return donateCount;
        }

        public void setDonateCount(int donateCount) {
            this.donateCount = donateCount;
        }

        public List<DetailsListBean> getDetailsList() {
            return detailsList;
        }

        public void setDetailsList(List<DetailsListBean> detailsList) {
            this.detailsList = detailsList;
        }

        public static class ParamsBean {
        }

        public static class TypeBean {
            /**
             * searchValue : null
             * createBy : null
             * createTime : null
             * updateBy : null
             * updateTime : null
             * remark : null
             * params : {}
             * id : 13
             * name : 自然保护
             * sort : 4
             * imgUrl : /prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png
             */

            private Object searchValue;
            private Object createBy;
            private Object createTime;
            private Object updateBy;
            private Object updateTime;
            private Object remark;
            private ParamsBeanX params;
            private int id;
            private String name;
            private int sort;
            private String imgUrl;

            public Object getSearchValue() {
                return searchValue;
            }

            public void setSearchValue(Object searchValue) {
                this.searchValue = searchValue;
            }

            public Object getCreateBy() {
                return createBy;
            }

            public void setCreateBy(Object createBy) {
                this.createBy = createBy;
            }

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public Object getUpdateBy() {
                return updateBy;
            }

            public void setUpdateBy(Object updateBy) {
                this.updateBy = updateBy;
            }

            public Object getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(Object updateTime) {
                this.updateTime = updateTime;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public ParamsBeanX getParams() {
                return params;
            }

            public void setParams(ParamsBeanX params) {
                this.params = params;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public String getImgUrl() {
                return imgUrl;
            }

            public void setImgUrl(String imgUrl) {
                this.imgUrl = imgUrl;
            }

            public static class ParamsBeanX {
            }
        }

        public static class DetailsListBean {
            /**
             * searchValue : null
             * createBy : null
             * createTime : null
             * updateBy : null
             * updateTime : null
             * remark : null
             * params : {}
             * id : 35
             * activityId : 17
             * itemName : 电脑
             * itemMoney : 120000
             */

            private Object searchValue;
            private Object createBy;
            private Object createTime;
            private Object updateBy;
            private Object updateTime;
            private Object remark;
            private ParamsBeanXX params;
            private int id;
            private int activityId;
            private String itemName;
            private int itemMoney;

            public Object getSearchValue() {
                return searchValue;
            }

            public void setSearchValue(Object searchValue) {
                this.searchValue = searchValue;
            }

            public Object getCreateBy() {
                return createBy;
            }

            public void setCreateBy(Object createBy) {
                this.createBy = createBy;
            }

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public Object getUpdateBy() {
                return updateBy;
            }

            public void setUpdateBy(Object updateBy) {
                this.updateBy = updateBy;
            }

            public Object getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(Object updateTime) {
                this.updateTime = updateTime;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public ParamsBeanXX getParams() {
                return params;
            }

            public void setParams(ParamsBeanXX params) {
                this.params = params;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getActivityId() {
                return activityId;
            }

            public void setActivityId(int activityId) {
                this.activityId = activityId;
            }

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public int getItemMoney() {
                return itemMoney;
            }

            public void setItemMoney(int itemMoney) {
                this.itemMoney = itemMoney;
            }

            public static class ParamsBeanXX {
            }
        }
    }
}
