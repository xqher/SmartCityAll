package com.xqh.smartcityall.pojo;

import java.util.List;

public class CharityBean {
    /**
     * total : 20
     * rows : [{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:01:47","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":17,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/416304b3-48d7-41df-8f3f-010f6b4e7127.jpeg","typeId":13,"name":"美人鱼的安全港湾","author":"腾讯公益","activityAt":"2018-03-30","moneyTotal":15630201,"moneyNow":125334186,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":13,"name":"自然保护","sort":4,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png"},"donateCount":31},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:20:01","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":22,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/e4fadab0-2aeb-43c2-9a73-22b6a79e6de6.jpeg","typeId":10,"name":"早产宝贝救援计划","author":"腾讯公益","activityAt":"2018-03-30","moneyTotal":2920000,"moneyNow":1037251796,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":10,"name":"疾病救助","sort":1,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/380a9465-d483-4a73-9f14-892db2e13b7a.png"},"donateCount":50},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:30:17","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":26,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/5ea9e3f9-9819-49ba-8a2a-4bf237293425.jpeg","typeId":14,"name":"HELLO小孩快乐套餐","author":"腾讯公益","activityAt":"2018-03-30","moneyTotal":4300000,"moneyNow":10225455,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":14,"name":"关爱留守儿童","sort":5,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/3146861b-4c85-4c29-98b1-9f8e39dd8bff.png"},"donateCount":15},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:10:05","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":19,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/faf2d1e0-3599-437e-ab72-9521b8555774.jpeg","typeId":13,"name":"卫蓝侠守护碧水蓝天","author":"腾讯公益","activityAt":"2018-03-28","moneyTotal":4430000,"moneyNow":69173,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":13,"name":"自然保护","sort":4,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png"},"donateCount":7},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:39:42","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":30,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/5090aa2d-e894-4915-ae92-96828880ddf0.jpeg","typeId":17,"name":"让果树带父母回乡","author":"腾讯公益","activityAt":"2018-03-27","moneyTotal":9394600,"moneyNow":16721,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":17,"name":"其他","sort":8,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/7c53f458-c34a-4535-a28a-fb60e0afb525.png"},"donateCount":7},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:16:52","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":21,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/d2a743fe-d686-40af-a881-231901d17303.png","typeId":10,"name":"9958急救储备金计划","author":"腾讯公益","activityAt":"2018-03-23","moneyTotal":64320000,"moneyNow":1626120,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":10,"name":"疾病救助","sort":1,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/380a9465-d483-4a73-9f14-892db2e13b7a.png"},"donateCount":19},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:06:18","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":18,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/a0a96e7a-ff81-4764-8ab9-7490e2c3a011.jpeg","typeId":13,"name":"幸福家园生态扶贫","author":"腾讯公益","activityAt":"2018-03-21","moneyTotal":4221100,"moneyNow":43931,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":13,"name":"自然保护","sort":4,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png"},"donateCount":9},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:22:11","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":23,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/fc40f86c-7acf-4bfa-b1b9-0c3d70f9a9da.jpeg","typeId":15,"name":"贫困白内障的光明","author":"腾讯公益","activityAt":"2018-03-21","moneyTotal":25280111,"moneyNow":91664522,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":15,"name":"残障病人","sort":6,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/fc16a4b5-9520-45fb-be7a-9dd7c2d7d196.png"},"donateCount":21},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 11:50:53","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":14,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/15df470e-f691-4c78-b4cb-3d0b3ea0d49b.jpeg","typeId":15,"name":"橙色计划助听障儿童","author":"腾讯公益","activityAt":"2018-03-13","moneyTotal":14300000,"moneyNow":2112907477,"description":"","isRecommend":"1","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":15,"name":"残障病人","sort":6,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/fc16a4b5-9520-45fb-be7a-9dd7c2d7d196.png"},"donateCount":108},{"searchValue":null,"createBy":null,"createTime":"2022-03-14 12:25:09","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":24,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/d671a73e-3b6a-4cf5-8c97-64a7a3285a44.png","typeId":16,"name":"关怀困境母亲","author":"腾讯公益","activityAt":"2018-03-13","moneyTotal":20221000,"moneyNow":1014583355,"description":"","isRecommend":"0","detailsList":null,"type":{"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":16,"name":"夕阳老人","sort":7,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/712ae235-ff2c-4e9a-b953-69e2451ed89f.png"},"donateCount":14}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-03-14 12:01:47
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 17
         * imgUrl : /prod-api/profile/upload/image/2022/03/14/416304b3-48d7-41df-8f3f-010f6b4e7127.jpeg
         * typeId : 13
         * name : 美人鱼的安全港湾
         * author : 腾讯公益
         * activityAt : 2018-03-30
         * moneyTotal : 15630201
         * moneyNow : 125334186
         * description :
         * isRecommend : 0
         * detailsList : null
         * type : {"searchValue":null,"createBy":null,"createTime":null,"updateBy":null,"updateTime":null,"remark":null,"params":{},"id":13,"name":"自然保护","sort":4,"imgUrl":"/prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png"}
         * donateCount : 31
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String imgUrl;
        private int typeId;
        private String name;
        private String author;
        private String activityAt;
        private int moneyTotal;
        private int moneyNow;
        private String description;
        private String isRecommend;
        private Object detailsList;
        private TypeBean type;
        private int donateCount;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public int getTypeId() {
            return typeId;
        }

        public void setTypeId(int typeId) {
            this.typeId = typeId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAuthor() {
            return author;
        }

        public void setAuthor(String author) {
            this.author = author;
        }

        public String getActivityAt() {
            return activityAt;
        }

        public void setActivityAt(String activityAt) {
            this.activityAt = activityAt;
        }

        public int getMoneyTotal() {
            return moneyTotal;
        }

        public void setMoneyTotal(int moneyTotal) {
            this.moneyTotal = moneyTotal;
        }

        public int getMoneyNow() {
            return moneyNow;
        }

        public void setMoneyNow(int moneyNow) {
            this.moneyNow = moneyNow;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIsRecommend() {
            return isRecommend;
        }

        public void setIsRecommend(String isRecommend) {
            this.isRecommend = isRecommend;
        }

        public Object getDetailsList() {
            return detailsList;
        }

        public void setDetailsList(Object detailsList) {
            this.detailsList = detailsList;
        }

        public TypeBean getType() {
            return type;
        }

        public void setType(TypeBean type) {
            this.type = type;
        }

        public int getDonateCount() {
            return donateCount;
        }

        public void setDonateCount(int donateCount) {
            this.donateCount = donateCount;
        }

        public static class ParamsBean {
        }

        public static class TypeBean {
            /**
             * searchValue : null
             * createBy : null
             * createTime : null
             * updateBy : null
             * updateTime : null
             * remark : null
             * params : {}
             * id : 13
             * name : 自然保护
             * sort : 4
             * imgUrl : /prod-api/profile/upload/image/2022/03/14/c30b1103-fc68-4f96-aa3b-8a133f28516b.png
             */

            private Object searchValue;
            private Object createBy;
            private Object createTime;
            private Object updateBy;
            private Object updateTime;
            private Object remark;
            private ParamsBeanX params;
            private int id;
            private String name;
            private int sort;
            private String imgUrl;

            public Object getSearchValue() {
                return searchValue;
            }

            public void setSearchValue(Object searchValue) {
                this.searchValue = searchValue;
            }

            public Object getCreateBy() {
                return createBy;
            }

            public void setCreateBy(Object createBy) {
                this.createBy = createBy;
            }

            public Object getCreateTime() {
                return createTime;
            }

            public void setCreateTime(Object createTime) {
                this.createTime = createTime;
            }

            public Object getUpdateBy() {
                return updateBy;
            }

            public void setUpdateBy(Object updateBy) {
                this.updateBy = updateBy;
            }

            public Object getUpdateTime() {
                return updateTime;
            }

            public void setUpdateTime(Object updateTime) {
                this.updateTime = updateTime;
            }

            public Object getRemark() {
                return remark;
            }

            public void setRemark(Object remark) {
                this.remark = remark;
            }

            public ParamsBeanX getParams() {
                return params;
            }

            public void setParams(ParamsBeanX params) {
                this.params = params;
            }

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public int getSort() {
                return sort;
            }

            public void setSort(int sort) {
                this.sort = sort;
            }

            public String getImgUrl() {
                return imgUrl;
            }

            public void setImgUrl(String imgUrl) {
                this.imgUrl = imgUrl;
            }

            public static class ParamsBeanX {
            }
        }
    }
}
