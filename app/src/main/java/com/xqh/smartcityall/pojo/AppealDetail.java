package com.xqh.smartcityall.pojo;

public class AppealDetail {
    /**
     * msg : 操作成功
     * code : 200
     * data : {"searchValue":null,"createBy":null,"createTime":"2022-03-12 07:49:22","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":17,"userId":2,"appealCategoryId":13,"title":"学校放学时间早","content":"我看其他小学都是6点放学，为什么XX小学就四点放学呢？","undertaker":"教育局","state":"1","detailResult":"该小学为私立小学，相关政策请按照该学校内部制定执行。","imgUrl":null,"appealCategoryName":"教育相关"}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-03-12 07:49:22
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 17
         * userId : 2
         * appealCategoryId : 13
         * title : 学校放学时间早
         * content : 我看其他小学都是6点放学，为什么XX小学就四点放学呢？
         * undertaker : 教育局
         * state : 1
         * detailResult : 该小学为私立小学，相关政策请按照该学校内部制定执行。
         * imgUrl : null
         * appealCategoryName : 教育相关
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int userId;
        private int appealCategoryId;
        private String title;
        private String content;
        private String undertaker;
        private String state;
        private String detailResult;
        private String imgUrl;
        private String appealCategoryName;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getAppealCategoryId() {
            return appealCategoryId;
        }

        public void setAppealCategoryId(int appealCategoryId) {
            this.appealCategoryId = appealCategoryId;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getUndertaker() {
            return undertaker;
        }

        public void setUndertaker(String undertaker) {
            this.undertaker = undertaker;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDetailResult() {
            return detailResult;
        }

        public void setDetailResult(String detailResult) {
            this.detailResult = detailResult;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public String getAppealCategoryName() {
            return appealCategoryName;
        }

        public void setAppealCategoryName(String appealCategoryName) {
            this.appealCategoryName = appealCategoryName;
        }

        public static class ParamsBean {
        }
    }
}
