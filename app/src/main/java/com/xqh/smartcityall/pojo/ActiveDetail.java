package com.xqh.smartcityall.pojo;

public class ActiveDetail {
    /**
     * msg : 操作成功
     * code : 200
     * data : {"searchValue":null,"createBy":"admin","createTime":"2021-05-08 14:06:09","updateBy":"1112338","updateTime":"2022-11-12 21:58:21","remark":null,"params":{},"id":50,"name":"你想在20/30多岁的时候安定下来，拥有理想的工作，有合适的伴侣，有美好的生活方式吗?","content":"<p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Topic / 话题<\/strong><\/p><p>We as human, naturally born with limited time and energy, are always facing the dilemma of exploring different choices and possibilities, and developing and accumulating in one vertical area. As much as the time, energy and effort required to achieve anything in any particular field, to cultivate any meaningful and long-standing relationship with any partner, and to develop your own friend circle for you to feel home and connected in any city, would you be willing to give up the temptation of other possibilities that are potentially more exciting and interesting, more fulfilling and rewarding, more naturing and inspiring?<\/p><p>我们人类生来就只有有限的时间和精力，总是面临着探索不同的选择和可能性，在一个垂直领域发展和积累的困境。所需的时间、精力和努力实现在任何特定领域，培养与不同合作伙伴有意义的和长期的关系，并开发自己的朋友圈在任何城市建立家的感觉。你愿意为了这些，放弃其他可能的诱惑，可能更令人兴奋并有趣，更有意义和更多的回报，更自然的和更鼓舞人心的事情吗？<\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Host / 主持人<\/strong><\/p><p><img src=\"http://portrait.s.postenglishtime.com/5d624bcbdcab7f1176de3008.jpg?imageView2/2/w/212\"><\/p><p><strong>Chenchen&nbsp;/&nbsp;咨询师<\/strong><\/p><p>Chenchen, born and raised in Chengdu until the end of high school, relocated to Canada and U.S for the next 13 years of her life where she not only explored academically amongst 3 schools, 4 majors, but also career wise where experienced 4 different jobs that exposed her to different industries and organization styles. To continue her exploring journey, Chenchen moved back to China 2 years ago and began her adventure within China, within her hometown Sichuan Chengdu; a place that seemed both familiar and strange to her. As much as she has continuously stretched her boundary and vision, she has also been looking for a place, a career path, and the right person to settle with. And she is constantly contemplating the question of balancing between the desire to see the world, explore her potential and true passion，and the need to settle and dive into one particular area to further develop and accumulate on.<\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Attention / 注意事项<\/strong><\/p><p><strong>People in insurance sales, MLM, direct selling and P2P... are banned from attending.<\/strong><\/p><p><strong>禁止保险销售（比如AIA），传销，直销，p2p等人员参加活动<\/strong><\/p><p>Please sign up in advance, or pay extra ¥30<\/p><p>请提前报名，空降需要额外支付30元场地费<\/p><p><strong>The event will be cancelled if the number of participants is less than 1/3 of the expected number<\/strong><\/p><p><strong>报名人数不足期望人数的1/3活动自动取消，请务必报名，谢谢<\/strong><\/p><p><strong>If you're unable to attend, please postpone the participation on the registration page. If refunded, deduct 10% registration fee.<\/strong><\/p><p><strong>因特殊情况无法参加，请自己在报名成功页面点击延期参加；如果退款，扣除10%报名费。<\/strong><\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Event / 活动信息<\/strong><\/p><p><strong>Time / 时间<\/strong><\/p><p>2021/03/10 Wed 19:00 - 21:00<\/p><p>2021/03/10 周三 19:00点 - 21:00点<\/p><p><strong>Number / 人数<\/strong><\/p><p>30 people around<\/p><p>30人左右<\/p><p><strong>Fee / 费用<\/strong><\/p><p>39¥, On site with 25¥ coupon<\/p><p>39元, 线下报名费，含25元代金券<\/p><p><strong>Address / 地址<\/strong><\/p><p>Younee Bar, F1-3, Ruichen Building, No.13 Nongzhan South Road<\/p><p>农展馆南路13号瑞辰国际一层 F1-3 友你餐吧<\/p><p><strong>Contact / 联系主办方<\/strong><\/p><p>Wechat/微信：cutepet2015<\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Past Events / 往期活动<\/strong><\/p><iframe class=\"ql-video\" frameborder=\"0\" allowfullscreen=\"true\" src=\"https://v.qq.com/txp/iframe/player.html?vid=i0715cpz8pz\" height=\"350px\" width=\"100%\"><\/iframe><p><a href=\"https://mp.weixin.qq.com/s/Mst3Kkr8JvX_h9X75X407Q\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">你相信有灵魂伴侣吗<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/oiGzVwgbHknmz2i2Gc62_w\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">What is loneliness? | 孤独是什么？<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/RTba2pON21bW4312mCtqFQ\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">美食与吃货双语沙龙精彩回顾 | Food &amp; Foodie<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/_UbhjOkXLmEQMAbnNaq7mQ\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">金风玉露，却抵不过媒妁之言 | My parents disapprove of my relationship<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/0XOmJ86We3paxKK6Y1MYvw\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">感恩节沙龙精彩回顾 | Thanksgiving's Day English Salon<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/mjHR4WK0to2B0ws7fHXoLA\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">亚洲的赛车世界，10月25日英语沙龙总结<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/zB3Day236B5jYpQTaTb4RQ\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">对于二战的思考，10月18日英语沙龙总结<\/a><\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">PET Bilingual Salon / 双语沙龙<\/strong><\/p><p class=\"ql-align-center\"><img src=\"http://show.s.postenglishtime.com/8436212952794.jpg?imageView2/2/w/650/interlace/1/q/90\"><\/p><p>The PET Bilingual Salon is a decentralized sharing event where people meets in a relaxed and cozy place every week to exchange ideas on a topic prepared by friends. Participants will have a comprehensive discussion on the topic in different forms and depths, so that they can not only experience the fun of chatting, but also learn other people's thoughts, try to think from different perspectives, improve communication skills, broaden horizons and build their social network.<\/p><p>PET双语沙龙是一个去中心化的分享活动，每周大家在一个轻松舒适的地方聚会，以朋友准备的一个话题交流想法。大家会通过不同的形式和深度对这个话题进行全面讨论，参加的朋友们不仅可以从中体验到聊天的乐趣，还可以了解别人的想法，尝试从不同的角度思考问题，提高沟通能力，开阔视野，构建自己的人脉网络。<\/p><p><img src=\"http://images.s.postenglishtime.com/btmLogo.jpg\"><\/p><p><strong>PostEnglishTime<\/strong>, Speak English and Make Friends<\/p><p><strong>PET后英语时代|聊英文，交朋友就在PET<\/strong><\/p><p>PostEnglishTime is a high-quality network of English-speaking Chinese professionals in Beijing. We organize various social activities and provide various community services. Our mission is to build a high-quality network of locals and foreigners who are seriously interested in engaging with culture exchange, learning different societies and humanities.<\/p><p>PET创立于2011年，是一个集语言学习，社交与知识分享的英文爱好者社区。我们致力于成为国内外朋友都信赖的高质量社区，并为广大外语爱好者提供线上线下不同类型的语言交流活动与不同主题的分享与社交活动，及其他社区服务。在这里有趣的灵魂将自由连接，平凡的生命也一样精彩。<\/p><p><br><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/03d54fd6-6392-4117-816f-b1fc9fa5cedc.jpg","categoryId":6,"recommend":"N","signupNum":3605,"likeNum":6665,"status":"1","publishTime":"2021-05-08 14:06:09","categoryName":"学习"}
     */

    private String msg;
    private int code;
    private DataBean data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * searchValue : null
         * createBy : admin
         * createTime : 2021-05-08 14:06:09
         * updateBy : 1112338
         * updateTime : 2022-11-12 21:58:21
         * remark : null
         * params : {}
         * id : 50
         * name : 你想在20/30多岁的时候安定下来，拥有理想的工作，有合适的伴侣，有美好的生活方式吗?
         * content : <p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Topic / 话题</strong></p><p>We as human, naturally born with limited time and energy, are always facing the dilemma of exploring different choices and possibilities, and developing and accumulating in one vertical area. As much as the time, energy and effort required to achieve anything in any particular field, to cultivate any meaningful and long-standing relationship with any partner, and to develop your own friend circle for you to feel home and connected in any city, would you be willing to give up the temptation of other possibilities that are potentially more exciting and interesting, more fulfilling and rewarding, more naturing and inspiring?</p><p>我们人类生来就只有有限的时间和精力，总是面临着探索不同的选择和可能性，在一个垂直领域发展和积累的困境。所需的时间、精力和努力实现在任何特定领域，培养与不同合作伙伴有意义的和长期的关系，并开发自己的朋友圈在任何城市建立家的感觉。你愿意为了这些，放弃其他可能的诱惑，可能更令人兴奋并有趣，更有意义和更多的回报，更自然的和更鼓舞人心的事情吗？</p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Host / 主持人</strong></p><p><img src="http://portrait.s.postenglishtime.com/5d624bcbdcab7f1176de3008.jpg?imageView2/2/w/212"></p><p><strong>Chenchen&nbsp;/&nbsp;咨询师</strong></p><p>Chenchen, born and raised in Chengdu until the end of high school, relocated to Canada and U.S for the next 13 years of her life where she not only explored academically amongst 3 schools, 4 majors, but also career wise where experienced 4 different jobs that exposed her to different industries and organization styles. To continue her exploring journey, Chenchen moved back to China 2 years ago and began her adventure within China, within her hometown Sichuan Chengdu; a place that seemed both familiar and strange to her. As much as she has continuously stretched her boundary and vision, she has also been looking for a place, a career path, and the right person to settle with. And she is constantly contemplating the question of balancing between the desire to see the world, explore her potential and true passion，and the need to settle and dive into one particular area to further develop and accumulate on.</p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Attention / 注意事项</strong></p><p><strong>People in insurance sales, MLM, direct selling and P2P... are banned from attending.</strong></p><p><strong>禁止保险销售（比如AIA），传销，直销，p2p等人员参加活动</strong></p><p>Please sign up in advance, or pay extra ¥30</p><p>请提前报名，空降需要额外支付30元场地费</p><p><strong>The event will be cancelled if the number of participants is less than 1/3 of the expected number</strong></p><p><strong>报名人数不足期望人数的1/3活动自动取消，请务必报名，谢谢</strong></p><p><strong>If you're unable to attend, please postpone the participation on the registration page. If refunded, deduct 10% registration fee.</strong></p><p><strong>因特殊情况无法参加，请自己在报名成功页面点击延期参加；如果退款，扣除10%报名费。</strong></p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Event / 活动信息</strong></p><p><strong>Time / 时间</strong></p><p>2021/03/10 Wed 19:00 - 21:00</p><p>2021/03/10 周三 19:00点 - 21:00点</p><p><strong>Number / 人数</strong></p><p>30 people around</p><p>30人左右</p><p><strong>Fee / 费用</strong></p><p>39¥, On site with 25¥ coupon</p><p>39元, 线下报名费，含25元代金券</p><p><strong>Address / 地址</strong></p><p>Younee Bar, F1-3, Ruichen Building, No.13 Nongzhan South Road</p><p>农展馆南路13号瑞辰国际一层 F1-3 友你餐吧</p><p><strong>Contact / 联系主办方</strong></p><p>Wechat/微信：cutepet2015</p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Past Events / 往期活动</strong></p><iframe class="ql-video" frameborder="0" allowfullscreen="true" src="https://v.qq.com/txp/iframe/player.html?vid=i0715cpz8pz" height="350px" width="100%"></iframe><p><a href="https://mp.weixin.qq.com/s/Mst3Kkr8JvX_h9X75X407Q" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">你相信有灵魂伴侣吗</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/oiGzVwgbHknmz2i2Gc62_w" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">What is loneliness? | 孤独是什么？</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/RTba2pON21bW4312mCtqFQ" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">美食与吃货双语沙龙精彩回顾 | Food &amp; Foodie</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/_UbhjOkXLmEQMAbnNaq7mQ" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">金风玉露，却抵不过媒妁之言 | My parents disapprove of my relationship</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/0XOmJ86We3paxKK6Y1MYvw" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">感恩节沙龙精彩回顾 | Thanksgiving's Day English Salon</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/mjHR4WK0to2B0ws7fHXoLA" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">亚洲的赛车世界，10月25日英语沙龙总结</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/zB3Day236B5jYpQTaTb4RQ" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">对于二战的思考，10月18日英语沙龙总结</a></p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">PET Bilingual Salon / 双语沙龙</strong></p><p class="ql-align-center"><img src="http://show.s.postenglishtime.com/8436212952794.jpg?imageView2/2/w/650/interlace/1/q/90"></p><p>The PET Bilingual Salon is a decentralized sharing event where people meets in a relaxed and cozy place every week to exchange ideas on a topic prepared by friends. Participants will have a comprehensive discussion on the topic in different forms and depths, so that they can not only experience the fun of chatting, but also learn other people's thoughts, try to think from different perspectives, improve communication skills, broaden horizons and build their social network.</p><p>PET双语沙龙是一个去中心化的分享活动，每周大家在一个轻松舒适的地方聚会，以朋友准备的一个话题交流想法。大家会通过不同的形式和深度对这个话题进行全面讨论，参加的朋友们不仅可以从中体验到聊天的乐趣，还可以了解别人的想法，尝试从不同的角度思考问题，提高沟通能力，开阔视野，构建自己的人脉网络。</p><p><img src="http://images.s.postenglishtime.com/btmLogo.jpg"></p><p><strong>PostEnglishTime</strong>, Speak English and Make Friends</p><p><strong>PET后英语时代|聊英文，交朋友就在PET</strong></p><p>PostEnglishTime is a high-quality network of English-speaking Chinese professionals in Beijing. We organize various social activities and provide various community services. Our mission is to build a high-quality network of locals and foreigners who are seriously interested in engaging with culture exchange, learning different societies and humanities.</p><p>PET创立于2011年，是一个集语言学习，社交与知识分享的英文爱好者社区。我们致力于成为国内外朋友都信赖的高质量社区，并为广大外语爱好者提供线上线下不同类型的语言交流活动与不同主题的分享与社交活动，及其他社区服务。在这里有趣的灵魂将自由连接，平凡的生命也一样精彩。</p><p><br></p>
         * imgUrl : /prod-api/profile/upload/image/2021/05/08/03d54fd6-6392-4117-816f-b1fc9fa5cedc.jpg
         * categoryId : 6
         * recommend : N
         * signupNum : 3605
         * likeNum : 6665
         * status : 1
         * publishTime : 2021-05-08 14:06:09
         * categoryName : 学习
         */

        private Object searchValue;
        private String createBy;
        private String createTime;
        private String updateBy;
        private String updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String name;
        private String content;
        private String imgUrl;
        private int categoryId;
        private String recommend;
        private int signupNum;
        private int likeNum;
        private String status;
        private String publishTime;
        private String categoryName;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getRecommend() {
            return recommend;
        }

        public void setRecommend(String recommend) {
            this.recommend = recommend;
        }

        public int getSignupNum() {
            return signupNum;
        }

        public void setSignupNum(int signupNum) {
            this.signupNum = signupNum;
        }

        public int getLikeNum() {
            return likeNum;
        }

        public void setLikeNum(int likeNum) {
            this.likeNum = likeNum;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPublishTime() {
            return publishTime;
        }

        public void setPublishTime(String publishTime) {
            this.publishTime = publishTime;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public static class ParamsBean {
        }
    }
}
