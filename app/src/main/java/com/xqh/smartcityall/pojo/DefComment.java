package com.xqh.smartcityall.pojo;

import java.util.List;

public class DefComment {
    /**
     * msg : 操作成功
     * code : 200
     * data : [{"searchValue":null,"createBy":null,"createTime":"2022-11-10 10:40:07","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":449,"userId":1111259,"libraryId":10,"content":"12","myLikeState":false,"userName":"Lisa","likeCount":0},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 10:39:26","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":448,"userId":1111259,"libraryId":10,"content":"123","myLikeState":false,"userName":"Lisa","likeCount":0},{"searchValue":null,"createBy":null,"createTime":"2022-11-09 10:10:45","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":443,"userId":1112241,"libraryId":10,"content":"12456","myLikeState":false,"userName":"e","likeCount":0},{"searchValue":null,"createBy":null,"createTime":"2022-11-08 14:54:09","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":442,"userId":1112241,"libraryId":10,"content":"评论测试330","myLikeState":false,"userName":"e","likeCount":1},{"searchValue":null,"createBy":null,"createTime":"2022-11-08 14:25:03","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":441,"userId":1111250,"libraryId":10,"content":"11212","myLikeState":false,"userName":"blaze01","likeCount":1},{"searchValue":null,"createBy":null,"createTime":"2022-11-08 14:23:26","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":440,"userId":1111250,"libraryId":10,"content":"11212","myLikeState":false,"userName":"blaze01","likeCount":2},{"searchValue":null,"createBy":null,"createTime":"2022-11-07 22:24:43","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":436,"userId":1112900,"libraryId":10,"content":"我在郑州","myLikeState":false,"userName":"。","likeCount":1},{"searchValue":null,"createBy":null,"createTime":"2022-11-07 00:08:55","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":433,"userId":1112675,"libraryId":10,"content":"还有人在学吗?","myLikeState":false,"userName":"yjj1234","likeCount":3},{"searchValue":null,"createBy":null,"createTime":"2022-11-06 18:08:45","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":431,"userId":1111671,"libraryId":10,"content":"6","myLikeState":false,"userName":"cuixishuai","likeCount":2},{"searchValue":null,"createBy":null,"createTime":"2022-11-04 21:20:15","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":417,"userId":1112675,"libraryId":10,"content":"学生妹全国空降+v19526340066","myLikeState":false,"userName":"yjj1234","likeCount":2}]
     */

    private String msg;
    private int code;
    private List<DataBean> data;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-11-10 10:40:07
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 449
         * userId : 1111259
         * libraryId : 10
         * content : 12
         * myLikeState : false
         * userName : Lisa
         * likeCount : 0
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int userId;
        private int libraryId;
        private String content;
        private boolean myLikeState;
        private String userName;
        private int likeCount;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getLibraryId() {
            return libraryId;
        }

        public void setLibraryId(int libraryId) {
            this.libraryId = libraryId;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public boolean isMyLikeState() {
            return myLikeState;
        }

        public void setMyLikeState(boolean myLikeState) {
            this.myLikeState = myLikeState;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public int getLikeCount() {
            return likeCount;
        }

        public void setLikeCount(int likeCount) {
            this.likeCount = likeCount;
        }

        public static class ParamsBean {
        }
    }
}
