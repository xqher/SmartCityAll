package com.xqh.smartcityall.pojo;

public class ItemList {
    private String title;
    private String content;
    private String cover;
    private String targetId;
    private String time;
    private String status;
    private int coverId;
    private boolean cur;

    public boolean isCur() {
        return cur;
    }

    public void setCur(boolean cur) {
        this.cur = cur;
    }

    public ItemList() {
    }

    public ItemList(String title, String content, String cover, String targetId, String time, String status) {
        this.title = title;
        this.content = content;
        this.cover = cover;
        this.targetId = targetId;
        this.time = time;
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getTargetId() {
        return targetId;
    }

    public void setTargetId(String targetId) {
        this.targetId = targetId;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCoverId() {
        return coverId;
    }

    public void setCoverId(int coverId) {
        this.coverId = coverId;
    }
}
