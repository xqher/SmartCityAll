package com.xqh.smartcityall.pojo;

import android.graphics.drawable.Icon;

public class IconBean {
    private String iconName;
    private String iconImg;
    private int iconRes;
    private String iconId;
    private int iconSort;

    public IconBean(){}

    public IconBean(String name, int res) {
        setIconName(name);
        setIconRes(res);
    }


    public IconBean(String name, String img) {
        setIconName(name);
        setIconImg(img);
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public String getIconImg() {
        return iconImg;
    }

    public void setIconImg(String iconImg) {
        this.iconImg = iconImg;
    }

    public int getIconRes() {
        return iconRes;
    }

    public void setIconRes(int iconRes) {
        this.iconRes = iconRes;
    }

    public String getIconId() {
        return iconId;
    }

    public void setIconId(String iconId) {
        this.iconId = iconId;
    }

    public int getIconSort() {
        return iconSort;
    }

    public void setIconSort(int iconSort) {
        this.iconSort = iconSort;
    }
}
