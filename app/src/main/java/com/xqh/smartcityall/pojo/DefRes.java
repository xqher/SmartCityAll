package com.xqh.smartcityall.pojo;

public class DefRes {
    /**
     * msg : 操作成功
     * code : 200
     * token : eyJhbGciOiJIUzUxMiJ9.eyJsb2dpbl91c2VyX2tleSI6IjY0MGIyM2M0LTE0NWQtNDUzZS1iNDU1LTE5ODI1NDE2MjhmNyJ9.R0W10csofUztWWTvOmjuj6BGO8DS8LlbGN1FYnJHdLfq5GwtrAHaneE7_D9y19jpEw4go7PlCJOlLHwl7o8nWQ
     */

    private String msg;
    private int code;
    private String token;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
