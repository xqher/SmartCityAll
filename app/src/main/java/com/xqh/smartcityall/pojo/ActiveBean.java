package com.xqh.smartcityall.pojo;

import java.util.List;

public class ActiveBean {
    /**
     * total : 48
     * rows : [{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 14:06:09","updateBy":"1112172","updateTime":"2022-11-12 16:42:55","remark":null,"params":{},"id":50,"name":"你想在20/30多岁的时候安定下来，拥有理想的工作，有合适的伴侣，有美好的生活方式吗?","content":"<p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Topic / 话题<\/strong><\/p><p>We as human, naturally born with limited time and energy, are always facing the dilemma of exploring different choices and possibilities, and developing and accumulating in one vertical area. As much as the time, energy and effort required to achieve anything in any particular field, to cultivate any meaningful and long-standing relationship with any partner, and to develop your own friend circle for you to feel home and connected in any city, would you be willing to give up the temptation of other possibilities that are potentially more exciting and interesting, more fulfilling and rewarding, more naturing and inspiring?<\/p><p>我们人类生来就只有有限的时间和精力，总是面临着探索不同的选择和可能性，在一个垂直领域发展和积累的困境。所需的时间、精力和努力实现在任何特定领域，培养与不同合作伙伴有意义的和长期的关系，并开发自己的朋友圈在任何城市建立家的感觉。你愿意为了这些，放弃其他可能的诱惑，可能更令人兴奋并有趣，更有意义和更多的回报，更自然的和更鼓舞人心的事情吗？<\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Host / 主持人<\/strong><\/p><p><img src=\"http://portrait.s.postenglishtime.com/5d624bcbdcab7f1176de3008.jpg?imageView2/2/w/212\"><\/p><p><strong>Chenchen&nbsp;/&nbsp;咨询师<\/strong><\/p><p>Chenchen, born and raised in Chengdu until the end of high school, relocated to Canada and U.S for the next 13 years of her life where she not only explored academically amongst 3 schools, 4 majors, but also career wise where experienced 4 different jobs that exposed her to different industries and organization styles. To continue her exploring journey, Chenchen moved back to China 2 years ago and began her adventure within China, within her hometown Sichuan Chengdu; a place that seemed both familiar and strange to her. As much as she has continuously stretched her boundary and vision, she has also been looking for a place, a career path, and the right person to settle with. And she is constantly contemplating the question of balancing between the desire to see the world, explore her potential and true passion，and the need to settle and dive into one particular area to further develop and accumulate on.<\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Attention / 注意事项<\/strong><\/p><p><strong>People in insurance sales, MLM, direct selling and P2P... are banned from attending.<\/strong><\/p><p><strong>禁止保险销售（比如AIA），传销，直销，p2p等人员参加活动<\/strong><\/p><p>Please sign up in advance, or pay extra ¥30<\/p><p>请提前报名，空降需要额外支付30元场地费<\/p><p><strong>The event will be cancelled if the number of participants is less than 1/3 of the expected number<\/strong><\/p><p><strong>报名人数不足期望人数的1/3活动自动取消，请务必报名，谢谢<\/strong><\/p><p><strong>If you're unable to attend, please postpone the participation on the registration page. If refunded, deduct 10% registration fee.<\/strong><\/p><p><strong>因特殊情况无法参加，请自己在报名成功页面点击延期参加；如果退款，扣除10%报名费。<\/strong><\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Event / 活动信息<\/strong><\/p><p><strong>Time / 时间<\/strong><\/p><p>2021/03/10 Wed 19:00 - 21:00<\/p><p>2021/03/10 周三 19:00点 - 21:00点<\/p><p><strong>Number / 人数<\/strong><\/p><p>30 people around<\/p><p>30人左右<\/p><p><strong>Fee / 费用<\/strong><\/p><p>39¥, On site with 25¥ coupon<\/p><p>39元, 线下报名费，含25元代金券<\/p><p><strong>Address / 地址<\/strong><\/p><p>Younee Bar, F1-3, Ruichen Building, No.13 Nongzhan South Road<\/p><p>农展馆南路13号瑞辰国际一层 F1-3 友你餐吧<\/p><p><strong>Contact / 联系主办方<\/strong><\/p><p>Wechat/微信：cutepet2015<\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">Past Events / 往期活动<\/strong><\/p><iframe class=\"ql-video\" frameborder=\"0\" allowfullscreen=\"true\" src=\"https://v.qq.com/txp/iframe/player.html?vid=i0715cpz8pz\" height=\"350px\" width=\"100%\"><\/iframe><p><a href=\"https://mp.weixin.qq.com/s/Mst3Kkr8JvX_h9X75X407Q\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">你相信有灵魂伴侣吗<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/oiGzVwgbHknmz2i2Gc62_w\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">What is loneliness? | 孤独是什么？<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/RTba2pON21bW4312mCtqFQ\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">美食与吃货双语沙龙精彩回顾 | Food &amp; Foodie<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/_UbhjOkXLmEQMAbnNaq7mQ\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">金风玉露，却抵不过媒妁之言 | My parents disapprove of my relationship<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/0XOmJ86We3paxKK6Y1MYvw\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">感恩节沙龙精彩回顾 | Thanksgiving's Day English Salon<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/mjHR4WK0to2B0ws7fHXoLA\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">亚洲的赛车世界，10月25日英语沙龙总结<\/a><\/p><p class=\"ql-align-center\"><a href=\"https://mp.weixin.qq.com/s/zB3Day236B5jYpQTaTb4RQ\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: inherit; background-color: transparent;\">对于二战的思考，10月18日英语沙龙总结<\/a><\/p><p><strong style=\"color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);\">PET Bilingual Salon / 双语沙龙<\/strong><\/p><p class=\"ql-align-center\"><img src=\"http://show.s.postenglishtime.com/8436212952794.jpg?imageView2/2/w/650/interlace/1/q/90\"><\/p><p>The PET Bilingual Salon is a decentralized sharing event where people meets in a relaxed and cozy place every week to exchange ideas on a topic prepared by friends. Participants will have a comprehensive discussion on the topic in different forms and depths, so that they can not only experience the fun of chatting, but also learn other people's thoughts, try to think from different perspectives, improve communication skills, broaden horizons and build their social network.<\/p><p>PET双语沙龙是一个去中心化的分享活动，每周大家在一个轻松舒适的地方聚会，以朋友准备的一个话题交流想法。大家会通过不同的形式和深度对这个话题进行全面讨论，参加的朋友们不仅可以从中体验到聊天的乐趣，还可以了解别人的想法，尝试从不同的角度思考问题，提高沟通能力，开阔视野，构建自己的人脉网络。<\/p><p><img src=\"http://images.s.postenglishtime.com/btmLogo.jpg\"><\/p><p><strong>PostEnglishTime<\/strong>, Speak English and Make Friends<\/p><p><strong>PET后英语时代|聊英文，交朋友就在PET<\/strong><\/p><p>PostEnglishTime is a high-quality network of English-speaking Chinese professionals in Beijing. We organize various social activities and provide various community services. Our mission is to build a high-quality network of locals and foreigners who are seriously interested in engaging with culture exchange, learning different societies and humanities.<\/p><p>PET创立于2011年，是一个集语言学习，社交与知识分享的英文爱好者社区。我们致力于成为国内外朋友都信赖的高质量社区，并为广大外语爱好者提供线上线下不同类型的语言交流活动与不同主题的分享与社交活动，及其他社区服务。在这里有趣的灵魂将自由连接，平凡的生命也一样精彩。<\/p><p><br><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/03d54fd6-6392-4117-816f-b1fc9fa5cedc.jpg","categoryId":6,"recommend":"N","signupNum":3603,"likeNum":6665,"status":"1","publishTime":"2021-05-08 14:06:09","categoryName":"学习"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 14:04:06","updateBy":"1112338","updateTime":"2022-11-12 21:07:51","remark":null,"params":{},"id":49,"name":"首位时间唤醒师，新书热销双榜第1《掌控24小时》作者尹慕言领读会，唤醒你被吞噬的时间","content":"<p>\u200b<img src=\"http://cdn.huodongxing.com/file/ue/20201021/11A13E7AD0281760410B85C44E9FE7EB6E/30864089840360529.jpeg\" alt=\"掌控24小时-尹慕言-横版 .png\"><\/p><p>好书领读会是晚修课周二的读书分享沙龙。有感于大家通过支离破碎的读书解读来获取知识的方式，减弱了思考能力和创造能力的训练，邀请原作者进行领读和导读，通过面对面交流的方式，让大家可以了解作者真实的创作意图和写作对象。且可以和作者进行深度交流。真正地开启一段好书的阅读之旅。<\/p><p><br><\/p><p>旨在破除\u201c写作者赚不到钱，讲书的把钱赚走了\u201d的魔咒。<\/p><p><br><\/p><p>晚修课是一个晚间线下面对面社交主题沙龙。<\/p><p>时间为工作日晚间19:30-21:00。<\/p><p>地点：联合办公空间、书店、咖啡馆、公共文化空间、博物馆等。<\/p><p>人群：针对已入职场的\u201c成年人\u201d。有独立的自我意识，想要自主择业、创业、个人修行、自我认识的人群。<\/p><p>内容：主要涉及四个方面：创业和职业、知识和技能、公益和文化、美学和爱好。<\/p><p><br><\/p><p><strong>01<\/strong><\/p><p><strong>活动介绍<\/strong><\/p><p><strong>◎&nbsp;好书领读会<\/strong><\/p><p>是固定周二举办的读书会，邀请好书的原作者或编者来解读好书写作的要求、观点、和关于该书创作的背后故事。<\/p><p>期望鼓励和支持大家重拾亲自阅读书籍的欲望和行动。帮我开启一次新书之旅。<\/p><p>3月16日（星期二）是 #好书领读会# 第2期，邀请新书热销双榜第1《掌控24小时》作者尹慕言老师为大家导读。<\/p><p><br><\/p><p>时间管理图书<\/p><p>过好每一天，时间看得见<\/p><p>\u201c时间唤醒教练\u201d尹慕言首部作品<\/p><p><br><\/p><p><strong>◎ 内容简介<\/strong><\/p><p>☆时间管理的好坏，绝不仅仅在于是否掌握了最先进的时间管理方法，更重要的是有没有重塑内在的心智模式。而时间管理的本质，其实是打破自我认知遮蔽，唤醒时间，习得成长型思维，不断践行终身成长的理念。<\/p><p>\u200b<\/p><p>☆当然，我们要想真正走进自己的内在，重塑心智模式，就必须直面价值观的探索，甚至学会与自我、与智能、与时代对话和相处。本书正是以一天24小时的不同时段、不同场景为时间管理的切入口，阐述了在每个时间段所对应的场景中，如何破除认知遮蔽，建立正确观念，以获得成长与突破。事实上，一天24小时只是一个隐喻，书中所探寻的路径，是我们每个人一生的功课，需要我们日复一日去迭代与修炼。<\/p><p><br><\/p><p><strong>◎ 编辑推荐<\/strong><\/p><p>☆国内从教练视角讲时间管理的首部作品<\/p><p>时间管理并不是一个孤立的问题，它和个人成长关系密切，因为处于三维空间的我们，最终都要面对时间这把尺子。\u201c时间唤醒教练\u201d尹慕言在本书中提出\u201c时间唤醒\u201d的理念，结合人生教练的方法与技术，按照24小时的时间线，探讨了个体如何在不同的时间段内提升效率，获得最大效能与效用，同时开启个人终身成长之旅。<\/p><p>☆高琳、李海峰、秋叶、易仁永澄等多位大咖联合推荐，让你活出真实的自己<\/p><p>本书是一个巨大的隐喻，24小时象征着人的一生。在出发之前，甚至在路上，我们都绕不开对于人生重要课题的思考。而不同时段对应的，正是人生不同的阶段，它们又有不同的子课题摆在我们面前。所以，实际上这是一本教你如何活出自己的手册。翻开它，想想从哪里开始改变吧。<\/p><p><br><\/p><p><strong>02<\/strong><\/p><p><strong>时间地点<\/strong><\/p><p>沙龙时间：2021年3月16日 星期二 晚19：30 - 21：30<\/p><p>沙龙地点：中间态（ONESPACE）共享办公空间（东大桥站），百富国际大厦3层（地铁6号线东大桥站东北口（A口）出<\/p><p>面向人群：时间管理存在问题、自认为有拖延问题的人，想要更高效的利用时间，想提供创新力和行动力的职场人士。<\/p><p><br><\/p><p><strong>03<\/strong><\/p><p><strong>日程安排<\/strong><\/p><p>19：00-19：30&nbsp;&nbsp;活动签到<\/p><p>19：30-19：45&nbsp;个人90sPR<\/p><p>19：45-20：45&nbsp;作者领读好书<\/p><p>20：45-21：30&nbsp;互动交流<\/p><p><br><\/p><p><br><\/p><p><strong>04<\/strong><\/p><p><strong>嘉宾介绍<\/strong><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/20201021/11A13E7AD0281760410B85C44E9FE7EB6E/30914089784209189.jpg\" alt=\"尹慕言.jpg\"><\/p><p><br><\/p><p><strong>尹慕言<\/strong><\/p><p><br><\/p><p><strong>组织发展专家、个人成长教练<\/strong><\/p><p><strong>时间唤醒师<\/strong><\/p><p><br><\/p><p><br><\/p><p><span style=\"color: rgb(0, 0, 0);\">《掌控24小时，让你效率倍增的时间管理术》作者、《掌控力：让你的人生变得高效》合著作者。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">师从埃里克森国际学院院长玛丽莲·阿特金森博士、前美国国家航空航天局物理学家查理·佩勒林博士、正面管教体系创始人简·尼尔森博士等。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">首提\u201c时间唤醒\u201d理念，\u201c时间唤醒教练\u201d发起人，\u201c终身成长\u201d理念的践行者。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">组织发展专家、国际教练联合会认证教练、高管1V1教练、DISC / PDP组织与人才测评导师、美国NASA4-D领导力认证教练、团队赋能教练。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">混沌大学 创新助教、辽宁卫视某栏目特邀嘉宾、企业管理咨询顾问、人力资源管理咨询顾问。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">人力资源与组织发展方向从业10余年，目前仍在商战、职场打拼的\u201c好\u201d字妈妈。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">深度聚焦组织发展与个人成长领域，并在职场效能提升、职场领导力提升方面有深入研究与实践。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">致力于运用教练技术唤醒并推动个人、团队和组织持续成长与发展。<\/span><\/p><p><span style=\"color: rgb(0, 0, 0);\">个人公众号\u201c慕言TIME\u201d主理人，知乎、微博账户与豆瓣同名。<\/span><\/p><p><br><\/p><p>05温馨提示<\/p><p><br><\/p><p>1、本次活动<span style=\"color: rgb(0, 87, 80);\">单一统一票价 ￥99/位<\/span>。<\/p><p>2、报名的朋友们如果提供的email和手机号码无误，我们会在活动开始前一天发送短信和邮件确认。所以请确认自己填写的email地址无误。如果未收到确认函，可致电话：<span style=\"color: rgb(0, 87, 80);\">186-6909-6820<\/span><span style=\"color: rgb(192, 0, 0);\">&nbsp;<\/span>进行咨询。<\/p><p>3、本活动欢迎大家带自己的同事和朋友一起参加，因为席位有限，为了保证交流质量，请尽可能提前为他们报名，或致电<span style=\"color: rgb(0, 87, 80);\">186-6909-6820<\/span>。<\/p><p><br><\/p><p><strong>06<\/strong><\/p><p><strong>关于主办方<\/strong><\/p><p>晚修课是一个晚间线下面对面社交主题沙龙。<\/p><p>时间为工作日晚间19:30-21:00。<\/p><p>人群：针对已入职场的\u201c成年人\u201d。有独立的自我意识，想要自主择业、创业、个人修行、自我认识的人群。<\/p><p>内容：主要涉及四个方面：创业和职业、知识和技能、公益和文化、美学和爱好。<\/p><p><br><\/p><p>本次活动场地介绍：<\/p><p><br><\/p><p>中间态ONESPACE是亿达置业旗下的全新商业模块，是基于现代都市人工作生活更为多元化、边界模糊化的发展趋势、运用科技智能及人本设计理念研发、创造的新型共享办公生态产品。<\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/3729bcdc-9813-49b1-a922-a22cff996e85.jpg","categoryId":6,"recommend":"N","signupNum":471,"likeNum":665,"status":"1","publishTime":"2021-05-08 14:04:06","categoryName":"学习"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 14:01:44","updateBy":"1111883","updateTime":"2022-11-10 19:25:31","remark":null,"params":{},"id":48,"name":"大数据 || 手把手教你做数据分析项目","content":"<p><img src=\"http://cdn.huodongxing.com/file/ue/20180527/117E663143A9E5C4F993A35C3E4EA61CF1/30294129565093949.jpg\" alt=\"图怪兽_466452e1ca2b320e58470c4558709ed6_18962.jpg\"><\/p><p>数据分析师最重要的项目经验，在入行前或工作中了解企业真实项目过程中的重点与细节，了解企业对于数据分析师的真实需求，从项目开始学习，助力转行的小伙伴能更加顺利，助力已经工作的小伙伴在工作中有例可鉴。<\/p><p><br><\/p><p>报名参加<\/p><p>添加九九老师微信<\/p><p>获取参与方式<\/p><p><br><\/p><p><strong>01<\/strong><\/p><p><strong>温馨提示<\/strong><\/p><p>1、本次活动<strong style=\"color: rgb(0, 87, 80);\">免费<\/strong>。<\/p><p>2、报名的朋友们请确认填写的手机号与邮箱无误，便于我们联系。如果未收到确认，可致电话：<strong style=\"color: rgb(0, 87, 80);\">18106547650<\/strong><strong style=\"color: rgb(192, 0, 0);\">&nbsp;<\/strong>进行咨询<span style=\"color: rgb(0, 87, 80);\">【同微信号】<\/span><\/p><p>3、本活动欢迎大家介绍自己的同事和朋友一起参加，也提前为他们报名。<\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/e7346186-53db-46d9-ac5b-562cba41adce.jpg","categoryId":5,"recommend":"N","signupNum":697,"likeNum":455,"status":"1","publishTime":"2021-05-08 14:01:44","categoryName":"行业"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:59:44","updateBy":"1112172","updateTime":"2022-11-12 16:15:13","remark":null,"params":{},"id":47,"name":"《大数据+医疗卫生》线上免费录播课，了解在医疗行业的大数据","content":"<p>大数据的增长让人们做了-些不可思议的事情。采用大数据，机器学习可以预测财产损失和检测欺诈，甚至预测未来的天气事件。然而，这些算法最令人难以置信的应用可能是在医疗保健领域。<\/p><p><strong>拯救生命的预测<\/strong><\/p><p>通过提供另-一个视角，数据驱动的机器学习算法可以提供关键的二次反馈，并有助于早期检测。例如，中国的研究人员已经开始使用机器学习算法来预测患者从昏迷中醒来的可能性。在一些情况下，该算法正确地预测了医护人员不在时患者是否会醒来。尽管有时算法是错误的，医生的判断是正确的，但人工智能仍然足够准确，可以为临床诊断提供宝贵的第二种意见。<\/p><p>医疗健康的图像数据对于机器学习尤其有价值。例如，当由眼科医生进行评估时，深度学习算法通过学习数千张视网膜照片,对糖尿病性视网膜病变具有超过90%的特异性和敏感性得以判断分析。在皮肤病的研究中，当在输入130,000张患者图像之后，机器学习的一-种算法在识别皮肤癌方面和皮肤科医生一样准确。 虽然提供不受人为错误影响的第二种意见无疑是有帮助的，但这些算法正在提供医生已经提供的诊断服务。这带来了一个问题:机器学习是否可以在医生无法提供帮助的领域提供帮助?<\/p><p>下面这个课程通过对大数据在医疗业务背景的介绍，业务架构、技术.选型、数据采集、报文处理、预警报警等模块的讲解,使学员了解大数据如何与医疗卫生相结合。本课程为线上录播课，可添加微信或者直接报名，助教老师会把课程链接和账号密码以短信的内容发送到您的手机或者微信，注意查收哟！<\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/259f19fe-03ba-4cc9-ba51-65cd54d0eb00.jpg","categoryId":5,"recommend":"Y","signupNum":493,"likeNum":756,"status":"1","publishTime":"2021-05-08 13:59:44","categoryName":"行业"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:57:47","updateBy":"1113292","updateTime":"2022-11-12 16:01:34","remark":null,"params":{},"id":46,"name":"副业赚钱法，零成本，易操作，打开运营赚钱思路短视频课程","content":"<p class=\"ql-align-center\"><img src=\"http://cdn.huodongxing.com/file/ue/20180912/119BA6637067B5B418EDDB9425897A2F5C/30833660605196499.jpg\" alt=\"副业.jpg\"><\/p><p><br><\/p><p><br><\/p><p>当你只有一份收入时，意味着你会面临如下问题。<\/p><p><br><\/p><p>（1）你会担心工作安全问题：有层出不穷的新人比你年轻，能力比你强，体力比你好，工资要求比你低；或者目前能力不如你，但是每天比你能多花4个小时在公司，反正单身，回家也没事情。<\/p><p><br><\/p><p>（2）你有职业的天花板：大家都想升职，因为要想加薪，必须升职。但是，在一个金字塔型的职场结构中，任何时候能升到上一级的都是少数。<\/p><p><br><\/p><p>（3）你缺少自由的时间：\u201c从世界那么大，我想出去转转\u201d曾引爆朋友圈，据此就能看出职场的大多数人经常做一个白日梦，叫\u201c想来一场说走就走的旅行\u201d。<\/p><p><br><\/p><p>（4）你担心年龄问题：为公司拼搏，一不小心奔四了，当朋友圈谈论着\u201c单位里那些超过40岁的人去哪里了\u201d的时候，你难道真的不焦虑吗？<\/p><p><br><\/p><p>（5）你惧怕通货膨胀：收入增长能赶上通货膨胀的人不多吧？<\/p><p><br><\/p><p>（6）你烦恼于无限多的开支和唯一收入之间的反差：开始养自己，后来养老婆，再后来养孩子，同时还得养老人。<\/p><p><br><\/p><p>开源节流从何谈起呢？<\/p><p><br><\/p><p>哪个你能下得了手去节流呢？<\/p><p><br><\/p><p>这就是只有一份收入时必然会碰到的问题，还没有认真和你谈谈有关你儿时梦想和兴趣爱好的问题，罢了。<\/p><p><br><\/p><p>所以，当你只有一份收入时，无论这份收入在社会平均水平之上有多少（前提假设你不是极个别的\u201c打工皇帝\u201d），你都会感受到无限的需求与欲望与有限的收入之间的巨大矛盾，而且这个矛盾会与日俱增。<\/p><p><br><\/p><p>怎么办？你需要多项收入，你需要开拓自己的赚钱思路，特别是副业思路。<\/p><p><br><\/p><p><br><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/4d8b6ecf-76b6-46e5-b94c-0e43021c1d75.jpg","categoryId":4,"recommend":"N","signupNum":468,"likeNum":6665,"status":"1","publishTime":"2021-05-08 13:57:47","categoryName":"校园"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:56:30","updateBy":"1112172","updateTime":"2022-11-07 16:15:06","remark":null,"params":{},"id":45,"name":"《财务管理MBA课程》职场人都必须学的财务管理能力(如何掘金财务数据，善用经济工具)","content":"<p><span style=\"color: rgb(51, 51, 51);\"><img src=\"http://cdn.huodongxing.com/file/ue/20190902/1132866F5447252998C768E0BE8EA65E25/30633755660438909.jpeg\" alt=\"《财经MBA》职场人都必须学的财务管理能力-详情页.png\"><\/span><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/3a552d5f-301d-40b1-8df3-9cc17316e198.jpg","categoryId":4,"recommend":"N","signupNum":3461,"likeNum":6788,"status":"1","publishTime":"2021-05-08 13:56:30","categoryName":"校园"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:55:20","updateBy":"1111397","updateTime":"2022-11-08 15:39:37","remark":null,"params":{},"id":44,"name":"《非暴力沟通》线下基础课程（连续6周）","content":"<p>作者介绍<\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/913915540364683.jpeg\"><\/p><p><br><\/p><p><br><\/p><p>马歇尔·卢森堡博士由于在促进人类和谐共处方面的突出成就，2006年他获得了地球村基金会颁发的和平之桥奖。<\/p><p><br><\/p><p>卢森堡博士早年师从心理学大师卡尔·罗杰斯，后来他发展出极具启发性和影响力的非暴力沟通的原则和方法，不仅教会人们如何使个人生活更加和谐美好，同时解决了众多世界范围内的冲突和争端。<\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><strong>&nbsp;什么是非暴力沟通？<\/strong><\/p><p>也被称作\u201c爱的语言\u201d，一种沟通方式，依照它谈话和聆听，不再条件反射一般粗暴地对待他人和自己的感受和愿望，重塑我们对冲突的积极思维方式，打开爱和理解，增进人与人之间的联接，使人们乐于互助。<\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/433915540364684.jpeg\"><\/p><p><br><\/p><p>\u200b<\/p><p><strong style=\"color: rgb(158, 114, 84);\">&nbsp;《非暴力沟通》读书会的价值<\/strong><\/p><p><span style=\"color: rgb(127, 127, 127);\">非暴力沟通不是头脑层面的道理，而是生命状态，因而需要透过体验，在任何时候阅读马歇尔的《非暴力沟通》的重要性是毋庸置疑的，可以说这本书囊括了非暴力沟通的框架、方法和精神，角角落落里都是知识点。正是因为此书的密度很高，用读书会的方式来学习非暴力沟通是一个绝佳的开启！这个过程你将收获对《非暴力沟通》这本书的深刻理解，将为更深入的系统学习非暴力沟通奠定扎实基础！<\/span><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p>■&nbsp;引领人介绍<\/p><p><br><\/p><p><br><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/133915540364686.jpeg\"><\/p><p class=\"ql-align-center\"><strong>王晓雷<\/strong><\/p><p class=\"ql-align-center\">●灵犀文化创始人；<\/p><p class=\"ql-align-center\">●世界银行IFC认证培训师；<\/p><p class=\"ql-align-center\">●非暴力沟通\u201c生命的语言\u201d讲师<\/p><p class=\"ql-align-center\">●国际非暴力沟通认证培训师候选人（审核中）<\/p><p class=\"ql-align-center\">●樊登讲师大赛全国亚军&amp;全场冠军<\/p><p><br><\/p><p>师从国际同理心教练（EC）创始人加拿大佛朗索瓦（Francois）和中国目前唯一非暴力沟通国际认证培训师（CNVC）刘轶；<\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><strong>★培训经历：<\/strong><\/p><ul><li>20年讲师生涯积累，几百场授课经历，指导数万名学生；<\/li><li>学习和传播非暴力沟通4年，<\/li><li>师从<span style=\"color: rgb(151, 72, 6);\">国际认证培训师刘轶；<\/span>国际同理心教练（EC）创始人加拿大弗朗索瓦（Francois）<span style=\"color: rgb(151, 72, 6);\">，<\/span><\/li><li><span style=\"color: rgb(151, 72, 6);\">浸泡工作坊学习非暴力沟通400以上课时，带领非暴力沟通沙龙、成长小组200+场次（600以上课时），致力于非暴力沟通在亲子关系、亲密关系、生活各关系上的探索和应用。<\/span><\/li><li>先后培训过通用电气、阿尔卡特、罗可威尔自动化、诺信、安利、复旦MBA、中国银行、百安居、百脑汇、上海海克、上海阳森等50多家中外知名企业，在<span style=\"color: rgb(12, 12, 12);\">个人心智模式成长、组织情商、团队心态、领导力<\/span>方面具备非常丰富的授课经验。<\/li><\/ul><p>\u200b<\/p><p>■&nbsp;课程介绍&nbsp;<\/p><p><br><\/p><p>参与式，导入式教学；<\/p><p>读书会将开启你学习《非暴力沟通》的大门，并且为在这条道路上持续学习和实践打下坚实基础。<\/p><p>相信会带给你更有力量的体验，并且为你日后深入地在非暴力沟通各个领域中的应用（包括：讲师、教练、冲突调解、引导培训、教育、领导力等）打下扎实的基础。<\/p><p>■&nbsp;课程费用及安排&nbsp;<\/p><p><br><\/p><p>课程时间：每周六&nbsp;13：00-15：30 连续6周<\/p><p><br><\/p><p>课程费用：20元/1次&nbsp;<\/p><p>10人制小班<\/p><p><br><\/p><p>注：<\/p><p>请自备《非暴力沟通》书一本<\/p><p><br><\/p><p><br><\/p><p>往期活动<\/p><p><br><\/p><p><br><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/823915540364687.jpeg\"><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/183915540364688.jpeg\"><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/703915540364689.jpeg\"><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/843915540364690.jpeg\"><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/crawl/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/583915540364691.jpeg\"><\/p><p>\u200b<\/p><p><br><\/p><p><br><\/p><p><strong>灵犀文化北京地区合作招募开放中<\/strong><\/p><p>&gt;&gt;合作对象&lt;&lt;<\/p><p>有意开展非暴力沟通主题线下工作坊、讲座或线上课程的任何教育机构、企业、组织、个人工作室等。<\/p><p>有意建设\u201c非暴力沟通\u201d北京本地的学习与实践社群。<\/p><p><br><\/p><p>联络人：小雷<\/p><p>微信：wangxiaolei6788<\/p><p>联系电话：15901186788<\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><br><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/20190328/11C6AD663DA98BA6CDC10A1ED4EB75DE74/30164020684472434.jpeg\" alt=\"WPS图片修正.png\"><\/p><p class=\"ql-align-center\"><strong>扫二维码｜关注我们<\/strong><\/p><p><br><\/p><p>非暴力沟通｜北京社群<\/p><p><br><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/57a01005-0478-49a3-b550-3eb4a306e866.jpg","categoryId":4,"recommend":"N","signupNum":348,"likeNum":566,"status":"1","publishTime":"2021-05-08 13:55:20","categoryName":"校园"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:53:19","updateBy":"1112172","updateTime":"2022-11-07 16:15:00","remark":null,"params":{},"id":43,"name":"股权激励课程（1天）","content":"<p>\u200b关于股权激励的知识涵盖非常全面，正常我们在北京、上海、深圳、杭州等城市线下班次两天要4980元，迎接5月课程上新，现福利来袭一波～～～<\/p><p>1）现在招募100名线下学员，学费只需399元，满50人事择最近周六日中一天开课。地点北京。可以线上课程的价格享受线下的待遇，非常超值，如果您在企业股权激励中有难以解决的问题还可现场咨询老师。<\/p><p>2）还可以在此预订线上课程，<span style=\"color: rgb(255, 0, 0);\">可享8折优惠<\/span><\/p><p><br><\/p><p><img src=\"http://cdn.huodongxing.com/file/ue/20150920/11A4E1E6906DC85BC433D331B384382BC6/30174113323881299.jpg\" alt=\"课程三介绍.jpg\"><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/f4abb564-dc67-4c11-999c-0ab864335d06.jpg","categoryId":4,"recommend":"N","signupNum":117,"likeNum":2366,"status":"1","publishTime":"2021-05-08 13:53:19","categoryName":"校园"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:51:04","updateBy":"1111246","updateTime":"2022-11-08 15:51:35","remark":null,"params":{},"id":42,"name":"【父亲节】邀请你参加来一场给孩子的TED","content":"<p class=\"ql-align-center\">未来的世界<\/p><p class=\"ql-align-center\">我们的孩子<\/p><p class=\"ql-align-center\">不仅需要爱逻辑，爱分析，爱推理<\/p><p class=\"ql-align-center\">还需要从自己的兴趣出发<\/p><p class=\"ql-align-center\">能共情，会审美，懂表达<\/p><p class=\"ql-align-center\">全面融合的发展和成长<\/p><p class=\"ql-align-center\">您可能带孩子逛博物馆<\/p><p class=\"ql-align-center\">您可能在为孩子甄别课外书<\/p><p class=\"ql-align-center\">是时候在这里和孩子一起<\/p><p class=\"ql-align-center\">与最人文最博雅的爸爸们<\/p><p class=\"ql-align-center\">探索好奇心、想象力和创新思维<\/p><p class=\"ql-align-center\">只为你和孩子而来<\/p><p class=\"ql-align-center\">618父亲节等你玩<\/p><p><br><\/p><p>在播呀，有一批这样的主播爸爸：<\/p><p>老潘，小猪短租副总裁，一个被女儿江南欺负到哭的爸爸；<\/p><p>徐和谦，国际新闻记者，一个半夜一边换尿布一边构思国际新闻选题的爸爸；<\/p><p>马小虎，天文科普专家、星空摄影师、天文漫画科普栏目EasyNight的创始人，每天带着儿子看天的爸爸；<\/p><p>王木头，北京大学硕士、资深科普爱好者，攒了一肚子科学故事等孩子出生的准爸爸；<\/p><p>老于，北京新闻广播主持人，每天为刚满周岁的儿子做声音日记的爸爸；<\/p><p>郭教授，北京人民广播电台资深主持人，那个等着给孩子写歌的未来爸爸\u2026\u2026<\/p><p><strong>6月18日父亲节，北京鼓楼西剧场，六个爸爸集体出没！<\/strong><\/p><p>2017年1月，郭教授的节目《林汉达历史故事》上线，很多小朋友追着听，后台留言每天刷屏，很多小朋友都在好奇郭教授是怎样一人饰多个角色，都想来和郭教授一起录音。此后我们定期把小粉丝请到博雅来一起录音，在孩子们心里这已然成为每周最期待的事情。<\/p><p class=\"ql-align-center\">3月，我们建立\u201c老潘粉丝群\u201c，潘叔叔的小粉丝们可是炸开了锅，这些听了潘叔叔讲了两年新闻的小朋友重要离老潘近了一点，群里每天都在讨论什么时候能抓到\u201d活的\u201c老潘。<\/p><p class=\"ql-align-center\">5月，上海的小粉丝专程来到北京，为了看一眼王木头，他好奇木头叔叔为什么懂那么多，一直在想我的偶像到底是什么样的人。<\/p><p class=\"ql-align-center\">很多孩子问小助手，我可以跟徐哥讨论国际大事不？马小虎怎么知道天上那么多事？老于为啥说话可以那么好听？小马哥到底是不是马？<\/p><p>每天，博雅的后台都会收到小朋友们的留言，想见主播们的呼声越来越高。别急，6月18日父亲节，博雅聚集7位主播，陪你过一天不一样的父亲节，带着爸爸一起来吧。<\/p><p><br><\/p><p><strong style=\"color: rgb(217, 33, 66);\">我们强烈建议爸爸带着孩子来<\/strong>，让妈妈自由一天，为了鼓励爸爸参加，我们决定<strong style=\"color: rgb(217, 33, 66);\">爸爸购票享受最最低价<\/strong>。618，和爸爸一起来播呀！<\/p><p class=\"ql-align-center\"><br><\/p><p>鼓楼西剧场！来吧，跟着爸爸和男神主播零距离一起做游戏，现场听他们把世界说给你听。<\/p><p>&nbsp;<\/p><p>&nbsp;<\/p><p><br><\/p><p>在这里，跟着爸爸和主播零距离一起做游戏，让这些平时不苟言笑的傲娇老爹敞开心扉和孩子痛快玩耍，来一场现实版的爸爸去哪儿。<\/p><p><br><\/p><p><strong style=\"color: rgb(192, 0, 0);\">活动当天，需要出示购票二维码，方可入场<\/strong><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/db5bd6b1-ea9e-4a26-963b-a88441aecee1.jpg","categoryId":4,"recommend":"Y","signupNum":354,"likeNum":567,"status":"1","publishTime":"2021-05-08 13:51:04","categoryName":"校园"},{"searchValue":null,"createBy":"admin","createTime":"2021-05-08 13:50:12","updateBy":"1111271","updateTime":"2022-11-05 17:05:25","remark":null,"params":{},"id":41,"name":"独家秘制\u2014\u2014你的TED演讲","content":"<p>一人你饮酒醉<\/p><p>最恨那别人开派对<\/p><p>两眼是快报废<\/p><p>但求升职加薪撒浪嘿<\/p><p>&nbsp;<\/p><p>古往今来，开着派对还能升职加薪的<\/p><p>那只能是参加干爹的生日会了<\/p><p>但是世界上，还就真有一种派对<\/p><p>可以让你升职加薪，语惊四座<\/p><p>千古留名传佳话<\/p><p>还不用认干爹<\/p><p><br><\/p><p>上周我们以《人民的名义》<\/p><p>撒了个精神文明的欢儿<\/p><p><br><\/p><p><span style=\"color: rgb(73, 73, 73);\">关门!!! 放视频<\/span><\/p><p><br><\/p><p>时间：本周五19：00--21:00<\/p><p><br><\/p><p><br><\/p><p>地点 :&nbsp;郎园vintage18号楼东侧，郎园会员中心（咖啡INN后面玻璃门进）<\/p><p><br><\/p><p><br><\/p><p>费用：50RMB （场地有苏福and干净的地毯）<\/p><p><img src=\"http://cdn.huodongxing.com/file/20160403/11742E88D6A35471FFB2F586DC73C39076/30282685450556720.jpg\" alt=\"c782f9608c75ee877257c8d985dc86b5.jpg\" height=\"427\" width=\"594\"><\/p><p>活动流程：<\/p><p><br><\/p><p>1、&nbsp;心灵侦探：认识彼此，也许一不小心会知道太多<\/p><p><br><\/p><p>2、&nbsp;返老还童：回到六岁以前的若干活动<\/p><p><br><\/p><p>3、&nbsp;第88套大脑体操：给大脑一记\u201c春药\u201d<\/p><p><br><\/p><p>4、&nbsp;潜意识大冒险and急转弯真心话<\/p><p><br><\/p><p>5、&nbsp;观点是玩出来滴，TED是独家秘制滴<\/p><p><br><\/p><p><br><\/p><p>安利一下：\u201c即兴表演\u201d牌大力丸<\/p><p><br><\/p><p>说明：即兴戏剧，最早源于意大利的假面喜剧，是一种不依赖剧本的舞台演出形式，后传入欧洲和美国，被广泛应用开发于：演员训练、编剧创意、团队建设、心理疗逾等方面<\/p><p><br><\/p><p>疗效：天性释放，大脑开窍，手舞足蹈，约谁谁答应<\/p><p><br><\/p><p><br><\/p><p>疗效分析：<\/p><p><br><\/p><p>1.它刺激了人本身的游戏性+自发性+创造性<\/p><p>2.利用游戏困境激发潜意识能量<\/p><p>3.利用团队接纳融洽的氛围破除犯错的风险<\/p><p><br><\/p><p><br><\/p><p>当日住持：支叙心<\/p><p><br><\/p><p><img src=\"http://cdn.huodongxing.com/file/20160403/11742E88D6A35471FFB2F586DC73C39076/30262685451096766.jpg\" alt=\"ccb61ab95b6eb201865701d19e02c70b.jpg\"><\/p><p>即兴戏剧实验室 &amp; 即神学院 创始人<\/p><p><br><\/p><p>即兴戏剧\u201c场景订制\u201d第一人<\/p><p><br><\/p><p>前媒体人+10年艺术教学经验<\/p><p><br><\/p><p><img src=\"http://cdn.huodongxing.com/file/20160403/11742E88D6A35471FFB2F586DC73C39076/30422685451356792.jpg\" alt=\"2ff461da8ea96555aa3fe99169c11795.jpg\" height=\"413\" width=\"596\"><\/p><p>\u201c即兴潜能被激发，每一秒都如同神来之笔\u201d<\/p><p><br><\/p><p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\u2014\u2014支叙心<\/p><p><br><\/p><p><br><\/p>","imgUrl":"/prod-api/profile/upload/image/2021/05/08/b597fd0b-0a76-422c-b249-842df5ea9a23.jpg","categoryId":4,"recommend":"N","signupNum":349,"likeNum":435,"status":"1","publishTime":"2021-05-08 13:50:12","categoryName":"校园"}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : admin
         * createTime : 2021-05-08 14:06:09
         * updateBy : 1112172
         * updateTime : 2022-11-12 16:42:55
         * remark : null
         * params : {}
         * id : 50
         * name : 你想在20/30多岁的时候安定下来，拥有理想的工作，有合适的伴侣，有美好的生活方式吗?
         * content : <p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Topic / 话题</strong></p><p>We as human, naturally born with limited time and energy, are always facing the dilemma of exploring different choices and possibilities, and developing and accumulating in one vertical area. As much as the time, energy and effort required to achieve anything in any particular field, to cultivate any meaningful and long-standing relationship with any partner, and to develop your own friend circle for you to feel home and connected in any city, would you be willing to give up the temptation of other possibilities that are potentially more exciting and interesting, more fulfilling and rewarding, more naturing and inspiring?</p><p>我们人类生来就只有有限的时间和精力，总是面临着探索不同的选择和可能性，在一个垂直领域发展和积累的困境。所需的时间、精力和努力实现在任何特定领域，培养与不同合作伙伴有意义的和长期的关系，并开发自己的朋友圈在任何城市建立家的感觉。你愿意为了这些，放弃其他可能的诱惑，可能更令人兴奋并有趣，更有意义和更多的回报，更自然的和更鼓舞人心的事情吗？</p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Host / 主持人</strong></p><p><img src="http://portrait.s.postenglishtime.com/5d624bcbdcab7f1176de3008.jpg?imageView2/2/w/212"></p><p><strong>Chenchen&nbsp;/&nbsp;咨询师</strong></p><p>Chenchen, born and raised in Chengdu until the end of high school, relocated to Canada and U.S for the next 13 years of her life where she not only explored academically amongst 3 schools, 4 majors, but also career wise where experienced 4 different jobs that exposed her to different industries and organization styles. To continue her exploring journey, Chenchen moved back to China 2 years ago and began her adventure within China, within her hometown Sichuan Chengdu; a place that seemed both familiar and strange to her. As much as she has continuously stretched her boundary and vision, she has also been looking for a place, a career path, and the right person to settle with. And she is constantly contemplating the question of balancing between the desire to see the world, explore her potential and true passion，and the need to settle and dive into one particular area to further develop and accumulate on.</p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Attention / 注意事项</strong></p><p><strong>People in insurance sales, MLM, direct selling and P2P... are banned from attending.</strong></p><p><strong>禁止保险销售（比如AIA），传销，直销，p2p等人员参加活动</strong></p><p>Please sign up in advance, or pay extra ¥30</p><p>请提前报名，空降需要额外支付30元场地费</p><p><strong>The event will be cancelled if the number of participants is less than 1/3 of the expected number</strong></p><p><strong>报名人数不足期望人数的1/3活动自动取消，请务必报名，谢谢</strong></p><p><strong>If you're unable to attend, please postpone the participation on the registration page. If refunded, deduct 10% registration fee.</strong></p><p><strong>因特殊情况无法参加，请自己在报名成功页面点击延期参加；如果退款，扣除10%报名费。</strong></p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Event / 活动信息</strong></p><p><strong>Time / 时间</strong></p><p>2021/03/10 Wed 19:00 - 21:00</p><p>2021/03/10 周三 19:00点 - 21:00点</p><p><strong>Number / 人数</strong></p><p>30 people around</p><p>30人左右</p><p><strong>Fee / 费用</strong></p><p>39¥, On site with 25¥ coupon</p><p>39元, 线下报名费，含25元代金券</p><p><strong>Address / 地址</strong></p><p>Younee Bar, F1-3, Ruichen Building, No.13 Nongzhan South Road</p><p>农展馆南路13号瑞辰国际一层 F1-3 友你餐吧</p><p><strong>Contact / 联系主办方</strong></p><p>Wechat/微信：cutepet2015</p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">Past Events / 往期活动</strong></p><iframe class="ql-video" frameborder="0" allowfullscreen="true" src="https://v.qq.com/txp/iframe/player.html?vid=i0715cpz8pz" height="350px" width="100%"></iframe><p><a href="https://mp.weixin.qq.com/s/Mst3Kkr8JvX_h9X75X407Q" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">你相信有灵魂伴侣吗</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/oiGzVwgbHknmz2i2Gc62_w" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">What is loneliness? | 孤独是什么？</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/RTba2pON21bW4312mCtqFQ" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">美食与吃货双语沙龙精彩回顾 | Food &amp; Foodie</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/_UbhjOkXLmEQMAbnNaq7mQ" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">金风玉露，却抵不过媒妁之言 | My parents disapprove of my relationship</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/0XOmJ86We3paxKK6Y1MYvw" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">感恩节沙龙精彩回顾 | Thanksgiving's Day English Salon</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/mjHR4WK0to2B0ws7fHXoLA" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">亚洲的赛车世界，10月25日英语沙龙总结</a></p><p class="ql-align-center"><a href="https://mp.weixin.qq.com/s/zB3Day236B5jYpQTaTb4RQ" rel="noopener noreferrer" target="_blank" style="color: inherit; background-color: transparent;">对于二战的思考，10月18日英语沙龙总结</a></p><p><strong style="color: rgb(255, 255, 255); background-color: rgb(47, 164, 231);">PET Bilingual Salon / 双语沙龙</strong></p><p class="ql-align-center"><img src="http://show.s.postenglishtime.com/8436212952794.jpg?imageView2/2/w/650/interlace/1/q/90"></p><p>The PET Bilingual Salon is a decentralized sharing event where people meets in a relaxed and cozy place every week to exchange ideas on a topic prepared by friends. Participants will have a comprehensive discussion on the topic in different forms and depths, so that they can not only experience the fun of chatting, but also learn other people's thoughts, try to think from different perspectives, improve communication skills, broaden horizons and build their social network.</p><p>PET双语沙龙是一个去中心化的分享活动，每周大家在一个轻松舒适的地方聚会，以朋友准备的一个话题交流想法。大家会通过不同的形式和深度对这个话题进行全面讨论，参加的朋友们不仅可以从中体验到聊天的乐趣，还可以了解别人的想法，尝试从不同的角度思考问题，提高沟通能力，开阔视野，构建自己的人脉网络。</p><p><img src="http://images.s.postenglishtime.com/btmLogo.jpg"></p><p><strong>PostEnglishTime</strong>, Speak English and Make Friends</p><p><strong>PET后英语时代|聊英文，交朋友就在PET</strong></p><p>PostEnglishTime is a high-quality network of English-speaking Chinese professionals in Beijing. We organize various social activities and provide various community services. Our mission is to build a high-quality network of locals and foreigners who are seriously interested in engaging with culture exchange, learning different societies and humanities.</p><p>PET创立于2011年，是一个集语言学习，社交与知识分享的英文爱好者社区。我们致力于成为国内外朋友都信赖的高质量社区，并为广大外语爱好者提供线上线下不同类型的语言交流活动与不同主题的分享与社交活动，及其他社区服务。在这里有趣的灵魂将自由连接，平凡的生命也一样精彩。</p><p><br></p>
         * imgUrl : /prod-api/profile/upload/image/2021/05/08/03d54fd6-6392-4117-816f-b1fc9fa5cedc.jpg
         * categoryId : 6
         * recommend : N
         * signupNum : 3603
         * likeNum : 6665
         * status : 1
         * publishTime : 2021-05-08 14:06:09
         * categoryName : 学习
         */

        private Object searchValue;
        private String createBy;
        private String createTime;
        private String updateBy;
        private String updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private String name;
        private String content;
        private String imgUrl;
        private int categoryId;
        private String recommend;
        private int signupNum;
        private int likeNum;
        private String status;
        private String publishTime;
        private String categoryName;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public String getCreateBy() {
            return createBy;
        }

        public void setCreateBy(String createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public String getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(String updateBy) {
            this.updateBy = updateBy;
        }

        public String getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(String updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getImgUrl() {
            return imgUrl;
        }

        public void setImgUrl(String imgUrl) {
            this.imgUrl = imgUrl;
        }

        public int getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(int categoryId) {
            this.categoryId = categoryId;
        }

        public String getRecommend() {
            return recommend;
        }

        public void setRecommend(String recommend) {
            this.recommend = recommend;
        }

        public int getSignupNum() {
            return signupNum;
        }

        public void setSignupNum(int signupNum) {
            this.signupNum = signupNum;
        }

        public int getLikeNum() {
            return likeNum;
        }

        public void setLikeNum(int likeNum) {
            this.likeNum = likeNum;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getPublishTime() {
            return publishTime;
        }

        public void setPublishTime(String publishTime) {
            this.publishTime = publishTime;
        }

        public String getCategoryName() {
            return categoryName;
        }

        public void setCategoryName(String categoryName) {
            this.categoryName = categoryName;
        }

        public static class ParamsBean {
        }
    }
}
