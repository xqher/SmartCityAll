package com.xqh.smartcityall.pojo;

import java.util.List;

public class CharityRecord {
    /**
     * total : 109
     * rows : [{"searchValue":null,"createBy":null,"createTime":"2022-11-12 22:45:15","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":502,"activityId":14,"userId":1113055,"donateMoney":-7477,"userName":"wzj","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-11 19:12:01","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":495,"activityId":14,"userId":1113071,"donateMoney":1111,"userName":"2097","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:55:50","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":487,"activityId":14,"userId":1112506,"donateMoney":11133,"userName":"popopo","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:55:42","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":486,"activityId":14,"userId":1112506,"donateMoney":2222,"userName":"popopo","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:52:56","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":485,"activityId":14,"userId":1112506,"donateMoney":1111111,"userName":"popopo","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:52:49","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":484,"activityId":14,"userId":1111910,"donateMoney":100,"userName":"admin2","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:48:32","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":483,"activityId":14,"userId":1112506,"donateMoney":123,"userName":"popopo","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:44:25","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":482,"activityId":14,"userId":1111910,"donateMoney":99888,"userName":"admin2","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 14:23:37","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":481,"activityId":14,"userId":1113102,"donateMoney":-99999,"userName":"ce","activityName":"橙色计划助听障儿童"},{"searchValue":null,"createBy":null,"createTime":"2022-11-10 11:33:02","updateBy":null,"updateTime":null,"remark":null,"params":{},"id":480,"activityId":14,"userId":1112696,"donateMoney":2,"userName":"dream","activityName":"橙色计划助听障儿童"}]
     * code : 200
     * msg : 查询成功
     */

    private int total;
    private int code;
    private String msg;
    private List<RowsBean> rows;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<RowsBean> getRows() {
        return rows;
    }

    public void setRows(List<RowsBean> rows) {
        this.rows = rows;
    }

    public static class RowsBean {
        /**
         * searchValue : null
         * createBy : null
         * createTime : 2022-11-12 22:45:15
         * updateBy : null
         * updateTime : null
         * remark : null
         * params : {}
         * id : 502
         * activityId : 14
         * userId : 1113055
         * donateMoney : -7477
         * userName : wzj
         * activityName : 橙色计划助听障儿童
         */

        private Object searchValue;
        private Object createBy;
        private String createTime;
        private Object updateBy;
        private Object updateTime;
        private Object remark;
        private ParamsBean params;
        private int id;
        private int activityId;
        private int userId;
        private int donateMoney;
        private String userName;
        private String activityName;

        public Object getSearchValue() {
            return searchValue;
        }

        public void setSearchValue(Object searchValue) {
            this.searchValue = searchValue;
        }

        public Object getCreateBy() {
            return createBy;
        }

        public void setCreateBy(Object createBy) {
            this.createBy = createBy;
        }

        public String getCreateTime() {
            return createTime;
        }

        public void setCreateTime(String createTime) {
            this.createTime = createTime;
        }

        public Object getUpdateBy() {
            return updateBy;
        }

        public void setUpdateBy(Object updateBy) {
            this.updateBy = updateBy;
        }

        public Object getUpdateTime() {
            return updateTime;
        }

        public void setUpdateTime(Object updateTime) {
            this.updateTime = updateTime;
        }

        public Object getRemark() {
            return remark;
        }

        public void setRemark(Object remark) {
            this.remark = remark;
        }

        public ParamsBean getParams() {
            return params;
        }

        public void setParams(ParamsBean params) {
            this.params = params;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getActivityId() {
            return activityId;
        }

        public void setActivityId(int activityId) {
            this.activityId = activityId;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public int getDonateMoney() {
            return donateMoney;
        }

        public void setDonateMoney(int donateMoney) {
            this.donateMoney = donateMoney;
        }

        public String getUserName() {
            return userName;
        }

        public void setUserName(String userName) {
            this.userName = userName;
        }

        public String getActivityName() {
            return activityName;
        }

        public void setActivityName(String activityName) {
            this.activityName = activityName;
        }

        public static class ParamsBean {
        }
    }
}
