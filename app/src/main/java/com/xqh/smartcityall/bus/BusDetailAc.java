package com.xqh.smartcityall.bus;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.ItemVerticalAdapter;
import com.xqh.smartcityall.pojo.BusLine;
import com.xqh.smartcityall.pojo.BusOrder;
import com.xqh.smartcityall.pojo.BusStation;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.get;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initHRV;

public class BusDetailAc extends BaseAc {


    private android.widget.Button mName;
    private android.widget.TextView mStart;
    private android.widget.TextView mEnd;
    private android.widget.TextView mStatus;
    private android.widget.TextView mTime;
    private androidx.recyclerview.widget.RecyclerView mStationRv;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "线路详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_bus_detail;
    }

    @Override
    public void initData() {

        ItemVerticalAdapter itemAdapter = new ItemVerticalAdapter(activity, null);
        initHRV(mStationRv, itemAdapter);
        BusLine.RowsBean bean = new Gson().fromJson(getJumpId(), BusLine.RowsBean.class);
        mName.setText(getStr(bean.getName()));
        mStart.setText(getStr(bean.getFirst()));
        mEnd.setText(getStr(bean.getEnd()));
        mTime.setText(getStr("运行时间:%s-%s",bean.getStartTime(), bean.getEndTime()));
        mStatus.setText(getStr("票价:%s 元\t\t里程:%s km",bean.getPrice(), bean.getMileage()));

        get("/prod-api/api/bus/stop/list?linesId="+bean.getId(), res -> {
            activity.runOnUiThread(() -> {
                BusStation station = new Gson().fromJson(res, BusStation.class);
                if (station.getCode() == 200) {
                    List<ItemList> items = new ArrayList<>();
                    for(BusStation.RowsBean row:station.getRows()) {
                        ItemList item = new ItemList();
                        item.setTitle(row.getName());
                        items.add(item);
                    }
                    itemAdapter.setData(items);
                }
            });
        });
        mTodoBt.setOnClickListener(v -> {
            BusOrder.RowsBean order = new BusOrder.RowsBean();
            order.setPath(bean.getName());
            order.setPrice(bean.getPrice());
            order.setId(bean.getId());
            order.setStart(bean.getFirst());
            order.setEnd(bean.getEnd());
            jumpId(BusTimeAc.class, new Gson().toJson(order));
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mName = findViewById(R.id.name);
        mStart = findViewById(R.id.start);
        mEnd = findViewById(R.id.end);
        mStatus = findViewById(R.id.status);
        mTime = findViewById(R.id.time);
        mStationRv = findViewById(R.id.stationRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}