package com.xqh.smartcityall.bus;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.ItemVerticalAdapter;
import com.xqh.smartcityall.charity.BusAdapter;
import com.xqh.smartcityall.pojo.BusLine;
import com.xqh.smartcityall.pojo.BusStation;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.get;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initHRV;
import static com.xqh.smartcityall.utils.Const.initRV;

public class BusAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "智慧巴士";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {

        BusAdapter itemAdapter = new BusAdapter(activity, null);
        initRV(mListRv, itemAdapter);
        get("/prod-api/api/bus/line/list", res -> {
            activity.runOnUiThread(() -> {
                BusLine station = new Gson().fromJson(res, BusLine.class);
                if (station.getCode() == 200) {

                    itemAdapter.setData(station.getRows());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}