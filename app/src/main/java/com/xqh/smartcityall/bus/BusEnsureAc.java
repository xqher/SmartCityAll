package com.xqh.smartcityall.bus;

import com.google.gson.Gson;
import com.xqh.smartcityall.MainActivity;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.BusOrder;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;

public class BusEnsureAc extends BaseAc {


    private android.widget.TextView mPageDesc;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "确认订单";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_bus_ensure;
    }

    @Override
    public void initData() {

        BusOrder.RowsBean data = new Gson().fromJson(getJumpId(), BusOrder.RowsBean.class);
        mPageDesc.setText(getStrHtml(
                "<h4>乘客姓名</h4>&emsp;%s<h4>乘客电话</h4>&emsp;%s<h4>乘车时间</h4>&emsp;%s<h4>上车地点</h4>&emsp;%s<h4>下车地点</h4>&emsp;%s",
                getStr(data.getUserName()),
                getStr(data.getUserTel()),
                getStr(data.getCreateTime()),
                getStr(data.getStart()),
                getStr(data.getEnd())

        ));
        mTodoBt.setOnClickListener(v -> {
            HashMap<String, String> map = new HashMap<>();
            map.put("start", data.getStart());
            map.put("end", data.getEnd());
            map.put("price", data.getPrice()+"");
            map.put("path", data.getPath()+"");

            post("/prod-api/api/bus/order", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if(res.getCode()==200) {
                       jump(MainActivity.class);
                       finish();
                    }
                });
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mPageDesc = findViewById(R.id.pageDesc);
        mTodoBt = findViewById(R.id.todoBt);
    }
}