package com.xqh.smartcityall.bus;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.BusOrder;
import com.xqh.smartcityall.pojo.BusStation;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.utils.BaseAc;

import static com.xqh.smartcityall.utils.Const.getStr;

public class BusTimeAc extends BaseAc {

    private String date = "2022-11-11";
    private String time = "12:00";
    private android.widget.TextView mDate;
    private android.widget.TextView mTime;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "设置时间";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_bus_time;
    }

    @Override
    public void initData() {
        mDate.setOnClickListener(v -> {
            DatePickerDialog dateDialog = new DatePickerDialog(activity);
            dateDialog.setOnDateSetListener((view, year, month, dayOfMonth) -> {
                date = year+"-"+(month+1)+"-"+dayOfMonth;
                mDate.setText(date);
            });
            dateDialog.show();
        });

        mTime.setOnClickListener(v -> {
            TimePickerDialog timeDialog = new TimePickerDialog(activity, (TimePickerDialog.OnTimeSetListener) (view, hourOfDay, minute) -> {
                time = hourOfDay+":"+minute;
                mTime.setText(time);
            },12,0,false);
            timeDialog.show();
        });

        mTodoBt.setOnClickListener(v -> {
            BusOrder.RowsBean order = new Gson().fromJson(getJumpId(), BusOrder.RowsBean.class);
            order.setCreateTime(date+" "+time);
            jumpId(BusStationAc.class, new Gson().toJson(order));
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mDate = findViewById(R.id.date);
        mTime = findViewById(R.id.time);
        mTodoBt = findViewById(R.id.todoBt);
    }
}