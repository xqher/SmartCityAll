package com.xqh.smartcityall.bus;

import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.hospital.HospitalCardCreateAc;
import com.xqh.smartcityall.pojo.BusLine;
import com.xqh.smartcityall.pojo.BusOrder;
import com.xqh.smartcityall.pojo.BusStation;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.HospitalCard;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.pojo.UserInfo;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.isEmpty;

public class BusStationAc extends BaseAc {


    private android.widget.TextView mStart;
    private android.widget.TextView mEnd;
    private android.widget.EditText mUsername;
    private android.widget.EditText mPhone;
    private android.widget.Spinner mStSpinner;
    private android.widget.Spinner mEdSpinner;
    private android.widget.Button mTodoBt;
    private List<String> items;
    @Override
    public String initTitle() {
        return "定制巴士";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_bus_station;
    }

    @Override
    public void initData() {
        BusOrder.RowsBean order = new Gson().fromJson(getJumpId(), BusOrder.RowsBean.class);
        mStart.setText(order.getStart());
        mEnd.setText(order.getEnd());

        get("/prod-api/api/common/user/getInfo", res -> {
            runOnUiThread(() -> {
                UserInfo bean = new Gson().fromJson(res, UserInfo.class);
                if(bean.getCode()==200) {
                    UserInfo.UserBean user = bean.getUser();
                    mUsername.setText(user.getUserName());
                    mPhone.setText(user.getPhonenumber());
                }
            });
        });

        get("/prod-api/api/bus/stop/list?linesId="+order.getId(), res -> {
            activity.runOnUiThread(() -> {
                BusStation station = new Gson().fromJson(res, BusStation.class);
                if (station.getCode() == 200) {
                    items = new ArrayList<>();
                    for(BusStation.RowsBean row:station.getRows()) {
                        items.add(row.getName());
                    }

                    ArrayAdapter<String> adapter = new ArrayAdapter<>(activity, android.R.layout.simple_spinner_dropdown_item, items);

                    mStSpinner.setAdapter(adapter);
                    mEdSpinner.setAdapter(adapter);
                    mEdSpinner.setSelection(1);
                    mStSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if(mEdSpinner.getSelectedItemPosition()==position) {
                                mEdSpinner.setSelection((position+1)%items.size());
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                    mEdSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            if(mStSpinner.getSelectedItemPosition()==position) {
                                mStSpinner.setSelection((position+1)%items.size());
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });
                }
            });
        });


        mTodoBt.setOnClickListener(v -> {
            String userName = mUsername.getText().toString();
            String phone = mPhone.getText().toString();

            if(isEmpty(userName)||isEmpty(phone)){
                toast("未输入完成");
            }else {
                // setup start and end
                order.setStart(items.get(mStSpinner.getSelectedItemPosition()));
                order.setEnd(items.get(mEdSpinner.getSelectedItemPosition()));
                order.setUserName(userName);
                order.setUserTel(phone);
                jumpId(BusEnsureAc.class, new Gson().toJson(order));
            }
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mStart = findViewById(R.id.start);
        mEnd = findViewById(R.id.end);
        mUsername = findViewById(R.id.username);
        mPhone = findViewById(R.id.phone);
        mStSpinner = findViewById(R.id.stSpinner);
        mEdSpinner = findViewById(R.id.edSpinner);
        mTodoBt = findViewById(R.id.todoBt);
    }
}