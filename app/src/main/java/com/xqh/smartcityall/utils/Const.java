package com.xqh.smartcityall.utils;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.page.NewsDetailAc;
import com.xqh.smartcityall.pojo.AdvBanner;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.UserInfo;
import com.youth.banner.Banner;
import com.youth.banner.adapter.BannerImageAdapter;
import com.youth.banner.holder.BannerImageHolder;

import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class Const extends Application {
    public static String URL = "http://218.7.112.123:10001";
    public static String USER_NAME;
    private static SharedPreferences sp;
    public static final OkHttpClient CLIENT = new OkHttpClient();
    public static final MediaType JSON = MediaType.parse("application/json");

    @Override
    public void onCreate() {
        super.onCreate();
        sp = getSharedPreferences("sp", 0);
    }
    private static String solveImgUrl(String url) {
        String img = getStr(url);
        if(img.contains("dev-api")) {
            img = img.replace("dev-api","prod-api");
        }else if(img.startsWith("/profile/")) {
            img = "/prod-api" + img;
        }
        return img;
    }
    public static void loadCircleImg(String url, ImageView iv) {
        Glide.with(iv.getContext()).load(URL + solveImgUrl(url)).apply(new RequestOptions().circleCrop()).error(R.drawable.error).into(iv);

    }
    public static void loadImg(String url, ImageView iv) {
        Glide.with(iv.getContext()).load(URL + solveImgUrl(url)).error(R.drawable.error).into(iv);
    }
    public static boolean isEmpty(String str) {
        return str == null || str.trim().length() == 0;
    }
    public static String getStr(String format, Object... param) {
        return String.format(format, param);
    }
    public static Spanned getStrHtml(String format, Object... param) {
        return Html.fromHtml( String.format(getStr(format), param), Html.FROM_HTML_MODE_COMPACT);
    }
    public static String getStr(String str) {
        return isEmpty(str)?"":str;
    }

    public static void initWebView(WebView wv, String content) {
        wv.loadDataWithBaseURL(URL, content, "text/html", "utf-8", null);
    }

    public static void initBanner(Banner banner, List<DefBanner.DataBean> banners, ICallback cb) {
        banner.setLoopTime(3000);
        banner.setAdapter(new BannerImageAdapter<DefBanner.DataBean>(banners) {
            @Override
            public void onBindView(BannerImageHolder bannerImageHolder, DefBanner.DataBean rowsBean, int i, int i1) {
                loadImg(rowsBean.getImgUrl(), bannerImageHolder.imageView);
                bannerImageHolder.imageView.setOnClickListener(v -> {
                    if(cb!=null) {
                        cb.onRes(rowsBean.getId()+"");
                    }
                });
            }
        },true).start();
    }
    public static void initTargetBanner(Banner banner, List<AdvBanner.RowsBean> banners, ICallback cb) {
        banner.setLoopTime(3000);
        banner.setAdapter(new BannerImageAdapter<AdvBanner.RowsBean>(banners) {
            @Override
            public void onBindView(BannerImageHolder bannerImageHolder, AdvBanner.RowsBean rowsBean, int i, int i1) {
                loadImg(rowsBean.getAdvImg(), bannerImageHolder.imageView);
                bannerImageHolder.imageView.setOnClickListener(v -> {
                    if(cb!=null) {
                        cb.onRes(rowsBean.getTargetId()+"");
                    }
                });
            }
        },true).start();
    }
    public static void initSv(SearchView sv, ICallback cb){
        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(isEmpty(query)) {
                    showToast((BaseAc)sv.getContext(), "未输入内容");
                    return true;
                }
                cb.onRes(query);
                sv.clearFocus();
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return false;
            }
        });
    }
    public static void initRV(RecyclerView rv, RecyclerView.Adapter<?> adapter) {
        rv.setLayoutManager(new LinearLayoutManager(rv.getContext(), LinearLayoutManager.VERTICAL, false));
        rv.addItemDecoration(new DividerItemDecoration(rv.getContext(), DividerItemDecoration.VERTICAL));
        rv.setAdapter(adapter);
    }
    public static void initHRV(RecyclerView rv, RecyclerView.Adapter<?> adapter) {
        rv.setLayoutManager(new LinearLayoutManager(rv.getContext(), LinearLayoutManager.HORIZONTAL, false));
//        rv.addItemDecoration(new DividerItemDecoration(rv.getContext(), DividerItemDecoration.HORIZONTAL));
        rv.setAdapter(adapter);
    }
    public static void initGRV(RecyclerView rv, RecyclerView.Adapter<?> adapter, int size) {
        rv.setLayoutManager(new GridLayoutManager(rv.getContext(), size));
        rv.setAdapter(adapter);
    }

    public static void setSp(String k, String v) {
        sp.edit().putString(k, v).apply();
    }

    public static String getSp(String k) {
        return sp.getString(k, "");
    }

    public static void showToast(Activity a, String s) {
        a.runOnUiThread(() -> Toast.makeText(a, s, Toast.LENGTH_SHORT).show());
    }

    public static void jumpTo(Context ctx, Class<?> clz, String k, String v) {
        Intent i = new Intent(ctx, clz);
        i.putExtra(k, v);
        ctx.startActivity(i);
    }
    public static void isLogin(boolean[] ans, Runnable r) {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(Duration.ofMillis(700)).build();
        client.newCall(new Request.Builder()
                .url(URL + "/prod-api/api/common/user/getInfo")
                .addHeader("Authorization", getSp("token"))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                if(r!=null) {
                    r.run();
                }
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String json = response.body().string();
                UserInfo userInfo = new Gson().fromJson(json ,UserInfo.class);
                if(userInfo.getCode()==200) {
                    ans[0] = true;
                }
                if(r!=null) {
                    r.run();
                }
            }
        });
    }
    public static void checkUrl(boolean[] ans, Runnable r) {
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(Duration.ofMillis(700)).build();
        client.newCall(new Request.Builder()
                .url(URL + "/prod-api/api/rotation/list")
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                if(r!=null) {
                    r.run();
                }
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                String json = response.body().string();
                UserInfo userInfo = new Gson().fromJson(json ,UserInfo.class);
                if(userInfo.getCode()==200) {
                    ans[0] = true;
                }
                if(r!=null) {
                    r.run();
                }
            }
        });
    }

    public static void get(String url, ICallback cb) {
        CLIENT.newCall(new Request.Builder()
                .url(URL + url)
                .addHeader("Authorization", getSp("token"))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {

                Log.e("OkHttp", e.toString());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                cb.onRes(response.body().string());
            }
        });
    }
    public static void post(String url, HashMap<String, String> map, ICallback cb) {
        CLIENT.newCall(new Request.Builder()
                .url(URL + url)
                .addHeader("Authorization", getSp("token"))
                .post(RequestBody.create(new Gson().toJson(map), JSON))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {


                Log.e("OkHttp", e.toString());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                cb.onRes(response.body().string());
            }
        });
    }
    public static void put(String url, HashMap<String, String> map, ICallback cb) {
        CLIENT.newCall(new Request.Builder()
                .url(URL + url)
                .addHeader("Authorization", getSp("token"))
                .put(RequestBody.create(new Gson().toJson(map), JSON))
                .build()).enqueue(new Callback() {
            @Override
            public void onFailure(@NotNull Call call, @NotNull IOException e) {
                Log.e("OkHttp", e.toString());
            }

            @Override
            public void onResponse(@NotNull Call call, @NotNull Response response) throws IOException {
                cb.onRes(response.body().string());
            }
        });
    }

    public interface ICallback {
        void onRes(String res);
    }



}
