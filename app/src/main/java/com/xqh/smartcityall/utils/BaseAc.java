package com.xqh.smartcityall.utils;

import android.app.Activity;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.*;

public abstract class BaseAc extends AppCompatActivity {
    public Activity activity;
    public android.widget.ImageView mBack;
    public android.widget.TextView mTitle;
    public android.widget.ImageView mOption;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(initLayout());

        activity = this;
        mBack = findViewById(R.id.back);
        mTitle = findViewById(R.id.title);
        mOption = findViewById(R.id.option);

        if(mTitle!=null) {
            mTitle.setText(getStr(initTitle()));
            mBack.setOnClickListener(v -> finish());
        }
        initView();
        initData();

    }
    public void setOptionClick(View.OnClickListener v){
        mOption.setVisibility(View.VISIBLE);
        mOption.setImageResource(R.drawable.ic_list);
        mOption.setOnClickListener(v);
    }
    public void setOptionClick(int res, View.OnClickListener v){
        mOption.setVisibility(View.VISIBLE);
        mOption.setImageResource(res);
        mOption.setOnClickListener(v);
    }
    public void setFr(Fragment fr, FrameLayout fl) {
        getSupportFragmentManager().beginTransaction().replace(fl.getId(), fr).commit();
    }


    public void toast(String s) {
        showToast(activity, s);
    }

    public String getJumpId() {
        return getStr(activity.getIntent().getStringExtra("id"));
    }

    public String getJumpVal(String k) {
        return getStr(activity.getIntent().getStringExtra(k));
    }

    public void jump(Class<?> c) {
        jumpTo(activity, c, "", "");
    }
    public void jumpId(Class<?> c, Object v) {
        jumpTo(activity, c, "id", new Gson().toJson(v));
    }
    public void jumpId(Class<?> c, String v) {
        jumpTo(activity, c, "id", v);
    }
    public void jumpId(Class<?> c, int v) {
        jumpTo(activity, c, "id", v+"");
    }

    public void jump(Class<?> c, String k, String v) {
        jumpTo(activity, c, k, v);
    }

    public void get(String url, ICallback cb) {
        Const.get(url, cb);
    }

    public void post(String url, HashMap<String, String> map, ICallback cb) {
        Const.post(url, map, cb);
    }

    public void put(String url, HashMap<String, String> map, ICallback cb) {
        Const.put(url, map, cb);
    }

    @Override
    public void onResume() {
        super.onResume();
        URL = getSp("url");

        resumeData();
    }

    public abstract String initTitle();

    public abstract int initLayout();

    public abstract void initData();

    public abstract void resumeData();

    public abstract void initView();
}
