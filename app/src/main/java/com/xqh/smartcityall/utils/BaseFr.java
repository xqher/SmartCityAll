package com.xqh.smartcityall.utils;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.gson.Gson;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.jumpTo;
import static com.xqh.smartcityall.utils.Const.showToast;

public abstract class BaseFr extends Fragment {
    public Activity activity;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(initLayout(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        activity = getActivity();

        initView();
        initData();
    }

    @Override
    public void onResume() {
        super.onResume();
        resumeData();
    }

    public void runOnUi(Runnable r) {
        if (isAdded())
            getActivity().runOnUiThread(r);
    }

    public void toast(String s) {
        if (isAdded())
            showToast(activity, s);
    }



    protected  <T extends View> T findViewById(int id) {
        return activity.findViewById(id);
    }
    public void jump(Class<?> c) {
        jumpTo(activity, c, "", "");
    }
    public void jumpId(Class<?> c, Object v) {
        jumpTo(activity, c, "id", new Gson().toJson(v));
    }
    public void jumpId(Class<?> c, String v) {
        jumpTo(activity, c, "id", v);
    }
    public void jumpId(Class<?> c, int v) {
        jumpTo(activity, c, "id", v+"");
    }

    public void jump(Class<?> c, String k, String v) {
        jumpTo(activity, c, k, v);
    }

    public void get(String url, Const.ICallback cb) {
        Const.get(url, cb);
    }

    public void post(String url, HashMap<String, String> map, Const.ICallback cb) {
        Const.post(url, map, cb);
    }

    public void put(String url, HashMap<String, String> map, Const.ICallback cb) {
        Const.put(url, map, cb);
    }


    public abstract int initLayout();

    public abstract void initData();

    public abstract void resumeData();

    public abstract void initView();
}
