package com.xqh.smartcityall.utils;

import android.view.View;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;

import static com.xqh.smartcityall.utils.Const.*;

public class EmptyAc extends BaseAc {


    @Override
    public String initTitle() {
        return getJumpId();
    }

    @Override
    public int initLayout() {
        return R.layout.activity_empty;
    }

    @Override
    public void initData() {
        mTitle.setText(getStr(getJumpId()));
        get("/prod-api/api/rotation/list?type=2", res -> {
            runOnUiThread(() -> {
                DefRes bean = new Gson().fromJson(res, DefRes.class);
                if (bean.getCode() == 200) {

                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

    }
}