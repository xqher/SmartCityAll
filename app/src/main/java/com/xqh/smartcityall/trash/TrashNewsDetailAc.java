package com.xqh.smartcityall.trash;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.CommentAdapter;
import com.xqh.smartcityall.pojo.AvatarComment;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.NewsDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;

import static com.xqh.smartcityall.utils.Const.URL;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.isEmpty;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class TrashNewsDetailAc extends BaseAc {


    private android.widget.TextView mPageTitle;
    private android.widget.TextView mPageStatus;
    private android.widget.ImageView mCover;
    private android.webkit.WebView mContent;
    private androidx.recyclerview.widget.RecyclerView mCommentRv;
    private com.google.android.material.textfield.TextInputEditText mCommentEt;
    private android.widget.Button mCommentBt;

    private CommentAdapter adapter;

    @Override
    public String initTitle() {
        return "新闻详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_detail_comment;
    }

    @Override
    public void initData() {

        get("/prod-api/api/garbage-classification/news/"+getJumpId(),res -> {
            runOnUiThread(()->{
                NewsDetail bean = new Gson().fromJson(res, NewsDetail.class);
                if(bean.getCode()==200) {
                    NewsDetail.DataBean data = bean.getData();
                    loadImg(data.getImgUrl(), mCover);
                    mPageTitle.setText(getStr(data.getTitle()));
                    mPageStatus.setText(getStr(data.getAuthor())+"于"+data.getCreateTime()+"发布");
                    mContent.loadDataWithBaseURL(URL, data.getContent(), "text/html", "utf-8", null);
                }
            });
        });

        adapter = new CommentAdapter(activity, null);
        initRV(mCommentRv, adapter);

        loadComment();
        mCommentBt.setOnClickListener(v -> {
            String content = mCommentEt.getText().toString();
            if(isEmpty(content)) {
                toast("未输入完成");
                return;
            }
            HashMap<String, String> map = new HashMap<>();
            map.put("newsId", getJumpId());
            map.put("content", content);

            post("/prod-api/api/garbage-classification/news-comment", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    loadComment();
                    if(res.getCode()==200) {
                        mCommentEt.setText("");
                    }
                });
            });
        });
    }
    public void loadComment() {
        get(String.format("/prod-api/api/garbage-classification/news-comment/list?newsId=%s&pageSize=20&pageNum=%s", getJumpId(), 1), res -> {
            runOnUiThread(()->{
                AvatarComment bean = new Gson().fromJson(res, AvatarComment.class);
                if(bean.getCode()==200) {
                    if(bean.getRows() == null || bean.getRows().size()==0) {
                        toast("没有更多数据了");
                        return;
                    }
                    adapter.setData(bean.getRows());
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mPageTitle = findViewById(R.id.pageTitle);
        mPageStatus = findViewById(R.id.pageStatus);
        mCover = findViewById(R.id.cover);
        mContent = findViewById(R.id.content);
        mCommentRv = findViewById(R.id.commentRv);
        mCommentEt = findViewById(R.id.commentEt);
        mCommentBt = findViewById(R.id.commentBt);
    }
}