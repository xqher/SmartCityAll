package com.xqh.smartcityall.trash;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.NewsBean.RowsBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class TrashAdapter extends RecyclerView.Adapter<TrashAdapter.VH> {


    private List<RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public RowsBean posBean;

    public TrashAdapter(Context ctx, List<RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_news, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_news;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mDate;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mStatus.setVisibility(View.GONE);
            mContent.setVisibility(View.GONE);
            ((LinearLayout)itemView).removeView(mCover);
            ((LinearLayout)itemView).addView(mCover);
        }

        public void onItem(RowsBean bean) {

            loadImg(bean.getImgUrl(), mCover);
            mTitle.setText(getStr(bean.getTitle()));
            mDate.setText(getStr(bean.getCreateTime()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(TrashNewsDetailAc.class, bean.getId());
            });
        }

        private void initView() {
            mCover = itemView.findViewById(R.id.cover);
            mTitle = itemView.findViewById(R.id.title);
            mContent = itemView.findViewById(R.id.content);
            mDate = itemView.findViewById(R.id.date);
            mStatus = itemView.findViewById(R.id.status);
        }
    }
}
