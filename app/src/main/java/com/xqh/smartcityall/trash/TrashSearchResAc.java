package com.xqh.smartcityall.trash;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.charity.CharityListAc;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.pojo.TrashType;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class TrashSearchResAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageDesc;
    private androidx.recyclerview.widget.RecyclerView mIconRv;
    private android.widget.TextView mPageGuide;

    @Override
    public String initTitle() {
        return "搜索结果";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_trash_search_res;
    }

    @Override
    public void initData() {

        TrashType.RowsBean bean = new Gson().fromJson(getJumpId(), TrashType.RowsBean.class);
        loadImg(bean.getImgUrl(), mCover);
        mPageDesc.setText(getStr(bean.getIntroduce()));
        mPageGuide.setText(getStr(bean.getGuide()));

        IconAdapter iconAdapter = new IconAdapter(activity, null);
        iconAdapter.onClick = ()->{
            jumpId(TrashTypeDetailAc.class, new Gson().toJson(bean));
        };
        initGRV(mIconRv, iconAdapter, 4);

        get("/prod-api/api/garbage-classification/garbage-example/list?type="+bean.getId(), res -> {
            runOnUiThread(() -> {
                ServeBean iconBean = new Gson().fromJson(res, ServeBean.class);
                if (iconBean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    for (ServeBean.RowsBean data : iconBean.getRows()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId()+""));
                        show.add(icon);
                    }
                    iconAdapter.setData(show);
                }
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageDesc = findViewById(R.id.pageDesc);
        mIconRv = findViewById(R.id.iconRv);
        mPageGuide = findViewById(R.id.pageGuide);
    }
}