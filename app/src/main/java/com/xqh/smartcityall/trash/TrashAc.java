package com.xqh.smartcityall.trash;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.NewsBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initRV;

public class TrashAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private android.widget.Button mSearchBt;
    private android.widget.Button mTypeBt;
    private com.google.android.material.tabs.TabLayout mTl;
    private androidx.recyclerview.widget.RecyclerView mListRv;

    @Override
    public String initTitle() {
        return "垃圾分类";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_trash;
    }

    @Override
    public void initData() {


        get("/prod-api/api/garbage-classification/poster/list", res -> {
            runOnUiThread(() -> {
                DefBanner bean = new Gson().fromJson(res, DefBanner.class);
                if (bean.getCode() == 200) {
                    initBanner(mBanner, bean.getData(), id->{});
                }
            });
        });
        TrashAdapter listAdapter = new TrashAdapter(activity, null);
        initRV(mListRv, listAdapter);

        mSearchBt.setOnClickListener(v -> jump(TrashSearchAc.class));
        mTypeBt.setOnClickListener(v -> jump(TrashTypeAc.class));
        get("/prod-api/api/garbage-classification/news/list?type=7", res -> {
            runOnUiThread(() -> {
                NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                if (bean.getCode() == 200) {
                    listAdapter.setData(bean.getRows());
                }
            });
        });

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int type = 7+tab.getPosition();
                get("/prod-api/api/garbage-classification/news/list?type="+type, res -> {
                    runOnUiThread(() -> {
                        NewsBean bean = new Gson().fromJson(res, NewsBean.class);
                        if (bean.getCode() == 200) {
                           listAdapter.setData(bean.getRows());
                        }
                    });
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mSearchBt = findViewById(R.id.searchBt);
        mTypeBt = findViewById(R.id.typeBt);
        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
    }
}