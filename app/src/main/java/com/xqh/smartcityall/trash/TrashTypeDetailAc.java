package com.xqh.smartcityall.trash;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.TrashType;
import com.xqh.smartcityall.utils.BaseAc;
import static com.xqh.smartcityall.utils.Const.*;

public class TrashTypeDetailAc extends BaseAc {


    private android.widget.ImageView mCover;
    private android.widget.TextView mPageDesc;
    private android.widget.TextView mPageGuide;

    @Override
    public String initTitle() {
        return "分类详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_trash_type_detail;
    }

    @Override
    public void initData() {

        TrashType.RowsBean bean = new Gson().fromJson(getJumpId(), TrashType.RowsBean.class);
        loadImg(bean.getImgUrl(), mCover);
        mPageDesc.setText(getStr(bean.getIntroduce()));
        mPageGuide.setText(getStr(bean.getGuide()));
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mCover = findViewById(R.id.cover);
        mPageDesc = findViewById(R.id.pageDesc);
        mPageGuide = findViewById(R.id.pageGuide);
    }
}