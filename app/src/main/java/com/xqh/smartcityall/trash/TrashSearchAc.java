package com.xqh.smartcityall.trash;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.ItemAdapter;
import com.xqh.smartcityall.adapter.ManifestAdapter;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.ItemList;
import com.xqh.smartcityall.pojo.ItemManifest;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.pojo.TrashKeyWord;
import com.xqh.smartcityall.pojo.TrashType;
import com.xqh.smartcityall.utils.BaseAc;
import com.xqh.smartcityall.utils.Const;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class TrashSearchAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private androidx.appcompat.widget.SearchView mSv;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private List<TrashType.RowsBean> types = new ArrayList<>();
    private Const.ICallback search;
    @Override
    public String initTitle() {
        return "垃圾搜索";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_trash_search;
    }

    @Override
    public void initData() {
        get("/prod-api/api/garbage-classification/poster/list", res -> {
            runOnUiThread(() -> {
                DefBanner bean = new Gson().fromJson(res, DefBanner.class);
                if (bean.getCode() == 200) {
                    initBanner(mBanner, bean.getData(), id->{});
                }
            });
        });
        search = res -> {
            boolean flag = false;
            for(TrashType.RowsBean row:types) {
                if(row.getName().contains(res)) {
                    jumpId(TrashSearchResAc.class, new Gson().toJson(row));
                    flag = true;
                }
            }
            if(!flag) toast("未搜索到相关垃圾分类");
        };
        initSv(mSv, search);

        ManifestAdapter listAdapter = new ManifestAdapter(activity, null);

        listAdapter.onClick = ()->{
          search.onRes(listAdapter.posBean.getName());
        };

        initRV(mListRv, listAdapter);

        get("/prod-api/api/garbage-classification/garbage-classification/list", res -> {
            runOnUiThread(() -> {
                TrashType bean = new Gson().fromJson(res, TrashType.class);
                if (bean.getCode() == 200) {
                    types = bean.getRows();
                }
            });
        });

        get("/prod-api/api/garbage-classification/garbage-classification/hot/list", res -> {
            runOnUiThread(() -> {
                TrashKeyWord bean = new Gson().fromJson(res, TrashKeyWord.class);
                if (bean.getCode() == 200) {
                    List<ItemManifest> tmp = new ArrayList<>();
                    for(TrashKeyWord.DataBean data:bean.getData()) {
                        tmp.add(new ItemManifest(data.getKeyword(), "搜索次数:"+data.getSearchTimes()));
                    }
                    listAdapter.setData(tmp);
                }
            });
        });

    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mSv = findViewById(R.id.sv);
        mListRv = findViewById(R.id.listRv);
    }
}