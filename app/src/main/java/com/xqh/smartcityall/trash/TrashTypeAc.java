package com.xqh.smartcityall.trash;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.pojo.TrashType;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initGRV;

public class TrashTypeAc extends BaseAc {


    private android.widget.Button mABt;
    private android.widget.Button mBBt;
    private android.widget.Button mCBt;
    private android.widget.Button mDBt;
    private com.google.android.material.tabs.TabLayout mTl;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private List<TrashType.RowsBean> types = new ArrayList<>();

    @Override
    public String initTitle() {
        return "垃圾分类";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_trash_type;
    }

    @Override
    public void initData() {
        get("/prod-api/api/garbage-classification/garbage-classification/list", res -> {
            runOnUiThread(() -> {
                TrashType bean = new Gson().fromJson(res, TrashType.class);
                if (bean.getCode() == 200) {
                    types = bean.getRows();
                }
            });
        });
        mABt.setOnClickListener(v -> jumpId(TrashTypeDetailAc.class, new Gson().toJson(types.get(0))));
        mBBt.setOnClickListener(v -> jumpId(TrashTypeDetailAc.class, new Gson().toJson(types.get(1))));
        mCBt.setOnClickListener(v -> jumpId(TrashTypeDetailAc.class, new Gson().toJson(types.get(2))));
        mDBt.setOnClickListener(v -> jumpId(TrashTypeDetailAc.class, new Gson().toJson(types.get(3))));

        IconAdapter iconAdapter = new IconAdapter(activity, null);
        iconAdapter.onClick = ()->{
            for(TrashType.RowsBean type:types) {
                if((""+type.getId()).equals(iconAdapter.posBean.getIconId())) {
                    jumpId(TrashTypeDetailAc.class, new Gson().toJson(type));
                }
            }
        };
        initGRV(mListRv, iconAdapter, 4);
        get("/prod-api/api/garbage-classification/garbage-example/list?type=8", res -> {
            runOnUiThread(() -> {
                ServeBean iconBean = new Gson().fromJson(res, ServeBean.class);
                if (iconBean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    for (ServeBean.RowsBean data : iconBean.getRows()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId()+""));
                        show.add(icon);
                    }
                    iconAdapter.setData(show);
                }
            });
        });

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int id = 8 + tab.getPosition();
                get("/prod-api/api/garbage-classification/garbage-example/list?type="+id, res -> {
                    runOnUiThread(() -> {
                        ServeBean iconBean = new Gson().fromJson(res, ServeBean.class);
                        if (iconBean.getCode() == 200) {
                            List<IconBean> show = new ArrayList<>();
                            for (ServeBean.RowsBean data : iconBean.getRows()) {
                                IconBean icon = new IconBean();
                                icon.setIconImg(getStr(data.getImgUrl()));
                                icon.setIconName(getStr(data.getName()));
                                icon.setIconId(getStr(data.getId()+""));
                                show.add(icon);
                            }
                            iconAdapter.setData(show);
                        }
                    });
                });
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mABt = findViewById(R.id.aBt);
        mBBt = findViewById(R.id.bBt);
        mCBt = findViewById(R.id.cBt);
        mDBt = findViewById(R.id.dBt);
        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
    }
}