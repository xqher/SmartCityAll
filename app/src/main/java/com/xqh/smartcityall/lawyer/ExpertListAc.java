package com.xqh.smartcityall.lawyer;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class ExpertListAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "法律专长列表";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {


        IconAdapter iconAdapter = new IconAdapter(activity, null);
        iconAdapter.onClick = ()->{
            jumpId(LawyerListAc.class, iconAdapter.posBean.getIconId());
        };
        initGRV(mListRv, iconAdapter, 4);
        get("/prod-api/api/lawyer-consultation/legal-expertise/list", res -> {
            runOnUiThread(() -> {
                ServeBean bean = new Gson().fromJson(res, ServeBean.class);
                if (bean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    for (ServeBean.RowsBean data : bean.getRows()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId()+""));
                        show.add(icon);
                    }
                    iconAdapter.setData(show);
                }
            });
        });



    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}