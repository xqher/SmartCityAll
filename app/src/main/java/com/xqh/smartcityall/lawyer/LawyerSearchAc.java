package com.xqh.smartcityall.lawyer;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.LawyerBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.initRV;

public class LawyerSearchAc extends BaseAc {


    private androidx.recyclerview.widget.RecyclerView mListRv;
    private LawyerAdapter adapter;
    @Override
    public String initTitle() {
        return "搜索结果";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_list_page;
    }

    @Override
    public void initData() {

        adapter = new LawyerAdapter(activity, null);
        initRV(mListRv, adapter);


    }

    @Override
    public void resumeData() {
        get("/prod-api/api/lawyer-consultation/lawyer/list", res -> {
            runOnUiThread(()->{
                LawyerBean bean = new Gson().fromJson(res, LawyerBean.class);
                List<LawyerBean.RowsBean> temp = new ArrayList<>();
                if(bean.getCode()==200) {
                    for(LawyerBean.RowsBean row:bean.getRows()) {
                        if(row.getName().contains(getJumpId())) {
                            temp.add(row);
                        }
                    }
                }
                adapter.setData(temp);
                if(temp.size()==0) {
                    toast("未查询到相应结果");
                }
            });
        });
    }

    @Override
    public void initView() {

        mListRv = findViewById(R.id.listRv);
    }
}