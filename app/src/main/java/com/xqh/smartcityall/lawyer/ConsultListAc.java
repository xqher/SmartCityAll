package com.xqh.smartcityall.lawyer;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.ConsultBean;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class ConsultListAc extends BaseAc {


    private com.google.android.material.tabs.TabLayout mTl;
    private androidx.recyclerview.widget.RecyclerView mListRv;

    @Override
    public String initTitle() {
        return "我的咨询";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_consult_list;
    }

    @Override
    public void initData() {
        ConsultAdapter listAdapter = new ConsultAdapter(activity, null);
        initRV(mListRv, listAdapter);

        get("/prod-api/api/lawyer-consultation/legal-advice/list", res -> {
            runOnUiThread(() -> {
                ConsultBean bean = new Gson().fromJson(res, ConsultBean.class);
                if (bean.getCode() == 200) {
                    listAdapter.setData(bean.getRows());
                }
            });
        });
        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getText().equals("未受理")) {
                    get("/prod-api/api/lawyer-consultation/legal-advice/list", res -> {
                        runOnUiThread(() -> {
                            ConsultBean bean = new Gson().fromJson(res, ConsultBean.class);
                            if (bean.getCode() == 200) {
                                listAdapter.setData(bean.getRows());
                            }
                        });
                    });
                }else {
                    listAdapter.setData(null);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

    }
    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
    }
}