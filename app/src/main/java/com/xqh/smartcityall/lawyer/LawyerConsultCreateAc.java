package com.xqh.smartcityall.lawyer;

import android.widget.ArrayAdapter;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.DefRowType;
import com.xqh.smartcityall.pojo.LawyerDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class LawyerConsultCreateAc extends BaseAc {



    private List<DefRowType.RowsBean> types = new ArrayList<>();
    private android.widget.ImageView mAvatar;
    private android.widget.TextView mName;
    private android.widget.TextView mDetail;
    private android.widget.TextView mStatus;
    private android.widget.Spinner mSpinner;
    private com.google.android.material.textfield.TextInputEditText mPhone;
    private com.google.android.material.textfield.TextInputEditText mContent;
    private android.widget.Button mUploadBt;
    private android.widget.Button mTodoBt;

    @Override
    public String initTitle() {
        return "新建诉求";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_consult_create;
    }

    @Override
    public void initData() {
        mUploadBt.setOnClickListener(v -> {
            toast("懒得写");
        });
        get("/prod-api/api/lawyer-consultation/lawyer/"+getJumpId(), lawyerRes -> {
            activity.runOnUiThread(() -> {
                LawyerDetail bean = new Gson().fromJson(lawyerRes, LawyerDetail.class);
                if (bean.getCode() == 200) {
                    LawyerDetail.DataBean data = bean.getData();
                    loadImg(data.getAvatarUrl(), mAvatar);
                    mName.setText(getStr(data.getName()));
                    mDetail.setText(getStrHtml("擅长:%s&emsp;已服务:%s人<br/>",
                            data.getLegalExpertiseName() ,
                            data.getServiceTimes() ));
                    mStatus.setText(String.format("好评率:%s",
                            data.getFavorableRate()+"%"));
                }
            });
        });

        get("/prod-api/api/lawyer-consultation/legal-expertise/list", json -> {
            runOnUiThread(()->{
                DefRowType bean = new Gson().fromJson(json, DefRowType.class);
                if(bean.getCode()==200) {
                    types = bean.getRows();
                    List<String> tmp = new ArrayList<>();
                    for(int i = 0;i<types.size();i++) {
                        DefRowType.RowsBean row = types.get(i);
                        tmp.add(row.getName());
                    }
                    mSpinner.setAdapter(new ArrayAdapter<String>(activity, android.R.layout.simple_spinner_dropdown_item, tmp));

                }
            });
        });
        mTodoBt.setOnClickListener(v -> {
            String phone = mPhone.getText().toString();
            String content = mContent.getText().toString();
            if (phone.isEmpty()
                    || content.isEmpty()
            ) {
                toast("未输入完成");
                return;
            }
            if(phone.length()<11) {
                toast("手机号码格式错误");
                return;
            }

            HashMap<String, String> map = new HashMap<>();
            map.put("legalExpertiseId", types.get(mSpinner.getSelectedItemPosition()).getId()+"");
            map.put("lawyerId", getJumpId());
            map.put("content", content);
            map.put("phone", phone);

            post("/prod-api/api/lawyer-consultation/legal-advice", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                    if(res.getCode() == 200) {
                        finish();
                    }
                });
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mAvatar = findViewById(R.id.avatar);
        mName = findViewById(R.id.name);
        mDetail = findViewById(R.id.detail);
        mStatus = findViewById(R.id.status);
        mSpinner = findViewById(R.id.spinner);
        mPhone = findViewById(R.id.phone);
        mContent = findViewById(R.id.content);
        mUploadBt = findViewById(R.id.uploadBt);
        mTodoBt = findViewById(R.id.todoBt);
    }
}