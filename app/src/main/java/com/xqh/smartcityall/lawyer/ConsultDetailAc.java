package com.xqh.smartcityall.lawyer;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ConsultDetail;
import com.xqh.smartcityall.pojo.HouseDetail;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.LawyerDetail;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.get;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadCircleImg;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ConsultDetailAc extends BaseAc {


    private android.widget.ImageView mAvatar;
    private android.widget.TextView mName;
    private android.widget.TextView mDetail;
    private android.widget.TextView mStatus;
    private android.widget.ImageView mCover;
    private android.widget.TextView mContent;
    private List<ServeBean.RowsBean> experts = new ArrayList<>();

    @Override
    public String initTitle() {
        return "咨询详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_consult_detail;
    }

    @Override
    public void initData() {


        get("/prod-api/api/lawyer-consultation/legal-advice/"+getJumpId(), res -> {
            runOnUiThread(() -> {
                ConsultDetail bean = new Gson().fromJson(res, ConsultDetail.class);
                if (bean.getCode() == 200) {
                    ConsultDetail.DataBean data = bean.getData();
                    loadImg(data.getImageUrls(), mCover);

                    get("/prod-api/api/lawyer-consultation/lawyer/"+data.getLawyerId(), lawyerRes -> {
                        activity.runOnUiThread(() -> {
                            LawyerDetail lawyer = new Gson().fromJson(lawyerRes, LawyerDetail.class);
                            if (lawyer.getCode() == 200) {
                                LawyerDetail.DataBean lawyerBean = lawyer.getData();
                                loadImg(lawyerBean.getAvatarUrl(), mAvatar);
                                mName.setText(getStr(lawyerBean.getName()));
                                mDetail.setText(getStrHtml("擅长:%s&emsp;已服务:%s人<br/>",
                                        lawyerBean.getLegalExpertiseName() ,
                                        lawyerBean.getServiceTimes() ));
                                mStatus.setText(String.format("好评率:%s",
                                        lawyerBean.getFavorableRate()+"%"));
                            }
                        });
                    });

                    get("/prod-api/api/lawyer-consultation/legal-expertise/list", expertise -> {
                        runOnUiThread(() -> {
                            ServeBean serve = new Gson().fromJson(expertise, ServeBean.class);
                            if (serve.getCode() == 200) {
                                experts = serve.getRows();
                                mContent.setText(getStrHtml(
                                        "<h4>问题类型</h4>&emsp;%s<h4>受理状态</h4>&emsp;%s<h4>提交时间</h4>&emsp;%s<h4>联系电话</h4>&emsp;%s<h4>描述</h4>&emsp;%s",
                                        getStr("拆迁安置"),
                                        getStr(data.getState().equals("0")?"未处理":"已完成"),
                                        getStr(data.getCreateTime()),
                                        getStr(data.getPhone()),
                                        getStr(data.getContent())
                                ));
                                for(ServeBean.RowsBean row:experts) {
                                    if(row.getId()==data.getLegalExpertiseId()) {
                                        mContent.setText(getStrHtml(
                                                "<h4>问题类型</h4>&emsp;%s<h4>受理状态</h4>&emsp;%s<h4>提交时间</h4>&emsp;%s<h4>联系电话</h4>&emsp;%s<h4>描述</h4>&emsp;%s",
                                                getStr(row.getName()),
                                                getStr(data.getState().equals("0")?"未处理":"已完成"),
                                                getStr(data.getCreateTime()),
                                                getStr(data.getPhone()),
                                                getStr(data.getContent())
                                        ));
                                    }
                                }
                            }
                        });
                    });

                }
            });
        });

    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mAvatar = findViewById(R.id.avatar);
        mName = findViewById(R.id.name);
        mDetail = findViewById(R.id.detail);
        mStatus = findViewById(R.id.status);
        mCover = findViewById(R.id.cover);
        mContent = findViewById(R.id.content);
    }
}