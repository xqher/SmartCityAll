package com.xqh.smartcityall.lawyer;

import android.app.AlertDialog;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.page.ServiceAdapter;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.LawyerBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class LawyerListAc extends BaseAc {


    private LawyerAdapter listAdapter;
    private String typeId = "10";
    private com.google.android.material.tabs.TabLayout mTl;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private String[] sortType = {"", "workStartAt", "serviceTimes", "favorableRate"};
    private String sort = "";
    private android.widget.Button mTodoBt;
    private List<IconBean> icons = new ArrayList<>();

    @Override
    public String initTitle() {
        return "律师列表";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_lawyer_list;
    }

    @Override
    public void initData() {

        typeId = getJumpId();

        listAdapter = new LawyerAdapter(activity, null);
        initRV(mListRv, listAdapter);

        IconAdapter iconAdapter = new IconAdapter(activity, null);


        get("/prod-api/api/lawyer-consultation/legal-expertise/list", res -> {
            runOnUiThread(() -> {
                ServeBean bean = new Gson().fromJson(res, ServeBean.class);
                if (bean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    for (ServeBean.RowsBean data : bean.getRows()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId() + ""));
                        show.add(icon);
                    }
                    icons = show;
                }
            });
        });

        updateData();
        mTodoBt.setOnClickListener(v -> {
            RecyclerView rv = new RecyclerView(activity);

            initGRV(rv, iconAdapter, 4);
            iconAdapter.setData(icons);

            AlertDialog dialog = new AlertDialog.Builder(activity)
                    .setTitle("筛选专长")
                    .setView(rv)
                    .setPositiveButton("取消", null)
                    .create();

            iconAdapter.onClick = () -> {
                typeId = "" +iconAdapter.posBean.getIconId();
                updateData();

            };
            dialog.show();
        });

        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                sort = sortType[tab.getPosition()];
                updateData();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });


    }

    private void updateData() {
        get(getStr("/prod-api/api/lawyer-consultation/lawyer/list?legalExpertiseId=%s&sort=%s", typeId, sort), res -> {
            runOnUiThread(() -> {
                LawyerBean bean = new Gson().fromJson(res, LawyerBean.class);
                listAdapter.setData(bean.getRows());
            });
        });
    }

    @Override
    public void resumeData() {

    }

    @Override
    public void initView() {

        mTl = findViewById(R.id.tl);
        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}