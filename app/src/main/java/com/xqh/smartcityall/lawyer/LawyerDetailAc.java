package com.xqh.smartcityall.lawyer;

import android.content.Intent;
import android.view.View;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.active.ActiveCommentAdapter;
import com.xqh.smartcityall.pojo.AvatarComment;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.EvaluateComment;
import com.xqh.smartcityall.pojo.HouseDetail;
import com.xqh.smartcityall.pojo.LawyerDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.jumpTo;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class LawyerDetailAc extends BaseAc {


    private android.widget.ImageView mAvatar;
    private android.widget.TextView mName;
    private android.widget.TextView mDetail;
    private android.widget.TextView mStatus;
    private com.google.android.material.tabs.TabLayout mTl;
    private android.widget.LinearLayout mContentLayout;
    private android.widget.ImageView mCover;
    private android.widget.TextView mContent;
    private androidx.recyclerview.widget.RecyclerView mCommentRv;
    private android.widget.Button mTodoBt;

    private List<EvaluateComment.RowsBean> comments = new ArrayList<>();
    private LawyerCommentAdapter adapter;

    @Override
    public String initTitle() {
        return "律师详情";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_lawyer_detail;
    }

    @Override
    public void initData() {

        adapter = new LawyerCommentAdapter(activity, null);
        adapter.onClick = ()->{
            HashMap<String, String> map = new HashMap<>();
            map.put("adviceId", adapter.posBean.getAdviceId()+"");
            map.put("like", !adapter.posBean.isMyLikeState()+"");

            post("/prod-api/api/lawyer-consultation/legal-advice/evaluate-like", map, json -> {
                runOnUiThread(() -> {
                    DefRes res = new Gson().fromJson(json, DefRes.class);
                    toast(res.getMsg());
                });
            });
        };
        initRV(mCommentRv, adapter);


        mTodoBt.setOnClickListener(v -> {
            jumpId(LawyerConsultCreateAc.class, getJumpId());
        });
        mTl.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tab.getText().equals("服务方式")) {
                    mContentLayout.setVisibility(View.VISIBLE);
                    mCommentRv.setVisibility(View.GONE);
                } else{
                    mCommentRv.setVisibility(View.VISIBLE);
                    mContentLayout.setVisibility(View.GONE);
                    loadComment();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void loadComment() {
        get(String.format("/prod-api/api/lawyer-consultation/lawyer/list-evaluate?lawyerId=%s", getJumpId()), res -> {
            runOnUiThread(()->{
                EvaluateComment bean = new Gson().fromJson(res, EvaluateComment.class);
                if(bean.getCode()==200) {
                    if(bean.getRows() == null || bean.getRows().size()==0) {
                        toast("没有更多数据了");
                        return;
                    }
                    adapter.setData(bean.getRows());
                }
            });
        });
    }
    @Override
    public void resumeData() {
        loadComment();
        get("/prod-api/api/lawyer-consultation/lawyer/"+getJumpId(), lawyerRes -> {
            activity.runOnUiThread(() -> {
                LawyerDetail bean = new Gson().fromJson(lawyerRes, LawyerDetail.class);
                if (bean.getCode() == 200) {
                    LawyerDetail.DataBean data = bean.getData();
                    loadImg(data.getAvatarUrl(), mAvatar);
                    mName.setText(getStr(data.getName()));
                    mDetail.setText(getStrHtml("擅长:%s&emsp;已服务:%s人<br/>",
                            data.getLegalExpertiseName() ,
                            data.getServiceTimes() ));
                    mStatus.setText(String.format("好评率:%s",
                            data.getFavorableRate()+"%"));
                    loadImg(data.getCertificateImgUrl(), mCover);
                    mContent.setText(getStrHtml(
                            "<h4>从业年限</h4>&emsp;%s年<h4>执业证号</h4>&emsp;%s<h4>教育背景</h4>&emsp;%s<h4>个人简介</h4>&emsp;%s",
                            getStr(
                                    Calendar.getInstance().get(Calendar.YEAR)-Integer.parseInt(getStr(data.getWorkStartAt().substring(0,4)))+""
                            ),
                            getStr(data.getLicenseNo()),
                            getStr(data.getEduInfo()),
                            getStr(data.getBaseInfo())
                    ));
                }
            });
        });
    }

    @Override
    public void initView() {

        mAvatar = findViewById(R.id.avatar);
        mName = findViewById(R.id.name);
        mDetail = findViewById(R.id.detail);
        mStatus = findViewById(R.id.status);
        mTl = findViewById(R.id.tl);
        mContentLayout = findViewById(R.id.contentLayout);
        mCover = findViewById(R.id.cover);
        mContent = findViewById(R.id.content);
        mCommentRv = findViewById(R.id.commentRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}