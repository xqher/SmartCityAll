package com.xqh.smartcityall.lawyer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.ConsultBean;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.LawyerBean;
import com.xqh.smartcityall.pojo.LawyerDetail;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.get;
import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.loadCircleImg;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class ConsultAdapter extends RecyclerView.Adapter<ConsultAdapter.VH> {


    private List<ConsultBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public ConsultBean.RowsBean posBean;

    public ConsultAdapter(Context ctx, List<ConsultBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<ConsultBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_button, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_button;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mStatus;
        private Button mTodoBt;


        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mTodoBt.setVisibility(View.GONE);
        }

        public void onItem(ConsultBean.RowsBean bean) {
            get("/prod-api/api/lawyer-consultation/lawyer/"+bean.getLawyerId(), res -> {
                activity.runOnUiThread(() -> {
                    LawyerDetail lawyer = new Gson().fromJson(res, LawyerDetail.class);
                    if (lawyer.getCode() == 200) {
                        loadImg(lawyer.getData().getAvatarUrl(), mCover);
                        mContent.setText(getStrHtml("专长:%s<br/>受理状态:%s<br/>",
                                lawyer.getData().getLegalExpertiseName() ,
                                bean.getState().equals("0")?"未受理":"已完成" ));
                    }
                });
            });
            mTitle.setText(getStr(bean.getLawyerName()));
            mStatus.setText(String.format("提交时间:%s",
                    bean.getCreateTime()));

            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                activity.jumpId(ConsultDetailAc.class, bean.getId());
            });
        }

        private void initView() {

            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mStatus = findViewById(R.id.status);
            mTodoBt = findViewById(R.id.todoBt);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
