package com.xqh.smartcityall.lawyer;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.xqh.smartcityall.R;
import com.xqh.smartcityall.pojo.AvatarComment;
import com.xqh.smartcityall.pojo.EvaluateComment;

import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class LawyerCommentAdapter extends RecyclerView.Adapter<LawyerCommentAdapter.VH> {


    private List<EvaluateComment.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public EvaluateComment.RowsBean posBean;

    public LawyerCommentAdapter(Context ctx, List<EvaluateComment.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<EvaluateComment.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_comment, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_comment;
        private ImageView mAvatar;
        private TextView mName;
        private TextView mContent;
        private ImageView mToLike;
        private TextView mLikeNum;
        private TextView mTime;
        private TextView mStatus;

        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mStatus.setVisibility(View.GONE);
        }

        public void onItem(EvaluateComment.RowsBean bean, int pos) {
            loadImg(bean.getFromUserAvatar(), mAvatar);
            mName.setText(getStr(bean.getFromUserName()));
            mContent.setText(Html.fromHtml(String.format("%s", getStr(bean.getEvaluateContent())), Html.FROM_HTML_MODE_COMPACT));
            mLikeNum.setText(String.format("%s", getStr(bean.getLikeCount()+"")));
            mTime.setText(String.format("%s", getStr(bean.getCreateTime()+"")));
            mToLike.setImageResource(bean.isMyLikeState()?R.drawable.ic_liked:R.drawable.ic_unlike);

            mToLike.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
                bean.setMyLikeState(!bean.isMyLikeState());
                bean.setLikeCount(bean.getLikeCount()+(bean.isMyLikeState()?1:-1));
                notifyItemChanged(pos);
            });
        }

        private void initView() {
            mAvatar = itemView.findViewById(R.id.avatar);
            mName = itemView.findViewById(R.id.name);
            mContent = itemView.findViewById(R.id.content);
            mToLike = itemView.findViewById(R.id.toLike);
            mLikeNum = itemView.findViewById(R.id.likeNum);
            mTime = itemView.findViewById(R.id.time);
            mStatus = itemView.findViewById(R.id.status);

        }
    }
}
