package com.xqh.smartcityall.lawyer;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.adapter.IconAdapter;
import com.xqh.smartcityall.charity.CharityAdapter;
import com.xqh.smartcityall.charity.CharityListAc;
import com.xqh.smartcityall.charity.CharitySearchAc;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.DefBanner;
import com.xqh.smartcityall.pojo.DefType;
import com.xqh.smartcityall.pojo.IconBean;
import com.xqh.smartcityall.pojo.LawyerBean;
import com.xqh.smartcityall.pojo.ServeBean;
import com.xqh.smartcityall.pojo.TopLawyer;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.ArrayList;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.initBanner;
import static com.xqh.smartcityall.utils.Const.initGRV;
import static com.xqh.smartcityall.utils.Const.initRV;
import static com.xqh.smartcityall.utils.Const.initSv;

public class LawyerAc extends BaseAc {


    private com.youth.banner.Banner mBanner;
    private androidx.appcompat.widget.SearchView mSv;
    private androidx.recyclerview.widget.RecyclerView mIconRv;
    private android.widget.LinearLayout mConsultBt;
    private androidx.recyclerview.widget.RecyclerView mListRv;
    private android.widget.Button mTodoBt;
    private LawyerAdapter listAdapter;
    @Override
    public String initTitle() {
        return "法律咨询";
    }

    @Override
    public int initLayout() {
        return R.layout.activity_lawyer;
    }

    @Override
    public void initData() {
        listAdapter = new LawyerAdapter(activity, null);
        initRV(mListRv, listAdapter);

        get("/prod-api/api/lawyer-consultation/ad-banner/list", res -> {
            runOnUiThread(() -> {
                DefBanner bean = new Gson().fromJson(res, DefBanner.class);
                if (bean.getCode() == 200) {
                    initBanner(mBanner, bean.getData(), id -> {
                    });
                }
            });
        });

        initSv(mSv, res -> jumpId(LawyerSearchAc.class, res));


        IconAdapter iconAdapter = new IconAdapter(activity, null,4);
        iconAdapter.onClick = ()->{
            jump(ExpertListAc.class);
        };
        initGRV(mIconRv, iconAdapter, 8);
        get("/prod-api/api/lawyer-consultation/legal-expertise/list", res -> {
            runOnUiThread(() -> {
                ServeBean bean = new Gson().fromJson(res, ServeBean.class);
                if (bean.getCode() == 200) {
                    List<IconBean> show = new ArrayList<>();
                    for (ServeBean.RowsBean data : bean.getRows()) {
                        IconBean icon = new IconBean();
                        icon.setIconImg(getStr(data.getImgUrl()));
                        icon.setIconName(getStr(data.getName()));
                        icon.setIconId(getStr(data.getId()+""));
                        show.add(icon);
                    }
                    iconAdapter.setData(show);
                }
            });
        });
        mTodoBt.setOnClickListener(v -> jump(LawyerListAc.class));
        mConsultBt.setOnClickListener(v -> jump(ConsultListAc.class));
    }

    @Override
    public void resumeData() {

        get("/prod-api/api/lawyer-consultation/lawyer/list-top10", res -> {
            runOnUiThread(() -> {
                TopLawyer bean = new Gson().fromJson(res, TopLawyer.class);
                List<LawyerBean.RowsBean> lawyers = new ArrayList<>();
                for(TopLawyer.DataBean top:bean.getData()) {
                    LawyerBean.RowsBean row = new LawyerBean.RowsBean();
                    row.setId(top.getId());
                    row.setAvatarUrl(top.getAvatarUrl());
                    row.setName(top.getName());
                    row.setLegalExpertiseName(top.getLegalExpertiseName());
                    row.setServiceTimes(top.getServiceTimes());
                    row.setFavorableRate(top.getFavorableRate());
                    lawyers.add(row);
                }
                listAdapter.setData(lawyers);
            });
        });
    }

    @Override
    public void initView() {

        mBanner = findViewById(R.id.banner);
        mSv = findViewById(R.id.sv);
        mIconRv = findViewById(R.id.iconRv);
        mConsultBt = findViewById(R.id.consultBt);
        mListRv = findViewById(R.id.listRv);
        mTodoBt = findViewById(R.id.todoBt);
    }
}