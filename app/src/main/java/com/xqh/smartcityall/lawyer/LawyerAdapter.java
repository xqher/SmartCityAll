package com.xqh.smartcityall.lawyer;

import android.app.AlertDialog;
import android.content.Context;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.xqh.smartcityall.R;
import com.xqh.smartcityall.charity.CharityDetailAc;
import com.xqh.smartcityall.pojo.CharityBean;
import com.xqh.smartcityall.pojo.DefRes;
import com.xqh.smartcityall.pojo.LawyerBean;
import com.xqh.smartcityall.utils.BaseAc;

import java.util.HashMap;
import java.util.List;

import static com.xqh.smartcityall.utils.Const.getStr;
import static com.xqh.smartcityall.utils.Const.getStrHtml;
import static com.xqh.smartcityall.utils.Const.loadImg;

public class LawyerAdapter extends RecyclerView.Adapter<LawyerAdapter.VH> {


    private List<LawyerBean.RowsBean> list;
    private Context ctx;
    public Runnable onClick;
    public LawyerBean.RowsBean posBean;

    public LawyerAdapter(Context ctx, List<LawyerBean.RowsBean> list) {
        this.ctx = ctx;
        this.list = list;
    }

    public void setData(List<LawyerBean.RowsBean> list) {
        this.list = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public VH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new VH(LayoutInflater.from(ctx).inflate(R.layout.item_button, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull VH holder, int position) {
        holder.onItem(list.get(position));
    }

    @Override
    public int getItemCount() {
        return list == null ? 0 : list.size();
    }

    class VH extends RecyclerView.ViewHolder {
        private static final int layoutId = R.layout.item_button;
        private BaseAc activity = (BaseAc) ctx;
        private ImageView mCover;
        private TextView mTitle;
        private TextView mContent;
        private TextView mStatus;
        private Button mTodoBt;


        public VH(@NonNull View itemView) {
            super(itemView);
            initView();
            mTodoBt.setText("咨询");
        }

        public void onItem(LawyerBean.RowsBean bean) {

            loadImg(bean.getAvatarUrl(), mCover);
            mTitle.setText(getStr(bean.getName()));
            mContent.setText(getStrHtml("擅长:%s&emsp;已服务:%s人<br/>",
                    bean.getLegalExpertiseName() ,
                    bean.getServiceTimes() ));
            mStatus.setText(String.format("好评率:%s",
                    bean.getFavorableRate()+"%"));

            mTodoBt.setOnClickListener(v -> {
                activity.jumpId(LawyerDetailAc.class, bean.getId());
            });
            itemView.setOnClickListener(v -> {
                posBean = bean;
                if (onClick != null) {
                    onClick.run();
                }
            });
        }

        private void initView() {

            mCover = findViewById(R.id.cover);
            mTitle = findViewById(R.id.title);
            mContent = findViewById(R.id.content);
            mStatus = findViewById(R.id.status);
            mTodoBt = findViewById(R.id.todoBt);
        }

        private <T extends View> T findViewById(int id) {
            return itemView.findViewById(id);
        }
    }
}
